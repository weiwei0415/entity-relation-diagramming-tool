#pragma once
#include "State.h"
class GUIPresentation;

class SetPrimaryKeyState : public State{
	public:
		SetPrimaryKeyState(GUIPresentation *present);
		~SetPrimaryKeyState();
		bool canViewItemSelectable();
		bool canViewItemMovwble();
		bool canEdgeSelectable();
		bool canEdgeMoveble();
};


