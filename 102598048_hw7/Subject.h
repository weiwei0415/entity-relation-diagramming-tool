#pragma once
#include <list>
class Observer;
using namespace std;

class Subject{
	public :
		Subject();
		virtual ~Subject();
		void attach(Observer*);
		void detach(Observer*);
		void notify();
	private :
		list<Observer*> observers;
};

