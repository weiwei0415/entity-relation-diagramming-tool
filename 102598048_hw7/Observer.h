#pragma once
class Observer{
	public:
		Observer();
		virtual ~Observer();
		virtual void updateChecked() = 0;
};

