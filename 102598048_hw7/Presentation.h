#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include "DefineSet.h"
#include "ERModel.h"
#include "Components.h"
#include "CommandManager.h"
#include "AddComponentCmd.h"
#include "DeleteComponentCmd.h"
#include "ConnectComponentsCmd.h"
using namespace std;

class Presentation{
	private:
		ERModel* model;
		CommandManager commandManager;
		vector <int> tempNode;
		vector <int> attributeOfEntity;
	public:
		Presentation(ERModel* model);
		~Presentation(void);
		bool loadFileAction(string fileName);
		void saveFileAction(string fileName);
		void addNodeAction(char nodeType, string text);
		void deleteNodeAction(size_t delID);
		void connectNodeAction(int fromID, int toID, string inputType);
		void addNodeCount(char inputType); //新增各節點種類數量
		void deleteNodeCount(char inputType); //刪除各節點種類數量
		void addAttributeOfEntity(int index);
		void addPrimaryKey(int selectedID, vector<int> input);
		string redo();
		string undo();
		bool isNodesEmpty();
		bool isEntityEmpty();
		bool isAttributeOfEntityEmpty();
		bool isBelongToEntity(vector<int> input);
		int checkCommand(string str);  //檢查Command
		char checkInput(string str);  //檢查新增類型
		bool checkID(size_t ID);  //檢查ID是否存在
		bool checkType(int fromID, int toID);
		bool checkCardinality(int fromID, int toID);
		string checkCardinalityType(char inputType);
		void checkAttributeOfEntity(int nodeOne, int nodeTwo, int index);
		bool existPathBetween(int firstID, int secondID);
		vector <Components*> getAllConnections();
		int getAttributeOfEntity(int index);
		int getAttributeOfEntitySize();
		int getErrorAttribute(vector<int> input);
		vector <Components*> getNodes();
		int getLastID();
		int getNodesSize();
		Entity* getPrimaryKey(int selectedID);
		int nodeCount;
		int attributeCount;
		int entityCount;
	friend class PresentationTest;
	friend class IntegrationTest;
};

