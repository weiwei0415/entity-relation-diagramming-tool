#include "ComponentTableWidget.h"

ComponentTableWidget::ComponentTableWidget(GUIPresentation *presentation, QWidget *widget){
	this->parentWidget = widget;
	this->presentation = presentation;
	this->presentation->attach(this);
	this->verticalHeader()->setVisible(false);
	this->setEditTriggers(false);
	this->setFont(QFont(TABLE_TEXT_FONT_, TABLE_TEXT_SIZE_));
	this->setColumnCount(COMPONENTS_TABLE_CULUMN_SIZE_);
	this->hearerId = new QTableWidgetItem(HEARER_ID_, Qt::Horizontal);
	this->headerType = new QTableWidgetItem(HEARER_TYPE_, Qt::Horizontal);
	this->hearerText = new QTableWidgetItem(HEARER_TEXT_, Qt::Horizontal);
	this->setHorizontalHeaderItem(TABLE_ID_COLUMN_, this->hearerId);
	this->setHorizontalHeaderItem(TABLE_TYPE_COLUMN_, this->headerType);
	this->setHorizontalHeaderItem(TABLE_TEXT_COLUMN_, this->hearerText);	
	this->updateChecked();
}

ComponentTableWidget::~ComponentTableWidget(void){
	delete this->hearerId, this->headerType, this->hearerText;
	this->freeTheTableWidgetItems();
}

void ComponentTableWidget::mouseDoubleClickEvent(QMouseEvent *event){
	QTableWidget::mouseDoubleClickEvent(event);
	bool isEdit = true;
	if(this->allComponents[this->currentRow()].type.c_str() == ATTRIBUTE_)
		presentation->setAddNodeType(Components::attributeType);
	else if(this->allComponents[this->currentRow()].type.c_str() == ENTITY_)
		presentation->setAddNodeType(Components::entityType);
	else if(this->allComponents[this->currentRow()].type.c_str() == RELATION_)
		presentation->setAddNodeType(Components::relationType);
	if (this->presentation->canEnterText() && this->currentColumn() == TABLE_TEXT_COLUMN_){
		QString newText = QInputDialog::getText(this->parentWidget, tr(ENTER_TEXT_TITLE_), tr(ENTER_TEXT_MESSAGE_), QLineEdit::Normal, this->selectedItems().back()->text(), &isEdit);
		if (!isEdit)
			return;
		this->presentation->editTheTextAction(this->allComponents[this->currentRow()].id, newText.toLocal8Bit().constData());
	}
}

void ComponentTableWidget::updateChecked(){
	this->freeTheTableWidgetItems();
	this->allComponents = this->presentation->getAllComponents();
	this->setRowCount(this->allComponents.size());
	for (size_t i = 0 ; i < this->allComponents.size() ; i++){		
		this->tableWidgetItems.push_back(new QTableWidgetItem(QString::number(this->allComponents[i].id)));
		this->setItem(i, TABLE_ID_COLUMN_, this->tableWidgetItems.back());
		this->tableWidgetItems.push_back(new QTableWidgetItem(QString::fromLocal8Bit(this->allComponents[i].type.c_str())));
		this->setItem(i, TABLE_TYPE_COLUMN_, this->tableWidgetItems.back());
		this->tableWidgetItems.push_back(new QTableWidgetItem(QString::fromLocal8Bit(this->allComponents[i].text.c_str())));
		this->setItem(i, TABLE_TEXT_COLUMN_, this->tableWidgetItems.back());
	}
	this->resizeColumnsToContents();
}

void ComponentTableWidget::freeTheTableWidgetItems(){
	qDeleteAll(tableWidgetItems);
	this->tableWidgetItems.resize(ZERO_);
}
