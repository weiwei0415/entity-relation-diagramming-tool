#include "SaveXmlComponentVisitor.h"

SaveXmlComponentVisitor::SaveXmlComponentVisitor(ERModel *model){
	this->componentStream = string(DEFAULT_);
	this->model = model;
}

SaveXmlComponentVisitor::~SaveXmlComponentVisitor(){
}

void SaveXmlComponentVisitor::visit(Attributes *attributeNode){
	stringstream attributeId, positionX, positionY;
	attributeId << attributeNode->getId();
	positionX << this->model->getPositionMap()[attributeNode->getId()].x();
	positionY << this->model->getPositionMap()[attributeNode->getId()].y();
	this->componentStream += string(TAB_) + string(ATTRIBUTE_HEAD_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(ID_HEAD_) + attributeId.str() + string(ID_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(TEXT_HEAD_) + attributeNode->getText() + string(TEXT_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(POSITION_X_HEAD_) + positionX.str() + string(POSITION_X_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(POSITION_Y_HEAD_) + positionY.str() + string(POSITION_Y_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(ATTRIBUTE_TAIL_) + string(BR_);	
}

void SaveXmlComponentVisitor::visit(Entity *entityNode){
	stringstream entityId, positionX, positionY, keyId;
	vector<Components*> primaryKey = entityNode->getAllPrimaryKey();
	entityId << entityNode->getId();
	positionX << this->model->getPositionMap()[entityNode->getId()].x();
	positionY << this->model->getPositionMap()[entityNode->getId()].y();
	this->componentStream += string(TAB_) + string(ENTITY_HEAD_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(ID_HEAD_) + entityId.str() + string(ID_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(TEXT_HEAD_) + entityNode->getText() + string(TEXT_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(PRIMARYKEY_HEAD_);
	for(size_t i = 0 ; i < primaryKey.size() ; i++){
		keyId << primaryKey[i]->getId();
		if(i < primaryKey.size() - ONE_)
			keyId << string(COMMA_STR_);
	}
	this->componentStream += keyId.str() + string(PRIMARYKEY_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(POSITION_X_HEAD_) + positionX.str() + string(POSITION_X_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(POSITION_Y_HEAD_) + positionY.str() + string(POSITION_Y_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(ENTITY_TAIL_) + string(BR_);
}

void SaveXmlComponentVisitor::visit(Relationship *relationNode){
	stringstream relationId, positionX, positionY;
	relationId << relationNode->getId();
	positionX << this->model->getPositionMap()[relationNode->getId()].x();
	positionY << this->model->getPositionMap()[relationNode->getId()].y();
	this->componentStream += string(TAB_) + string(RELATION_HEAD_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(ID_HEAD_) + relationId.str() + string(ID_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(TEXT_HEAD_) + relationNode->getText() + string(TEXT_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(POSITION_X_HEAD_) + positionX.str() + string(POSITION_X_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(POSITION_Y_HEAD_) + positionY.str() + string(POSITION_Y_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(RELATION_TAIL_) + string(BR_);
}

void SaveXmlComponentVisitor::visit(Connection *connections){
	stringstream connectionId, sourceId, targetId;
	connectionId << connections->getId();
	sourceId << connections->getConnection()[ZERO_]->getId();
	targetId << connections->getConnection()[ONE_]->getId();
	this->componentStream += string(TAB_) + string(CONNECTION_HEAD_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(ID_HEAD_) + connectionId.str() + string(ID_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(TEXT_HEAD_) + connections->getText() + string(TEXT_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(SOURCE_HEAD_) + sourceId.str() + string(SOURCE_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(TAB_) + string(TARGET_HEAD_) + targetId.str() + string(TARGET_TAIL_) + string(BR_);
	this->componentStream += string(TAB_) + string(CONNECTION_TAIL_) + string(BR_);
}

string SaveXmlComponentVisitor::getFileString(){
	return this->componentStream;
}