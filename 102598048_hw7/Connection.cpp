#include "Connection.h"
#include "ComponentVisitor.h"

Connection::Connection(void){
}

Connection::~Connection(void){
}

Connection::Connection(int id, string name, string text){
	this->setId(id);
	this->setType(name);
	this->setText(text);
}

vector<Components*> Connection::getConnection(){
	return connections;
}

void Connection::connectTo(Components* Components){
	connections.push_back(Components);
}

void Connection::disconnectTo(Components* Components){
	for(size_t i = 0 ; i < connections.size() ; i++){
		if(connections[i]->getId() == Components->getId())
			connections.erase(connections.begin() + i);
	}	
}


void Connection::accept(ComponentVisitor &visitor){
	visitor.visit(this);
}

Components* Connection::clone(){
	return new Connection(getId(), getType(), getText());
}