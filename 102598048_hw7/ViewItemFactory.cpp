#include "ViewItemFactory.h"

ComponentViewItem* ViewItemFactory::createViewItem(char inputType, QString text, size_t id){
	ComponentViewItem *newViewItem = NULL;
	switch (inputType){
		case ATTRIBUTE_TYPE_:
			newViewItem = new AttributeViewItem(text, id);
			break;
		case ENTITY_TYPE_:
			newViewItem = new EntityViewItem(text, id);
			break;
		case RELATION_TYPE_:
			newViewItem = new RelationViewItem(text, id);
			break;		
	}
	return newViewItem;
}