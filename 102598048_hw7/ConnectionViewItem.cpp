#include "ConnectionViewItem.h"

ConnectionViewItem::ConnectionViewItem(size_t id, ComponentViewItem *startItem, ComponentViewItem *endItem, QString text)
	: myStartItem(startItem), myEndItem(endItem){
	this->id = id;
	this->text = text;
	this->lastRotateAngel = ZERO_;
	this->lastStartPoint = startItem->pos();
	this->lastEndPoint = endItem->pos();
	this->lastPoint = QPointF(ZERO_, ZERO_);
	qPath.moveTo(lastStartPoint);
	qPath.lineTo(lastEndPoint);
	qFont = QFont(DRAW_TEXT_FONT_, DRAW_TEXT_SIZE_);
}

void ConnectionViewItem::setStartItem(ComponentViewItem *startItem){
	this->myStartItem = startItem;
	this->lastStartPoint = startItem->pos();
}

void ConnectionViewItem::setEndItem(ComponentViewItem *endItem){
	this->myEndItem = endItem;
	this->lastEndPoint = endItem->pos();
}

void ConnectionViewItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
	this->controlMovement();
	painter->setRenderHint(QPainter::Antialiasing);
	painter->setFont(qFont);
	QPointF startPosition = myStartItem->pos();
	QPointF endPosition = myEndItem->pos();
	QPointF startIntersect = this->calculatorIntersectPoint(endPosition, startPosition, myStartItem);
	QPointF ensIntersect = this->calculatorIntersectPoint(startPosition, endPosition, myEndItem);
	finalLine = QLineF(ensIntersect, startIntersect);//算長度
	this->stretchEdge(finalLine.length());
	if (isSelected()) 
		this->extendBoundingRectangle(painter);// If Selected, 畫BoundingBox
	painter->setPen(QPen(Qt::black, DRAW_CONNECTION_PEN_WIDTH_, Qt::SolidLine));
	painter->drawPath(qPath);// arrow and line
	qPath.addPath(this->addTextPath(finalLine.length(), startPosition.x(), myEndItem->pos().x()));
	painter->fillPath(qPath, QBrush(Qt::black)); // for text
	qPath.addRect(this->boundingRect().normalized().adjusted(-ADDED_BOUNDING_HORIZONTAL_, -ADDED_BOUNDING_VERTICAL_, ADDED_BOUNDING_HORIZONTAL_, ADDED_BOUNDING_VERTICAL_));// 增加bounding 
	this->setPos(startIntersect);// this item's position to startItem
	this->doRotate(finalLine.length(), startPosition, endPosition);// ---計算旋轉角度 to endItem
	this->lastPoint = this->pos();	
}

void ConnectionViewItem::controlMovement(){
	if (myEndItem->getComponentId() == ADSORPTION_POINT_ && myStartItem->getComponentId() != ADSORPTION_POINT_){
		myEndItem->setPos(myEndItem->x() + (myStartItem->x() - lastStartPoint.x()), myEndItem->y() + myStartItem->y() - lastStartPoint.y());
		lastStartPoint = myStartItem->pos();
	}
	else if (myEndItem->getComponentId() != ADSORPTION_POINT_ && myStartItem->getComponentId() == ADSORPTION_POINT_){
		myStartItem->setPos(myStartItem->x() + (myEndItem->x() - lastEndPoint.x()), myStartItem->y() + myEndItem->y() - lastEndPoint.y());
		lastEndPoint = myEndItem->pos();
	}
	else if (myEndItem->getComponentId() == ADSORPTION_POINT_ && myStartItem->getComponentId() == ADSORPTION_POINT_){
		myStartItem->setPos(myStartItem->x() + (this->x() - lastPoint.x()), myStartItem->y() + (this->y() - lastPoint.y()));
		myEndItem->setPos(myEndItem->x() + (this->x() - lastPoint.x()), myEndItem->y() + (this->y() - lastPoint.y()));
		if (myStartItem->pos() == myEndItem->pos())
			myStartItem->setX(myEndItem->x() - DEFAULT_CONNECTION_LENGTH_); 
	}
}

QPointF ConnectionViewItem::calculatorIntersectPoint(QPointF beginning, QPointF termination, ComponentViewItem *goalItem){
	QLineF centerLine(beginning, termination);
	QPolygonF endPolygon = goalItem->getPolygon();
	QPointF pointOne = endPolygon.first() + termination;
	QPointF pointTwo;
	QPointF goalIntersectPoint;
	QLineF polyLine;
	for (size_t i = 0 ; i < endPolygon.count() ; ++i){
		pointTwo = endPolygon.at(i) + termination;
		polyLine = QLineF(pointOne, pointTwo);
		QLineF::IntersectType intersectType = polyLine.intersect(centerLine, &goalIntersectPoint);
		if (intersectType == QLineF::BoundedIntersection) 
			return goalIntersectPoint;
		pointOne = pointTwo;
	}
	return goalItem->pos();
}

void ConnectionViewItem::stretchEdge(qreal length){
	qPath = QPainterPath();//每次都先清空
	qPath.moveTo(ZERO_, ZERO_);
	qPath.lineTo(-length, ZERO_);// 先劃出長度足以連接startItem 與endItem 的水平線
}

void ConnectionViewItem::extendBoundingRectangle(QPainter *painter){
	painter->setPen(QPen(Qt::darkCyan, DRAW_BOUNDING_PEN_WIDTH_, Qt::DashDotDotLine));
	painter->drawRect(this->boundingRect().normalized());
}

QPainterPath ConnectionViewItem::addTextPath(qreal length, qreal beginning, qreal termination){
	QMatrix matrix;
	QPainterPath textPath;
	QFontMetrics fontMetrics(qFont);
	fontMetrics.width(this->text);
	if (beginning < termination){
		matrix.rotate(PI_);// Rotated 180 degrees
		textPath.addText(QPointF(HALF_ * length-HALF_ * fontMetrics.width(this->text), DRAW_CONNECTION_TEXT_POS_Y_), qFont, this->text);
	}
	else 
		textPath.addText(QPointF(-HALF_ * length-HALF_ * fontMetrics.width(this->text), DRAW_CONNECTION_TEXT_POS_Y_), qFont, this->text);
	return matrix.map(textPath);
}

void ConnectionViewItem::doRotate(qreal length, QPointF beginlPosition, QPointF goalPosition){
	QPointF tempPosition = goalPosition;
	tempPosition.setX(tempPosition.x() + length);
	QLineF line = QLineF(goalPosition, tempPosition);
	rotateAngle = line.angle(finalLine);
	if (beginlPosition.y() < tempPosition.y())  
		rotateAngle = DOUBLE_ * PI_ - rotateAngle;
	this->rotate(rotateAngle - lastRotateAngel);
	lastRotateAngel = rotateAngle;
}

void ConnectionViewItem::setTextAndId(QString text, size_t id){
	this->text = text;
	this->id = id;
}

QPainterPath ConnectionViewItem::shape() const{
    return qPath;
}

void ConnectionViewItem::updateViewItem(QString text){
	this->text = text;
}

void ConnectionViewItem::setPrimaryKeyViewItem(){
}

void ConnectionViewItem::resetPrimaryKeyViewItem(){
}