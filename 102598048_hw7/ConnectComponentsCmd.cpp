#include "ConnectComponentsCmd.h"

ConnectComponentsCmd::ConnectComponentsCmd(ERModel *model, size_t from, size_t to, string inputType){
	this->model = model;
	this->connectionId = this->model->getLastID() + ONE_;
	this->from = from;
	this->to = to;
	this->input = inputType;
}

ConnectComponentsCmd::~ConnectComponentsCmd(void){
}

void ConnectComponentsCmd::execute(){
	this->model->addNode(CONNECTION_TYPE_, input, connectionId);
	this->model->addConnection(connectionId, model->getNodes()[from], model->getNodes()[to], input);
}

void ConnectComponentsCmd::unexecute(){
	this->model->deleteComponent(connectionId);
}