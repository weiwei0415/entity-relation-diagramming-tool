#include "PasteComponentCmd.h"
const QPointF NODE_PASTE_OFFSET_ = QPointF(30, 30); 

PasteComponentCmd::PasteComponentCmd(ERModel *model){
	this->model = model;
	this->clone = this->model->getClone();
}

PasteComponentCmd::~PasteComponentCmd(){
}

void PasteComponentCmd::execute(){
	for(size_t i = 0 ; i < clone.size() ; i++){
		AddComponentCmd* addCmd = new AddComponentCmd(this->model, clone[i]->getType()[ZERO_], clone[i]->getText());
		addCmd->execute();
		commands.push(addCmd);
		QPointF clonePosition = this->model->getPositionMap()[clone[i]->getId()];
		this->model->setPostionMap(this->model->getNodes().back()->getId(), clonePosition + NODE_PASTE_OFFSET_);
		clone[i]->setId(this->model->getNodes().back()->getId());		
	}
}

void PasteComponentCmd::unexecute(){
	while(commands.size() != 0){
		Command* command = commands.top();
		commands.pop();
		command->unexecute();
	}
}