#pragma once
#include <QtGui>
#include <QTableWidget>
#include <QString>
#include "DefineSet.h"
#include "Observer.h"
#include "GUIPresentation.h"

class ComponentTableWidget : public QTableWidget, public Observer{
	public:
		ComponentTableWidget(GUIPresentation *presentation, QWidget *widget);
		~ComponentTableWidget(void);
		void updateChecked();
	protected:
		void mouseDoubleClickEvent(QMouseEvent *event);
	private:
		QWidget *parentWidget;
		GUIPresentation *presentation;
		QTableWidgetItem *hearerId;
		QTableWidgetItem *headerType;
		QTableWidgetItem *hearerText;
		vector<AllComponents> allComponents;
		vector<QTableWidgetItem *> tableWidgetItems;
		void freeTheTableWidgetItems();
};