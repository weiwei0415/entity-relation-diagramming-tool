#include "DeleteComponentCmd.h"

DeleteComponentCmd::DeleteComponentCmd(ERModel *model, int deleteID){
	this->model = model;
	this->nodeId = deleteID;
	this->allComponents = this->model->getNodes();
	for(size_t i = 0 ; i < allComponents.size() ; i++){
		if(this->model->getNodes()[i]->getId() == deleteID){
			this->nodeType = allComponents[i]->getType()[ZERO_];
			this->nodeText = allComponents[i]->getText();
			this->component = allComponents[i];  //欲刪除的Component
		}
	}	
	this->connectors = component->getConnection();     //抓取Component所有相關的Connection
	for each(Components* comp in this->model->getAllConnections()){   //抓取所有Connection
		for each(Components* node in comp->getConnection())        //抓取Connection裡的節點
			tempComponents.push_back(node);
		if(tempComponents[ZERO_]->getId() == nodeId || tempComponents[ONE_]->getId() == nodeId)
			connectionVector.push_back(comp);
		tempComponents.clear();	
	}	
	if(nodeType == ENTITY_TYPE_){
		this->primarykeys = dynamic_cast <Entity*> (component)->getAllPrimaryKey();
		for(size_t i = 0 ; i < primarykeys.size() ; i++)
			input.push_back(primarykeys[i]->getId());
	}
}

DeleteComponentCmd::~DeleteComponentCmd(void){
}

void DeleteComponentCmd::execute(){
	this->model->deleteComponent(nodeId);
}

void DeleteComponentCmd::unexecute(){
	this->model->addNode(nodeType, nodeText, nodeId);
	if(nodeType == CONNECTION_TYPE_)
		this->model->addConnection(nodeId, connectors[ZERO_], connectors[ONE_], CONNECTION_);
	else{
		for(size_t i = 0 ; i < connectionVector.size() ; i++){
			this->model->addNode(CONNECTION_TYPE_, connectionVector[i]->getText(), connectionVector[i]->getId());
			this->model->addConnection(connectionVector[i]->getId(), connectionVector[i]->getConnection()[ZERO_],
				connectionVector[i]->getConnection()[ONE_], CONNECTION_);
		}
		if(nodeType == ENTITY_TYPE_)
			this->model->addPrimaryKey(nodeId, input);
	}
}