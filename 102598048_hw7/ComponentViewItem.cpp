#include "ComponentViewItem.h"

ComponentViewItem::ComponentViewItem(void){
	this->setFlag(QGraphicsItem::ItemIsSelectable, true);
	this->setFlag(QGraphicsItem::ItemIsMovable, true);
}

ComponentViewItem::~ComponentViewItem(void){
}

QPolygonF ComponentViewItem::getPolygon() const{
	return qPath.toFillPolygon().toPolygon();
}

QRectF ComponentViewItem::boundingRect() const{
	return qPath.boundingRect();
}


