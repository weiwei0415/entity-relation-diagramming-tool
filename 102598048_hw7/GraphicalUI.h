#include "ComponentScene.h"
#include "ComponentTableWidget.h"
#include "Observer.h"
#include "DefineSet.h"
#include <QtGui>
#include <QObject>
#include <vector>
using namespace std;

class GraphicalUI : public QMainWindow, public Observer{
	Q_OBJECT
	public:
		GraphicalUI(GUIPresentation *present);
		~GraphicalUI();
		void updateChecked();
	private:
		GUIPresentation *presentation;
		QGraphicsView *view;
		QWidget *widget;
		QToolBar *toolBarFile;
		QToolBar *toolBarEdit;
		QToolBar *toolBarAdd;
		ComponentScene *scene;
		QMenu *menuFile;
		QMenu *menuEdit;
		QMenu *menuAdd;
		QMenu *menuHelp;
		QMessageBox *msgBox;
		QAction *openFile;
		QAction *saveFile;
		QAction *saveFileAsXml;
		QAction *exit;
		QAction *undo;
		QAction *redo;
		QAction *deleteComponent;
		QAction *cut;
		QAction *copy;
		QAction *paste;
		QAction *newAttribute;
		QAction *newEntity;
		QAction *newRelation;
		QAction *pointer;
		QAction *connection;
		QAction *primaryKey;
		QAction *tableViewer;
		QAction *aboutTool;
		QDockWidget *componentsTableDockWidget;
		QVBoxLayout *componentsTableLayout;
		QGroupBox *componentsTableGroupBox;
		ComponentTableWidget *componentsTableWidget;
		QTextEdit *textEdit;
		void initialize();
		void initialMenu();
		void initialAction();
		void createToolBar();
	private slots:
		void slotOpenFile();
		void slotSaveFile();
		void slotSaveFileAsXml();
		void slotSelectPointerMode();
		void slotUndo();
		void slotRedo();
		void slotDelete();
		void slotCut();
		void slotCopy();
		void slotPaste();
		void slotAddConnectionMode();
		void slotAddAttributeNode();
		void slotAddEntityNode();
		void slotAddRelationNode();
		void slotAddPrimaryKey();
		void slotSceneViewItemSelectionChanged();
		void slotTableViewer();
		void slotAboutTool();	
};

