#include "TextUI.h"

TextUI::TextUI(Presentation *present):isEnd(true){
	presentation = present;
}

TextUI::~TextUI(void){
}

void TextUI::displayMenu(){
	do{
		cout << "1. Load ER diagram file" << endl
			 << "2. Save ER diagram file" << endl
			 << "3. Add a node" << endl
			 << "4. Connect two nodes" << endl
			 << "5. Display the current diagram" <<endl
			 << "6. Set a primary key" <<endl
			 << "7. Display the table " <<endl
			 << "8. Delete a component " <<endl
			 << "9. Undo" <<endl
			 << "10.Redo" <<endl
			 << "11.Exit" <<endl
			 << ">";
		processCommand();
	}while (isEnd);
}

void TextUI::processCommand(){
	getline(cin, tempString);  //取得整串文字放入tempString
	inputCommand = presentation->checkCommand(tempString);  //用checkInput檢查tempString，並回傳一char
	switch(inputCommand){		
		case CASE_OF_LOAD_ER_DIAGRAM_FILE_ :
			loadERDiagramFile();
			break;
		case CASE_OF_SAVE_ER_DIAGRAM_FILE_ :
			saveERDiagramFile();
			break;
		case CASE_OF_ADD_A_NODE_ :
			addANode();
			break;
		case CASE_OF_CONNECT_TWO_NODES_ :
			connectTwoNodes();
			break;
		case CASE_OF_DISPLAY_THE_CURRENT_DIAGRAM_ :
			cout << "The ER diagram is displayed as follows:" << endl;
			showDiagrams(CASE_OF_DISPLAY_THE_CURRENT_DIAGRAM_);  //顯示Nodes清單
			showConnections();  
			break;
		case CASE_OF_SET_A_PRIMARY_KEY_ :
			setAPrimaryKey();
			break;
		case CASE_OF_DISPLAY_THE_TABLE_ :
			showTable();
			break;
		case CASE_OF_DELETE_A_COMPONENT_ :
			deleteAComponent();
			break;
		case CASE_OF_UNDO_ :
			if(presentation->undo().compare(UNDO_SUCCEED_) == ZERO_){
				cout << UNDO_SUCCEED_ << endl;
				showDiagrams(CASE_OF_DISPLAY_THE_CURRENT_DIAGRAM_);  //顯示Nodes清單
				showConnections();  
			}
			else
				cout << CANNOT_UNDO_ << endl << endl;
			break;
		case CASE_OF_REDO_ :
			if(presentation->redo().compare(REDO_SUCCEED_) == ZERO_){
				cout << REDO_SUCCEED_ << endl;
				showDiagrams(CASE_OF_DISPLAY_THE_CURRENT_DIAGRAM_);  //顯示Nodes清單
				showConnections();  
			}
			else
				cout << CANNOT_REDO_ << endl << endl;
			break;
		case CASE_OF_EXIT_ :
			cout << "Goodbye!" << endl;
			isEnd = false;
			break;
		default:
			cout << endl << "The node ID you entered does not exist. Please enter a valid one again." << endl;
			break;
	}
}

void TextUI::loadERDiagramFile(){
	string fileName;
	cout << "Please key in a file path: ";
	getline(cin, fileName);
	bool flag = presentation->loadFileAction(fileName);
	if(!flag) 
		cout << "File not found error!" << endl << endl;
	else{
		cout << "The activity diagram is displayed as follows:" << endl;
		showDiagrams(CASE_OF_DISPLAY_THE_CURRENT_DIAGRAM_);  //顯示Nodes清單
		showConnections(); 
	}
}

void TextUI::saveERDiagramFile(){
	string fileName;
	cout << "Please input the file name: ";
	getline(cin, fileName);
	presentation->saveFileAction(fileName);
	cout << endl;
}

void TextUI::addANode() {
	cout << "What kind of node do you want to add?" << endl
		 << "[A]Attribute [E]Entity [R]Relation" << endl
		 << ">";
	getline(cin, tempString);  //取得整串文字放入tempString
	inputType = presentation->checkInput(tempString);  //用checkInput檢查temp，並回傳一char
	processType(inputType);  //進入下一選單，選擇nodeType
	cout << "A Node [" <<  presentation->getNodes()[presentation->getNodesSize()-ONE_]->getType() 
		 << "] has been added. ID: " << presentation->getNodes()[presentation->getNodesSize()-ONE_]->getId();
	cout << ", Text: " << '"' << presentation->getNodes()[presentation->getNodesSize()-ONE_]->getText() << '"' << endl;
	showDiagrams(CASE_OF_ADD_A_NODE_);  //顯示目前Node清單
}

void TextUI::processType(char inputType){
	if(inputType != ATTRIBUTE_TYPE_ && inputType != ENTITY_TYPE_ && inputType != RELATION_TYPE_){
		cout << "You entered an invalid node. Please enter a valid one again." << endl
			<<">";
		isEnd = false;
	}
	else{
		cout << "Enter the name of this node:" << endl
			 << ">";
		getline(cin, inputName);  //取得整串輸入(含空白)
		presentation->addNodeAction(inputType, inputName); //新增節點動作
		isEnd = true;
	}
	if(isEnd == false){
		getline(cin, tempString);
		inputType = presentation->checkInput(tempString);
		processType(inputType);		
	}
}

void TextUI::connectTwoNodes(){
	if(presentation->isNodesEmpty())
		cout << "There is no nodes can be connected." << endl;
	else{
		cout << "Please enter the first node ID" << endl;
		connectFrom = checkExistNode(CASE_OF_CONNECT_TWO_NODES_);
	
		cout << "Please enter the second node ID" << endl;
		connectTo = checkExistNode(CASE_OF_CONNECT_TWO_NODES_);
		checkConnectState(connectFrom, connectTo);
	}
}

void TextUI::checkConnectState(int connectFrom, int connectTo){
	if(connectFrom == connectTo)  //連接自己的情況下
		cout << "The node '" << connectFrom << "' cannot be connected to itself." << endl << endl;
	else if(presentation->checkType(connectFrom, connectTo) == true) //兩個node型別相同
		cout << "The node '" << connectTo << "' cannot be connected by the node '" << connectFrom << "'." << endl << endl;
	else if(presentation->existPathBetween(connectFrom, connectTo) == true)  //兩個相同node之間已經相連
		cout << "The node '" << connectFrom << "' has already been connected to component '" << connectTo << "'." << endl << endl;
	else{
		if(presentation->checkCardinality(connectFrom, connectTo) == true){
			cout << "Enter the type of the cardinality:" << endl
				 << "[0]1 [1]N " << endl
				 << ">";
			getline(cin, tempString);  
			inputType = presentation->checkInput(tempString);  //用checkInput檢查temp，並回傳一char給InputType
			presentation->connectNodeAction(connectFrom, connectTo, presentation->checkCardinalityType(inputType));
			cout << "The node '" << connectTo << "' has been connected to the node '" << connectFrom << "'." << endl;
			cout << "Its cardinality of the relationship is '" << presentation->checkCardinalityType(inputType) << "'. " << endl << endl;
		}
		else{
			presentation->connectNodeAction(connectFrom, connectTo, DEFAULT_);
			cout << "The node '" << connectTo << "' has been connected to the node '" << connectFrom << "'." << endl << endl;
			showConnections(); 
		}
	}
}

int TextUI::checkExistNode(int inputType){
	do{
		cout << ">";	
		getline(cin, tempString);  //取得整串文字放入temp
		if(atoi(tempString.c_str()) == ZERO_ && tempString != ZERO_OF_STRING_TYPE_)  //判斷輸入非int型別的狀況
			tempInt = INVALID_ID_;   
		else
			tempInt = atoi(tempString.c_str()); //將取得的字串做轉型
		if(presentation->checkID(tempInt)){  //檢查是否存在此ID
			if(inputType == CASE_OF_CONNECT_TWO_NODES_ || inputType == CASE_OF_SET_A_PRIMARY_KEY_)
				cout << "The Node ID you entered does not exist. Please enter a valid one again." << endl;
			else if(inputType == CASE_OF_DELETE_A_COMPONENT_)
				cout << "The Component ID you entered does not exist. Please enter a valid one again." << endl;
		}
	}while(presentation->checkID(tempInt));
	return tempInt;
}

void TextUI::setAPrimaryKey(){
	if(presentation->isEntityEmpty())
		cout << "It has no entity to display." << endl;
	else{
		showDiagrams(CASE_OF_SET_A_PRIMARY_KEY_);  //顯示Entities清單
		cout << "Enter the ID of the entity:" << endl;
		selectedID = checkExistNode(CASE_OF_SET_A_PRIMARY_KEY_);
		checkEntity(selectedID);
	}
}

void TextUI::checkEntity(int index){
	if(presentation->getNodes()[index]->getType()[ZERO_] != ENTITY_TYPE_){
		cout << "The node '" << index << "' is not an entity. Please enter a valid one again." << endl;
		selectedID = checkExistNode(CASE_OF_SET_A_PRIMARY_KEY_);
		checkEntity(selectedID);
	}
	else{
		presentation->addAttributeOfEntity(index);
		if(presentation->isAttributeOfEntityEmpty())
			cout << "The Entity has no attribute. Please add a attribute." << endl;
		else{
			cout << "Attributes of the entity '" << index << "'" << endl;
			cout << "------------------------------------" << endl
				 << "  Type  |  ID  | Text" << endl
				 << "--------+------+--------------------" << endl;
			for(size_t i = 0 ; i < (unsigned)presentation->getAttributeOfEntitySize() ; i++)
				printDiagram(presentation->getAttributeOfEntity(i));
			cout << "------------------------------------" << endl;
			enterIdsOfAttributes();
		}
	}
}

void TextUI::enterIdsOfAttributes(){
	cout << "Enter the IDs of the attributes (use a comma to separate two attributes):" << endl	
		 << ">";
	getline(cin, tempString);  
    istringstream iss(tempString);  //綁定字串
	idsOfAttributes.clear();
	while(getline(iss, token, COMMA_))
		idsOfAttributes.push_back(atoi(token.c_str()));
	if(presentation->isBelongToEntity(idsOfAttributes)){ //輸入的值皆存在
		presentation->addPrimaryKey(selectedID, idsOfAttributes);
		cout << "The entity '" << selectedID << "' has the primary key (" << tempString << ")." << endl << endl;
	}
	else{
		cout << "The node '" << presentation->getErrorAttribute(idsOfAttributes)
			 << "' does not belong to Entity '" << selectedID << "'. Please enter a valid one again." << endl;
		enterIdsOfAttributes();
	}
}

void TextUI::deleteAComponent(){
	if(presentation->isNodesEmpty())
		cout << "There is no components can be deleted." << endl;
	else{
		cout << "Please enter the component ID " << endl;
		deleteID = checkExistNode(CASE_OF_DELETE_A_COMPONENT_);
		presentation->deleteNodeAction(deleteID);
		cout << "The component \"" << deleteID << "\" has been deleted." << endl; 
		showDiagrams(CASE_OF_DELETE_A_COMPONENT_);
		showConnections();
	}
}

void TextUI::showDiagrams(char input){
	switch(input){
		case CASE_OF_ADD_A_NODE_ :
			cout << "Components:" << endl;		
			break;
		case CASE_OF_DISPLAY_THE_CURRENT_DIAGRAM_ :
			cout << "Nodes:" << endl;
			break;
		case CASE_OF_SET_A_PRIMARY_KEY_:
			cout << "Entitys:" << endl;
			break;
		case CASE_OF_DELETE_A_COMPONENT_:
			cout << "Components:" << endl;		
			break;
	}
	cout << "------------------------------------" << endl
	     << "  Type  |  ID  | Text" << endl
	     << "--------+------+--------------------" << endl;
	judgeCaseToPrint(input);
	cout << "------------------------------------" << endl << endl;
}

void TextUI::judgeCaseToPrint(char input){
	for(int i = 0 ; i < presentation->getNodesSize() ; i++){
		switch(input){
			case CASE_OF_ADD_A_NODE_ :
				printDiagram(i);
				break;
			case CASE_OF_DISPLAY_THE_CURRENT_DIAGRAM_ :
				printDiagram(i);
				break;
			case CASE_OF_SET_A_PRIMARY_KEY_:
				if(presentation->getNodes()[i]->getType()[ZERO_] == ENTITY_TYPE_)
					printDiagram(i);
				break;
			case CASE_OF_DELETE_A_COMPONENT_:
				printDiagram(i);
				break;
		}
	}
}

void TextUI::printDiagram(int index){
	cout << setw(THREE_) << presentation->getNodes()[index]->getType()[ZERO_] << "     |" << setw(THREE_)
		 << presentation->getNodes()[index]->getId() << "   |" << " " << presentation->getNodes()[index]->getText() << endl;
}

void TextUI::showConnections(){	
	cout << "Connections:" << endl
	     << "--------------------------" << endl
	     << "Connection | node | node |" << endl
	     << "-----------+------+------|" << endl;	
	for each(Components* comp in presentation->getAllConnections()){
		cout << setw(THREE_) << comp->getId() << "        |";
		for each(Components* node in comp->getConnection())
			cout << setw(THREE_) << node->getId() << "   |";
		cout << endl;
	}
	cout << "--------------------------" << endl << endl;	
}

void TextUI::showTable(){
	if(presentation->isEntityEmpty())
		cout << "It has no table to display." << endl;
	else{
		cout << "Tables:" << endl
			 << "----------------------------------------------------------" << endl
			 << " Entity   | Attribute" << endl
			 << "----------+-----------------------------------------------" << endl;
		for(int i = 0 ; i < presentation->getLastID() ; i++){
			if(presentation->getNodes()[i]->getType()[ZERO_] == ENTITY_TYPE_){
				cout << setw(EIGHT_) << presentation->getNodes()[i]->getText() << "  | ";
				presentation->addAttributeOfEntity(i);
				entity = presentation->getPrimaryKey(i);
				tempAttribute = entity->getAllPrimaryKey(); //暫存Entity內的PrimaryKey
				printPK();		
				printAttribute();
				printFK(i);
				cout << endl;			
			}
		}
		cout << "----------+-----------------------------------------------" << endl;
	}
}

void TextUI::printPK(){
	if(!(tempAttribute.empty())){
		cout << "PK(";
		for(size_t l = 0 ; l < tempAttribute.size() ; l++){
			cout << tempAttribute[l]->getText();
			if(l == tempAttribute.size()-ONE_)
				break;
			cout << ", ";
		}
		cout << ")";
		if((unsigned) presentation->getAttributeOfEntitySize() > tempAttribute.size())
			cout << ", ";
	}	
}

void TextUI::printAttribute(){
	for(size_t j = 0 ; j < (unsigned) presentation->getAttributeOfEntitySize() ; j++){
		isPrimaryKey = false;	//初始先預設為false
		for(size_t k = 0 ; k < tempAttribute.size() ; k++){  //判斷屬性是否為PrimaryKey
			if(presentation->getAttributeOfEntity(j) == tempAttribute[k]->getId()){
				isPrimaryKey = true;
				break;
			}
		}
		if(!(isPrimaryKey)){ //如果不是PrimaryKey則印出
			cout << presentation->getNodes()[presentation->getAttributeOfEntity(j)]->getText();
			if(j == presentation->getAttributeOfEntitySize()-ONE_)
				break;
			cout << ", ";
		}
	}
}

void TextUI::printFK(int index){
	for each(Components* comp in presentation->getAllConnections()){
		for each(Components* node in comp->getConnection())
			tempNode.push_back(node->getId());
		checkRelationOfEntity(tempNode[ZERO_], tempNode[ONE_], index, comp->getId());
		tempNode.clear();
	}
}

void TextUI::checkRelationOfEntity(int nodeOne, int nodeTwo, int entityID, int connectionID){
	if(nodeOne == entityID && presentation->getNodes()[nodeTwo]->getType()[ZERO_] == RELATION_TYPE_){  //Entity Relation
		if(presentation->getNodes()[connectionID]->getText() == SINGLE_)
			checkAnotherEntity(nodeOne, nodeTwo, ENTITY_TO_RELATION_);
	}
	else if(nodeTwo == entityID && presentation->getNodes()[nodeOne]->getType()[ZERO_] == RELATION_TYPE_){  //Relation Entity	
		if(presentation->getNodes()[connectionID]->getText() == SINGLE_)
			checkAnotherEntity(nodeTwo, nodeOne, RELATION_TO_ENTITY_);
	}
}

void TextUI::checkAnotherEntity(int entityID, int relationID, char connectDirection){
	tempNode.clear();  
	for each(Components* comp in presentation->getAllConnections()){
		for each(Components* node in comp->getConnection())
			tempNode.push_back(node->getId());
		if(tempNode[ZERO_] == relationID && tempNode[ONE_] != entityID){   //Relation Entity	
			if(presentation->getNodes()[tempNode[ONE_]]->getType()[ZERO_] == ENTITY_TYPE_){
				if(presentation->getNodes()[comp->getId()]->getText() == SINGLE_)
					printForeignKey(tempNode[ONE_]);
			}
		}
		else if(tempNode[ONE_] == relationID && tempNode[ZERO_] != entityID){   //Entity Relation	
			if(presentation->getNodes()[tempNode[ZERO_]]->getType()[ZERO_] == ENTITY_TYPE_){
				if(presentation->getNodes()[comp->getId()]->getText() == SINGLE_)
					printForeignKey(tempNode[ONE_]);
			}
		}
		tempNode.clear();
	}
}

void TextUI::printForeignKey(int entityID){
	entity = presentation->getPrimaryKey(entityID);
	tempAttribute = entity->getAllPrimaryKey(); //暫存Entity內的PrimaryKey
	if(!(tempAttribute.empty())){
		cout << ", FK(";
		for(size_t i = 0 ; i < tempAttribute.size() ; i++){
			cout << tempAttribute[i]->getText();
			if(i == tempAttribute.size()-ONE_)
				break;
			cout << ", ";
		}
		cout << ")";
	}	
}
