#pragma once
#include "Command.h"

class GUIMoveCmd : public Command{
	public:
		GUIMoveCmd(ERModel *model, size_t id, QPointF pos);
		~GUIMoveCmd();
		void execute();
		void unexecute();
	private:
		QPointF start;
		QPointF terminal;
};

