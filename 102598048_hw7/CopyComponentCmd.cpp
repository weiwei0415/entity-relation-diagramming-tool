#include "CopyComponentCmd.h"

CopyComponentCmd::CopyComponentCmd(ERModel *model, vector<int> itemsId){
	this->model = model;
	this->cutId = itemsId;
	this->allComponents = this->model->getNodes();
}

CopyComponentCmd::~CopyComponentCmd(){
}

void CopyComponentCmd::execute(){
	this->model->cleanClone();
	for(size_t i = 0 ; i < cutId.size() ; i++){
		for(size_t j = 0 ; j < allComponents.size() ; j++){
			if(allComponents[j]->getId() == cutId[i])
				this->model->setClone(cutId[i]);
		}
	}
	this->model->setCopyMode();
}

void CopyComponentCmd::unexecute(){
	while(commands.size() != 0){
		Command* command = commands.top();
		commands.pop();
		command->unexecute();
	}
	this->model->setCopyMode();
	this->model->cleanClone();
}