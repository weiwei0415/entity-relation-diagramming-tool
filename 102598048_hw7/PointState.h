#pragma once
#include "State.h"
class GUIPresentation;

class PointState : public State{
	public:
		PointState(GUIPresentation *present);
		~PointState();
		bool canViewItemSelectable();
		bool canViewItemMovwble();
		bool canEdgeSelectable();
		bool canEdgeMoveble();
};

