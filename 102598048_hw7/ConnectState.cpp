#include "ConnectState.h"

ConnectState::ConnectState(GUIPresentation *present) : State(presentation){
}

ConnectState::~ConnectState(){

}

bool ConnectState::canViewItemSelectable(){
	return false;
}

bool ConnectState::canViewItemMovwble(){
	return true;
}

bool ConnectState::canEdgeSelectable(){
	return false;
}

bool ConnectState::canEdgeMoveble(){
	return true;
}
