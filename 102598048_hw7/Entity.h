#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "Node.h"
#include "Attributes.h"
using namespace std;

class Entity : public Node{
	public:
		Entity();
		~Entity();
		Entity(int id, string name, string text);
		void connectTo(Components* components);
		vector<Components*> getConnection();
		void setPrimaryKey(Components* key);
		vector<Components*> getAllPrimaryKey();
		void resetPrimaryKey();
		void deletePrimaryKey();
		void accept(ComponentVisitor &visitor);
		Components* clone();
	private:
		vector<Components*> connections;
		vector<Components*> primaryKey;  //�s��Primary Key
};

