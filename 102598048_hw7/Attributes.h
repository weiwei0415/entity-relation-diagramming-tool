#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "Node.h"
using namespace std;

class Attributes : public Node{
	public:
		Attributes();
		~Attributes();
		Attributes(int id, string name, string text);
		void connectTo(Components* components);
		vector<Components*> getConnection();
		void accept(ComponentVisitor &visitor);
		Components* clone();
	private:
		vector<Components*> connections;
};

