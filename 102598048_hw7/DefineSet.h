#pragma once
//choose type
#define TYPE_OF_TEXT_UI_ "1"
#define TYPE_OF_GRAPHICAL_UI_ "2"
//textUI
#define ERROR_COMMAND_ -1
#define ZERO_ 0
#define ONE_ 1
#define THREE_ 3
#define EIGHT_ 8
#define INPUT_ ""
#define EMPTY_ ""
#define ZERO_OF_STRING_TYPE_ "0"
#define INVALID_ID_ -1
#define NODE_ONE_ 0
#define NODE_TWO_ 1
#define SINGLE_TYPE_ '0'
#define MULTI_TYPE_ '1'
#define SINGLE_ "1"
#define MULTI_ "N"
#define DEFAULT_ ""
#define COMMA_ ','
#define COMMA_STR_ ","
#define SPACE_ ' '
#define SPACE_STR_ " "
#define BR_ "\n"
#define TAB_ "\t"
#define XML_TITLE_ "<?xml version=\"1.0\"?>"
#define ERD_HEAD_ "<ERDiagram>"
#define ERD_TAIL_ "</ERDiagram>"
#define ATTRIBUTE_HEAD_ "<Attribute>"
#define ATTRIBUTE_TAIL_ "</Attribute>"
#define ENTITY_HEAD_ "<Entity>"
#define ENTITY_TAIL_ "</Entity>"
#define RELATION_HEAD_ "<Relation>"
#define RELATION_TAIL_ "</Relation>"
#define CONNECTION_HEAD_ "<Connection>"
#define CONNECTION_TAIL_ "</Connection>"
#define ID_HEAD_ "<Id>"
#define ID_TAIL_ "</Id>"
#define TEXT_HEAD_ "<Text>"
#define TEXT_TAIL_ "</Text>"
#define POSITION_X_HEAD_ "<positionX>"
#define POSITION_X_TAIL_ "</positionX>"
#define POSITION_Y_HEAD_ "<positionY>"
#define POSITION_Y_TAIL_ "</positionY>"
#define PRIMARYKEY_HEAD_ "<PrimaryKey>"
#define PRIMARYKEY_TAIL_ "</PrimaryKey>"
#define SOURCE_HEAD_ "<Source>"
#define SOURCE_TAIL_ "</Source>"
#define TARGET_HEAD_ "<Target>"
#define TARGET_TAIL_ "</Target>"
#define CANNOT_REDO_ "Cannot redo."
#define CANNOT_UNDO_ "Cannot undo."
#define REDO_SUCCEED_ "Redo succeed!"
#define UNDO_SUCCEED_ "Undo succeed!"
#define ENTITY_TO_RELATION_ '1'
#define RELATION_TO_ENTITY_ '2'
#define ATTRIBUTE_TYPE_ 'A'
#define CONNECTION_TYPE_ 'C'
#define ENTITY_TYPE_ 'E'
#define RELATION_TYPE_ 'R'
#define ERROR_TYPE_ 'X'
#define ATTRIBUTE_ "Attribute"
#define CONNECTION_ "Connection"
#define ENTITY_ "Entity"
#define RELATION_ "Relation"
#define LOADFILE_ADD_A_NODE_ 0
#define LOADFILE_CONNECT_TWO_NODES_ 1
#define LOADFILE_SET_A_PRIMARY_KEY_ 2
#define CASE_OF_LOAD_ER_DIAGRAM_FILE_ 1
#define CASE_OF_SAVE_ER_DIAGRAM_FILE_ 2
#define CASE_OF_ADD_A_NODE_ 3
#define CASE_OF_CONNECT_TWO_NODES_ 4
#define CASE_OF_DISPLAY_THE_CURRENT_DIAGRAM_ 5
#define CASE_OF_SET_A_PRIMARY_KEY_ 6
#define CASE_OF_DISPLAY_THE_TABLE_ 7
#define CASE_OF_DELETE_A_COMPONENT_ 8
#define CASE_OF_UNDO_ 9
#define CASE_OF_REDO_ 10
#define CASE_OF_EXIT_ 11
//graphicalUI
#define DEFAULT_WINDOWS_X 150
#define DEFAULT_WINDOWS_Y 150
#define WINDOWS_WIDTH 1080
#define WINDOWS_HEIGHT 600
#define SCENE_WIDTH_ 3000
#define SCENE_HEIGHT_ 3000
#define TABLE_WIDTH_ 330
#define WINDOW_TITLE_ "Entity Relation Diagramming Tool"
#define TABLE_TEXT_SIZE_ 10
#define TABLE_TEXT_FONT_ "Segoe Print"
#define COMPONENTS_TABLE_NAME_ "Components Table"
#define COMPONENTS_TABLE_CULUMN_SIZE_ 3
#define HEARER_ID_ "ID#"
#define HEARER_TYPE_ "Component Type"
#define HEARER_TEXT_ "Text"
#define TABLE_ID_COLUMN_ 0
#define TABLE_TYPE_COLUMN_ 1
#define TABLE_TEXT_COLUMN_ 2
#define FILE_ "&File"
#define OPEN_ "Open"
#define OPEN_ICON_PATH_ "Resources/folder.png"
#define OPEN_FILE_TITLE_ "Open ERD File"
#define DEFAULT_PATH_ "C:\\"
#define DEFAULT_TYPE_ "ERD File(*.erd)"
#define XML_TYPE_ "XML File(*.xml)"
#define SAVE_ "Save"
#define SAVE_ICON_PATH_ "Resources/disk.png"
#define SAVE_FILE_TITLE_ "Save ERD File"
#define SAVE_XML_FILE_TITLE_ "Save File As Xml"
#define SAVE_AS_XML_ "Save as xml"
#define FILE_ERD_ ".erd"
#define FILE_POS_ ".pos"
#define EXIT_ "Exit"
#define EXIT_ICON_PATH_ "Resources/exit.png"
#define EDIT_ "&Edit"
#define UNDO_ "&Undo"
#define REDO_ "&Redo"
#define DELETE_ "&Delete"
#define CUT_ "&Cut"
#define COPY_ "&Copy"
#define PASTE_ "&Paste"
#define UNDO_ICON_PATH_ "Resources/undo.png"
#define REDO_ICON_PATH_ "Resources/redo.png"
#define DELETE_ICON_PATH_ "Resources/delete.png"
#define CUT_ICON_PATH_ "Resources/cut.png"
#define COPY_ICON_PATH_ "Resources/copy.png"
#define PASTE_ICON_PATH_ "Resources/paste.png"
#define ADD_ "&Add"
#define HELP_ "&Help"
#define ABOUT_TOOL_ "About Entity Relation Diagramming Tool"
#define ABOUT_TOOL_TITLE_ "About Entity Relation Diagramming Tool"
#define ABOUT_TOOL_TEXT_ "Entity Relation Diagramming Tool 1.0\r\nAnthor:Nick Lu"
#define POINTER_ "Pointer"
#define PRIMARY_KEY_ "Primary Key"
#define TABLE_VIEWER_ "Table Viewer"
#define POINTER_ICON_PATH_ "Resources/pointer.png"
#define CONNECTION_ICON_PATH_ "Resources/Connection.png"
#define PRIMARY_ICON_PATH_ "Resources/primarykey.png"
#define PK_ICON_PATH_ "Resources/primary-key.png"
#define ATTRIBUTE_ICON_PATH_ "Resources/attribute.png"
#define ENTITY_ICON_PATH_ "Resources/entity.png"
#define RELATION_ICON_PATH_ "Resources/relation.png"
#define TABLE_VIEWER_ICON_PATH_ "Resources/database.png"
#define TOOL_BAR_FILE_ "Tool Bar File"
#define TOOL_BAR_Edit_ "Tool Bar Edit"
#define TOOL_BAR_ADD_ "Tool Bar Add"
#define ENTER_TEXT_TITLE_ "Enter text"
#define ENTER_TEXT_MESSAGE_ "Please enter the text:"
#define HALF_ 0.5
#define DOUBLE_ 2
#define PI_ 180.0
#define ADSORPTION_POINT_ -1
#define DEMO_NODE_ID_ -1
#define DEMO_NODE_TEXT_ "   "
#define DRAW_TEXT_FONT_ "Lucida Calligraphy"
#define DRAW_TEXT_SIZE_ 10
#define DRAW_ACTIVITY_PEN_WIDTH_ 2
#define DRAW_BOUNDING_PEN_WIDTH_ 4
#define DRAW_CONNECTION_PEN_WIDTH_ 2.3
#define DRAW_CONNECTION_TEXT_POS_Y_ 15
#define ADDED_BOUNDING_HORIZONTAL_ 10.0
#define ADDED_BOUNDING_VERTICAL_ 20.0
#define ATTRIBUTE_WIDTH_ 50
#define ATTRIBUTE_HEIGHT_ 25
#define ENTITY_WIDTH_ 50
#define ENTITY_HEIGHT_ 25
#define DEFAULT_CONNECTION_LENGTH_ 150
#define DEFAULT_CONNECTION_VIEWITEM_RX_ 50
#define DEFAULT_CONNECTION_VIEWITEM_RY_ 80
#define CONNECTION_VIEWITEM_COLUMN_DISTANCE_ 200
#define CAN_DO_DELETE_COMPONENT_ 1
