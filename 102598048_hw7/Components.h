#pragma once
#include <iostream>
#include <vector>
using namespace std;

class ComponentVisitor;
class Components{
	public:
		enum NodeType{
			typeDoesNotExist = 0, 
			connectionType = 1, 
			attributeType = 2, 
			entityType = 3,	
			relationType = 4
		};
		Components();
		virtual ~Components();
		void setId(int value);
		int getId();
		void setType(string name);
		string getType();
		void setText(string name);
		string getText();
		virtual void connectTo(Components* components);
		virtual void disconnectTo(Components* components);
		virtual vector<Components*> getConnection() = 0;
		virtual void accept(ComponentVisitor &visitor) = 0;
		virtual Components *clone(){return this;};
	private:
		int id;
		string type;
		string text;
	friend class CommandManagerTest;
};

