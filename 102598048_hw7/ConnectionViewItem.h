#pragma once
#include "ComponentViewItem.h"
#include "DefineSet.h"
#include <QtGui>
#include <QGraphicsLineItem>
#include <QGraphicsItem>
#include <QBrush>
#include <QPainter>
#include <QFont>

class ConnectionViewItem : public ComponentViewItem{
	public:
		ConnectionViewItem(size_t id, ComponentViewItem *startItem, ComponentViewItem *endItem, QString text);
		virtual ~ConnectionViewItem() {};
		void setTextAndId(QString text, size_t id);
		void setStartItem(ComponentViewItem *startItem);
		void setEndItem(ComponentViewItem *endItem);
		QPainterPath shape() const;
		void updateViewItem(QString text);
		void setPrimaryKeyViewItem();
		void resetPrimaryKeyViewItem();
	protected:
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);		
	private:
		void controlMovement();
		QPointF calculatorIntersectPoint(QPointF beginning, QPointF termination, ComponentViewItem *goalItem);
		void stretchEdge(qreal length);
		void extendBoundingRectangle(QPainter *painter);
		void addArrowHead(qreal length);
		QPainterPath addTextPath(qreal length, qreal beginning, qreal termination);
		void doRotate(qreal length, QPointF beginlPosition, QPointF goalPosition);
		ComponentViewItem *myStartItem;
		ComponentViewItem *myEndItem;
		QLineF line;
		QLineF finalLine;
		QString text;
		QPointF lastStartPoint;
		QPointF lastEndPoint;
		QPointF lastPoint;
		qreal rotateAngle,lastRotateAngel;
		QPointF textPosition;
};

