#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "ERModel.h"
#include "Node.h"
#include "Connection.h"
#include "Entity.h"
#include "Attributes.h"
#include "Relationship.h"

class ComponentFactory{
	public:
		ComponentFactory(void);
		~ComponentFactory(void);
		static Components* createComponent(int id, char type, string text);
};

