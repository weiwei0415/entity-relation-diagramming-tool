#include "Attributes.h"
#include "ComponentVisitor.h"

Attributes::Attributes(){
}

Attributes::~Attributes(){
}

Attributes::Attributes(int id, string name, string text){
	this->setId(id);
	this->setType(name);
	this->setText(text);	
}

vector<Components*> Attributes::getConnection(){
	return connections;
}

void Attributes::connectTo(Components* Components){
	connections.push_back(Components);
}


void Attributes::accept(ComponentVisitor &visitor){
	visitor.visit(this);
}

Components* Attributes::clone(){
	return new Attributes(getId(), getType(), getText());
}
