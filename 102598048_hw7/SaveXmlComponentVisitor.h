#pragma once
#include "ComponentVisitor.h"
#include "DefineSet.h"
#include "ERModel.h"
#include <sstream>

class ERModel;
class SaveXmlComponentVisitor : public ComponentVisitor{
	public:
		SaveXmlComponentVisitor(ERModel *model);
		~SaveXmlComponentVisitor();
		void visit(Attributes *attributeNode);
		void visit(Entity *entityNode);
		void visit(Relationship *relationNode);
		void visit(Connection *connections);
		string getFileString();
	private:
		ERModel *model;
		map<size_t, QPointF> position;
		string componentStream;
};

