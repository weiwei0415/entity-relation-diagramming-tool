#include "CommandManager.h"

CommandManager::CommandManager(){
}

CommandManager::~CommandManager(){
	while (!undoCmds.empty()){
		Command* command = undoCmds.top();
		undoCmds.pop();
		delete command;
	}
	while (!redoCmds.empty()){
		Command* command = redoCmds.top();
		redoCmds.pop();
		delete command;
	}
}

void CommandManager::execute(Command* command){
	command->execute();
	undoCmds.push(command);
	 //cleanup and release redoable commands
	while (!redoCmds.empty()) {
		Command* command = redoCmds.top();
		redoCmds.pop();
		delete command;
	}
}

void CommandManager::redo(){
	if (redoCmds.size() == ZERO_)
		return; // or throw exception
	Command* command = redoCmds.top();
	redoCmds.pop();
	command->execute(); // redo the command
	undoCmds.push(command);
}

void CommandManager::undo(){
	if (undoCmds.size() == ZERO_)
		return;
	Command* command = undoCmds.top();
	undoCmds.pop();
	command->unexecute(); // undo the command
	redoCmds.push(command);
}

string CommandManager::isRedoEmpty(){
	return redoCmds.empty() ? CANNOT_REDO_ : REDO_SUCCEED_ ;
}

string CommandManager::isUndoEmpty(){
	return undoCmds.empty() ? CANNOT_UNDO_ : UNDO_SUCCEED_ ;
}
