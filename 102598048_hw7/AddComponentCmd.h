#pragma once
#include "Command.h"

class AddComponentCmd : public Command{
	public:
		AddComponentCmd(ERModel *model, char nodeType, string text);
		~AddComponentCmd();
		void execute();
		void unexecute();
		void setId(size_t id);
};

