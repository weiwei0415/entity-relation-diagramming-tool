#pragma once
#include "ComponentViewItem.h"
#include "DefineSet.h"
#include "AttributeViewItem.h"
#include "EntityViewItem.h"
#include "RelationViewItem.h"
#include "ConnectionViewItem.h"

class ViewItemFactory{
	public:
		static ComponentViewItem* createViewItem(char inputType, QString text, size_t id);
};

