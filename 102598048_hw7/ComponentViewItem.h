#pragma once
#include <QGraphicsItem>
#include <QBrush>
#include <QPainter>
#include <QFont>
#include <string>
using namespace std;


class ComponentViewItem : public QGraphicsItem{
	public:
		ComponentViewItem(void);
		~ComponentViewItem(void);
		QPolygonF getPolygon() const;
		QRectF boundingRect() const;
		virtual QPainterPath shape() const = 0;
		virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) = 0;
		size_t getComponentId() {return this->id;};
		size_t id;
		virtual void updateViewItem(QString text) = 0;
		virtual void setPrimaryKeyViewItem() = 0;
		virtual void resetPrimaryKeyViewItem() = 0;
	protected:
		QPainterPath qPath;
		QFont qFont;
		
};

