#include "Components.h"

Components::Components(){
}

Components::~Components(){
}

void Components::setId(int value){  
	this->id = value;
}

int Components::getId(){  
	return id; 
}

void Components::setType(string name){ 
	type = name;
}

string Components::getType(){  
	return type; 
}

void Components::setText(string name){  
	text = name;
}

string Components::getText(){ 
	return text; 
}

void Components::connectTo(Components* components){
}

void Components::disconnectTo(Components* components){
}
