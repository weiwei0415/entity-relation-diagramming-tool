#include "Subject.h"
#include "Observer.h"

Subject::Subject(){
}

Subject::~Subject(){
}

void Subject::attach(Observer *observer){
	observers.push_back(observer);
}

void Subject::detach(Observer *observer){
	observers.remove(observer);
}

void Subject::notify(){
	for(list<Observer*>::iterator it = observers.begin() ; it != observers.end() ; it++)
		(*it)->updateChecked();
}