#pragma once
#include "ComponentVisitor.h"
#include "DefineSet.h"
#include <sstream>

class SaveComponentVisitor : public ComponentVisitor{
	public:
		SaveComponentVisitor();
		~SaveComponentVisitor();
		void visit(Attributes *attributeNode);
		void visit(Entity *entityNode);
		void visit(Relationship *relationNode);
		void visit(Connection *connections);
		string getFileString();
	private:
		string componentStream, connectionStream, primaryKeyStream;
};

