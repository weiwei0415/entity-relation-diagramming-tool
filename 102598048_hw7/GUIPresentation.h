#pragma once
#include "CommandManager.h"
#include "ERModel.h"
#include "Subject.h"
#include "StateFactory.h"
#include "AddComponentCmd.h"
#include "ConnectComponentsCmd.h"
#include "DeleteComponentCmd.h"
#include "EditTextCmd.h"
#include "SetPrimaryKeyCmd.h"
#include "CutComponentCmd.h"
#include "PasteComponentCmd.h"
#include "GUIMoveCmd.h"
#include <stack>

class State;
class GUIPresentation : public Subject{
	public:
		enum {addNodeState, connectState, pointState, setPrimaryKeyState};
		GUIPresentation(ERModel *model);
		~GUIPresentation();
		bool openErdFileAction(string fileName);
		bool openPosFileAction(string fileName);
		void saveErdFileAction(string fileName);
		void savePosFileAction(string fileName);
		void saveXmlFileAction(string fileName);
		vector<Nodes> getNodesData();
		vector<ConnectionNodes> getConnectionsData();
		vector<AllComponents> getAllComponents();
		vector<Components*> getComponents();
		void setMode(int mode);
		int getMode();
		void setAddNodeType(Components::NodeType type);
		Components::NodeType getAddNodeType();
		bool isActionPointerChecked();
		bool isActionConnectionChecked();
		bool isActionNewAttributeChecked();
		bool isActionNewEntityChecked();
		bool isActionNewRelationChecked();
		bool isActionSetPrimaryKeyChecked();
		bool canEnterText();
		void addNodeAction(Components::NodeType nodeType, string text);
		void addConnectionAction(size_t fromNode, size_t toNode, string inputType);
		void addConnectionForLoad(size_t id, size_t fromNode, size_t toNode, string inputType);
		void buildNodeAction(string text);
		bool canViewItemSelectable();
		bool canViewItemMoveable();
		bool canEdgeMoveable();
		bool canEdgeSelectable();
		//hw6 �s�W
		bool checkRedoStackEmpty();
		bool checkUndoStackEmpty();
		void deleteComponentAction(size_t id);
		void editTheTextAction(size_t id, string text);
		void setPrimaryKeyAction(size_t selectedId, int input);
		void redoAction();
		void undoAction();
		void setPostionMap(size_t id, QPointF pos);
		map<size_t, QPointF> getPositionMap();
		//hw7 �s�W
		stack<int> redoCommands;
		stack<int> undoCommands;
		int currentCount;
		void setCommands(int counts);
		void moveAction(vector<size_t> id, vector<QPointF> pos);
		void cutComponentAction(vector<int> itemsId);
		void pasteComponentAction();
		vector<Components*> getClone();
		void setClone(int id);
		void cleanClone();
		bool checkCloneEmpty();
	private:
		ERModel *model;
		int myMode;
		Components::NodeType addNodeTpye;
		State *nowState;
		CommandManager commandManager;
};
