#include "CutComponentCmd.h"

CutComponentCmd::CutComponentCmd(ERModel *model, vector<int> itemsId){
	this->model = model;
	this->cutId = itemsId;
	this->allComponents = this->model->getNodes();
}

CutComponentCmd::~CutComponentCmd(){
}

void CutComponentCmd::execute(){
	this->model->cleanClone();
	for(size_t i = 0 ; i < cutId.size() ; i++){
		for(size_t j = 0 ; j < allComponents.size() ; j++){
			if(allComponents[j]->getId() == cutId[i])
				this->model->setClone(cutId[i]);
		}
	}
	for(size_t i = 0 ; i < cutId.size() ; i++){
		DeleteComponentCmd* deleteCmd = new DeleteComponentCmd(this->model, cutId[i]);
		deleteCmd->execute();
		commands.push(deleteCmd);
	}
}

void CutComponentCmd::unexecute(){
	while(commands.size() != 0){
		Command* command = commands.top();
		commands.pop();
		command->unexecute();
	}
	this->model->cleanClone();
}