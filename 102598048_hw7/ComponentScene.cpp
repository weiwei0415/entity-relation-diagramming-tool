#include "ComponentScene.h"
const qreal zValue = 100;

ComponentScene::ComponentScene(GUIPresentation *presentations, QWidget *widget){
	qApp->installEventFilter(this);  //使用EventFilter
	isDemoNodeExists = false;
	this->presentation = presentations;
	this->parentWidget = widget;
	presentation->attach(this);
	this->updateChecked();
}

ComponentScene::~ComponentScene(void){
	this->cleanAllViewItems();
	presentation->detach(this);
}

void ComponentScene::updateChecked(){
	this->cleanAllViewItems();
	this->autoCreateViewItem();
	this->autoUpdateViewItem();
	this->changeViewItemState();
	this->changeEdgeItemState();
	this->update();
}

void ComponentScene::moveViewItemJudge(){	
	QList<QGraphicsItem*> moveItems = this->selectedItems();
	isChange = false;
	for(int i = 0 ; i < moveItems.size() ; i++){
		ComponentViewItem *item = static_cast<ComponentViewItem *> (moveItems[i]);
		itemsId.push_back(item->getComponentId());
		itemsPos.push_back(item->pos());
		if(presentation->getPositionMap()[item->getComponentId()] != item->pos())
			isChange = true;
	}
	if(isChange)
		this->presentation->moveAction(itemsId, itemsPos);	
	itemsId.clear();
	itemsPos.clear();
}

void ComponentScene::autoCreateViewItem(){
	vector<Nodes> nodes = this->presentation->getNodesData();
	vector<ConnectionNodes> connectionNodes = presentation->getConnectionsData();
	map<size_t, QPointF> pointOfId = this->presentation->getPositionMap();
	//新增NodeViewItem
	for (size_t i = 0 ; i < nodes.size() ; i++){
		this->nodeViewItems.push_back(ViewItemFactory::createViewItem(nodes[i].type, QString::fromLocal8Bit(nodes[i].text.c_str()), nodes[i].id));
		this->addItem(this->nodeViewItems.back());	
		this->nodeViewItems.back()->setPos(pointOfId[nodes[i].id]);
	}
	//新增ConnectionViewItem
	for(size_t i = 0 ; i < connectionNodes.size() ; i++){
		ComponentViewItem *startItem, *endItem;
		for(size_t j = 0 ; j < nodeViewItems.size() ; j++){
			if(nodeViewItems[j]->getComponentId() == connectionNodes[i].source)
				startItem = nodeViewItems[j];
			else if(nodeViewItems[j]->getComponentId() == connectionNodes[i].target)
				endItem = nodeViewItems[j];
		}
		ConnectionViewItem *tmpConnection = new ConnectionViewItem(connectionNodes[i].id, startItem, endItem, QString::fromLocal8Bit(connectionNodes[i].text.c_str()));
		this->connectionViewItems.push_back(tmpConnection);
		this->addItem(this->connectionViewItems.back());	
	}	
}

void ComponentScene::removeDemoItem(){
	if(isDemoNodeExists){
		for(size_t i = 0 ; i < items().size() ; i++){
			if(dynamic_cast <ComponentViewItem*> (this->items().at(i))->id == DEMO_NODE_ID_)
				this->removeItem(this->items().at(i));
		}
		isDemoNodeExists = false;
		this->updateChecked();
	}
}

bool ComponentScene::eventFilter(QObject *object, QEvent *events){
	QMouseEvent *mouseEvent = static_cast <QMouseEvent*> (events);
	QPointF pointF = mouseEvent->pos();
	if(events->type() == QEvent::MouseMove){
		if (presentation->getMode() == GUIPresentation::addNodeState){
			removeDemoItem();
			if(presentation->getAddNodeType() == Components::attributeType)
				demoViewItem = new AttributeViewItem(DEMO_NODE_TEXT_, DEMO_NODE_ID_);
			else if(presentation->getAddNodeType() == Components::entityType)
				demoViewItem = new EntityViewItem(DEMO_NODE_TEXT_, DEMO_NODE_ID_);
			else if(presentation->getAddNodeType() == Components::relationType)
				demoViewItem = new RelationViewItem(DEMO_NODE_TEXT_, DEMO_NODE_ID_);
			demoViewItem->setPos(pointF);
			this->addItem(demoViewItem);
			isDemoNodeExists = true;   //有demoItem存在
			this->updateChecked();
		}
	}
	return false;
}

void ComponentScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent){
	int mousePointX = mouseEvent->scenePos().x();
	int mousePointY = mouseEvent->scenePos().y();
	if(presentation->getMode() == GUIPresentation::addNodeState){
		delete demoViewItem;
		removeDemoItem();
		presentation->setMode(GUIPresentation::pointState);
		this->addNodeStateCreateViewItem(mouseEvent->scenePos());
	}
	else if(presentation->getMode() == GUIPresentation::connectState){
		for (size_t i = 0 ; i < nodeViewItems.size() ; i++){
			int nodePointX = nodeViewItems[i]->scenePos().x();
			int nodePointY = nodeViewItems[i]->scenePos().y();
			//判斷是否抓取到元件
			if(mousePointX > nodePointX - (ATTRIBUTE_HEIGHT_ / HALF_) && mousePointX < nodePointX + (ATTRIBUTE_HEIGHT_ / HALF_) &&
				mousePointY > nodePointY - (ATTRIBUTE_WIDTH_ / HALF_) && mousePointY < nodePointY + (ATTRIBUTE_WIDTH_ / HALF_))
					connectionNode.push_back(nodeViewItems[i]->getComponentId());	
		}
		if(connectionNode.empty())
			connectionNode.push_back(ERROR_COMMAND_);
	}
	else if(presentation->getMode() == GUIPresentation::setPrimaryKeyState){
		vector<Nodes> nodes = presentation->getNodesData();
		vector<ConnectionNodes> connectionNodes = presentation->getConnectionsData();
		vector<AllComponents> components = presentation->getAllComponents();
		for (size_t i = 0 ; i < nodes.size() ; i++){
			int nodePointX = nodeViewItems[i]->scenePos().x();
			int nodePointY = nodeViewItems[i]->scenePos().y();
			if(mousePointX > nodePointX - (ATTRIBUTE_HEIGHT_ / HALF_) && mousePointX < nodePointX + (ATTRIBUTE_HEIGHT_ / HALF_) &&
				mousePointY > nodePointY - (ATTRIBUTE_WIDTH_ / HALF_) && mousePointY < nodePointY + (ATTRIBUTE_WIDTH_ / HALF_)){
				if(nodes[i].type == ATTRIBUTE_TYPE_){
					for(size_t j = 0 ; j < connectionNodes.size() ; j++){
						if(connectionNodes[j].source == nodes[i].id && components[connectionNodes[j].target].type == ENTITY_){
							presentation->setPrimaryKeyAction(connectionNodes[j].target, nodes[i].id);	
							nodeViewItems[i]->setPrimaryKeyViewItem();
							break;
						}
						else if(connectionNodes[j].target == nodes[i].id && components[connectionNodes[j].source].type == ENTITY_){
							presentation->setPrimaryKeyAction(connectionNodes[j].source, nodes[i].id);
							nodeViewItems[i]->setPrimaryKeyViewItem();
							break;
						}
					}
					presentation->setMode(GUIPresentation::pointState);
				}
			}
		}
	}
	QGraphicsScene::mousePressEvent(mouseEvent);
}

void ComponentScene::mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent){
	if(presentation->getMode() == GUIPresentation::connectState){	
		for (size_t i = 0 ; i < nodeViewItems.size() ; i++)  //點選物件時不能讓物件移動
			nodeViewItems[i]->setFlag(QGraphicsItem::ItemIsMovable, false);
	}
	QGraphicsScene::mouseMoveEvent(mouseEvent);
}

void ComponentScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent){
	if(presentation->getMode() == GUIPresentation::pointState)
		moveViewItemJudge();
	if(presentation->getMode() == GUIPresentation::connectState){
		for (size_t i = 0 ; i < nodeViewItems.size() ; i++){
			int mousePointX = mouseEvent->scenePos().x();
			int mousePointY = mouseEvent->scenePos().y();
			int nodePointX = nodeViewItems[i]->scenePos().x();
			int nodePointY = nodeViewItems[i]->scenePos().y();
			//判斷是否抓取到元件
			if(mousePointX > nodePointX - (ATTRIBUTE_HEIGHT_ / HALF_) && mousePointX < nodePointX + (ATTRIBUTE_HEIGHT_ / HALF_) &&
				mousePointY > nodePointY - (ATTRIBUTE_WIDTH_ / HALF_) && mousePointY < nodePointY + (ATTRIBUTE_WIDTH_ / HALF_))
				connectionNode.push_back(nodeViewItems[i]->getComponentId());
		}
		if(connectionNode.size() == ONE_)
			connectionNode.push_back(ERROR_COMMAND_);
		this->checkConnection(connectionNode[ZERO_], connectionNode[ONE_], DEFAULT_);
		connectionNode.clear();
		presentation->setMode(GUIPresentation::pointState);	
		this->updateChecked();
	}
	QGraphicsScene::mouseReleaseEvent(mouseEvent);
}

void ComponentScene::checkConnection(size_t fromNode, size_t toNode, string text){
	string startNodeType = presentation->getComponents()[fromNode]->getType();
	string endNodeType = presentation->getComponents()[toNode]->getType();
	ComponentViewItem *startItem, *endItem;
	for(size_t i = 0 ; i < nodeViewItems.size() ; i++){
		if(nodeViewItems[i]->getComponentId() == fromNode)
			startItem = nodeViewItems[i];
		else if(nodeViewItems[i]->getComponentId() == toNode)
			endItem = nodeViewItems[i];
	}
	//抓取到兩個元件，以及確認是否已有連接線才執行
	if(fromNode != ERROR_COMMAND_ && toNode != ERROR_COMMAND_){
		if((startNodeType == ENTITY_ && endNodeType == ATTRIBUTE_) || (startNodeType == ATTRIBUTE_ && endNodeType == ENTITY_) ||
		(startNodeType == ENTITY_ && endNodeType == RELATION_) || (startNodeType == RELATION_ && endNodeType == ENTITY_)){
			if(presentation->getConnectionsData().empty()){
				presentation->addConnectionAction(fromNode, toNode, DEFAULT_);
				this->buildConnectionViewItem(presentation->getComponents().back()->getId(), fromNode, toNode, text);
			}
			else{
				vector<ConnectionNodes> connectionNodes = presentation->getConnectionsData();
				for(size_t i = 0 ; i < connectionNodes.size() ; i++){
					if((fromNode == connectionNodes[i].source && toNode == connectionNodes[i].target) ||
						(toNode == connectionNodes[i].source && fromNode == connectionNodes[i].target))
						break;
					if(i == connectionNodes.size()-ONE_){
						presentation->addConnectionAction(fromNode, toNode, DEFAULT_);
						this->buildConnectionViewItem(presentation->getComponents().back()->getId(), fromNode, toNode, text);	
					}
				}
			}	
		}
	}
}

void ComponentScene::createViewItemAfterOpenFile(){
	this->cleanAllViewItems();
	this->createNodeItemForLoadFile();
	this->createConnectionItemForLoadFile();
}

void ComponentScene::cleanAllViewItems(){
	qDeleteAll(nodeViewItems);
	qDeleteAll(connectionViewItems);
	nodeViewItems.resize(ZERO_);
	connectionViewItems.resize(ZERO_);
}

void ComponentScene::createNodeItemForLoadFile(){
	vector<Nodes> nodes = presentation->getNodesData();
	map<size_t, QPointF> positions = presentation->getPositionMap();
	for (size_t i = 0 ; i < nodes.size() ; i++){
		nodeViewItems.push_back(ViewItemFactory::createViewItem(nodes[i].type, QString::fromLocal8Bit(nodes[i].text.c_str()), nodes[i].id));
		this->addItem(nodeViewItems.back());	
	}
	for (size_t i = 0 ; i < nodeViewItems.size() ; i++)
		nodeViewItems[i]->setPos(positions[nodeViewItems[i]->getComponentId()]);
}

void ComponentScene::createConnectionItemForLoadFile(){
	vector<ConnectionNodes> connections = presentation->getConnectionsData();
	for (size_t i = 0, count = 0 ; i < connections.size() ; i++){
		vector<Nodes> nodes = presentation->getNodesData();
		size_t source, target;
		ConnectionViewItem *connection = this->buildConnectionViewItem(connections[i].id, connections[i].source, connections[i].target, connections[i].text); 
		this->setEdgeSituation(connections[i].source, connections[i].target, count, connections[i]);
	}
}

ConnectionViewItem* ComponentScene::buildConnectionViewItem(size_t id, size_t source, size_t target, string text){
	ComponentViewItem *startItem, *endItem;
	string startNodeType = presentation->getComponents()[source]->getType();
	string endNodeType = presentation->getComponents()[target]->getType();
	for(size_t i = 0 ; i < nodeViewItems.size() ; i++){
		if(nodeViewItems[i]->getComponentId() == presentation->getComponents()[source]->getId())
			startItem = nodeViewItems[i];
		else if(nodeViewItems[i]->getComponentId() == presentation->getComponents()[target]->getId())
			endItem = nodeViewItems[i];
	}
	if((startNodeType == ENTITY_ && endNodeType == ATTRIBUTE_) || (startNodeType == ATTRIBUTE_ && endNodeType == ENTITY_) ||
		(startNodeType == ENTITY_ && endNodeType == RELATION_) || (startNodeType == RELATION_ && endNodeType == ENTITY_)){
			ConnectionViewItem *tmpConnection = new ConnectionViewItem(id, startItem, endItem, QString::fromLocal8Bit(text.c_str()));
			presentation->addConnectionForLoad(id, source, target, text);   //新增connection
			connectionViewItems.push_back(tmpConnection);
			this->addItem(tmpConnection);
			ConnectionNodes lastEdge = presentation->getConnectionsData().back();
			tmpConnection->setZValue(zValue);
			tmpConnection->setTextAndId(QString::fromLocal8Bit(lastEdge.text.c_str()), lastEdge.id);
			return tmpConnection;	
	}
	return NULL;
}

void ComponentScene::setEdgeSituation(size_t source, size_t target, size_t &count, ConnectionNodes connection){
	size_t startIndex, endIndex;
	for(size_t i = 0 ; i < nodeViewItems.size() ; i++){
		if(nodeViewItems[i]->getComponentId() == presentation->getComponents()[source]->getId())
			startIndex = i;
		else if(nodeViewItems[i]->getComponentId() == presentation->getComponents()[target]->getId())
			endIndex = i;
	}
	if(connection.source == ADSORPTION_POINT_ && connection.target == ADSORPTION_POINT_){
		//若這條edge 是獨立的則從左上角依序往右排
		nodeViewItems[startIndex]->setPos(DEFAULT_CONNECTION_VIEWITEM_RX_ + CONNECTION_VIEWITEM_COLUMN_DISTANCE_ * (count), DEFAULT_CONNECTION_VIEWITEM_RY_);
		nodeViewItems[endIndex]->setPos(DEFAULT_CONNECTION_VIEWITEM_RX_ + CONNECTION_VIEWITEM_COLUMN_DISTANCE_ * (count) + DEFAULT_CONNECTION_LENGTH_, DEFAULT_CONNECTION_VIEWITEM_RY_);
		count++;
	}
	else if(connection.source != ADSORPTION_POINT_ && connection.target == ADSORPTION_POINT_){
		connectionViewItems.back()->setStartItem(findViewItemById(connection.source));
		nodeViewItems[endIndex]->setPos(findViewItemById(connection.source)->pos() + QPointF(ZERO_, DEFAULT_CONNECTION_LENGTH_));
	}
	else if(connection.source == ADSORPTION_POINT_ && connection.target != ADSORPTION_POINT_){
		nodeViewItems[startIndex]->setPos(nodeViewItems[endIndex]->pos() + QPointF(ZERO_, DEFAULT_CONNECTION_LENGTH_));
		connectionViewItems.back()->setEndItem(findViewItemById(connection.target));
	}
	else{
		connectionViewItems.back()->setStartItem(findViewItemById(connection.source));
		connectionViewItems.back()->setEndItem(findViewItemById(connection.target));
	}
}

ComponentViewItem * ComponentScene::findViewItemById(size_t id){
	for (size_t i = 0 ; i < nodeViewItems.size() ; i++){
		if (nodeViewItems[i]->getComponentId() == id) 
			return nodeViewItems[i];
	}
	return NULL;
}

void ComponentScene::addNodeStateCreateViewItem(QPointF eventPos){
	bool flag = true;
	QString text = QString(DEFAULT_);
	this->showEnterTextDialogIfNeeded(text, flag);
	if (!flag)
		return;
	presentation->buildNodeAction(text.toLocal8Bit().constData());
	Nodes lastNode = presentation->getNodesData().back();
	nodeViewItems.push_back(ViewItemFactory::createViewItem(lastNode.type, QString::fromLocal8Bit(lastNode.text.c_str()), lastNode.id));
	this->addItem(nodeViewItems.back());
	nodeViewItems.back()->setPos(eventPos);
	this->presentation->setPostionMap(nodeViewItems.back()->getComponentId(), eventPos);
	presentation->setMode(GUIPresentation::pointState);
}

void ComponentScene::showEnterTextDialogIfNeeded(QString &text, bool &flag){
	if (presentation->canEnterText())
		text = QInputDialog::getText(parentWidget, tr(ENTER_TEXT_TITLE_), tr(ENTER_TEXT_MESSAGE_), QLineEdit::Normal, DEFAULT_, &flag);
}

void ComponentScene::changeViewItemState(){
	for (size_t i = 0 ; i < nodeViewItems.size() ; i++){
		nodeViewItems[i]->setFlag(QGraphicsItem::ItemIsMovable, presentation->canViewItemMoveable());
		nodeViewItems[i]->setFlag(QGraphicsItem::ItemIsSelectable, presentation->canViewItemSelectable());
	}
}

void ComponentScene::changeEdgeItemState(){
	for (size_t i = 0 ; i < connectionViewItems.size() ; i++){
		connectionViewItems[i]->setFlag(QGraphicsItem::ItemIsMovable, presentation->canEdgeMoveable());
		connectionViewItems[i]->setFlag(QGraphicsItem::ItemIsSelectable, presentation->canEdgeSelectable());
	}
}

QVector <ComponentViewItem*> ComponentScene::getNodeViewItems(){
	return nodeViewItems;
}

void ComponentScene::autoUpdateViewItem(){
	vector<Nodes> nodes = this->presentation->getNodesData();
	vector<ConnectionNodes> connectionNodes = presentation->getConnectionsData();
	vector<Components*> components = presentation->getComponents();
	vector<AllComponents> allComponents = presentation->getAllComponents();
	//更新ViewItem名稱
	for (size_t i = 0 ; i < nodes.size() ; i++)
		nodeViewItems[i]->updateViewItem(QString::fromLocal8Bit(nodes[i].text.c_str()));
	//重設PrimaryKeyViewItem狀態
	for (size_t i = 0 ; i < nodes.size() ; i++){
		if(nodes[i].type == ATTRIBUTE_TYPE_)
			nodeViewItems[i]->resetPrimaryKeyViewItem();
	}
	//設定PrimaryKeyViewItem狀態
	setPrimaryKeyViewItem(nodes, connectionNodes, components, allComponents);
}

void ComponentScene::setPrimaryKeyViewItem(vector<Nodes> nodes, vector<ConnectionNodes> connectionNodes, 
	vector<Components*> components, vector<AllComponents> allComponents){
	for (size_t i = 0 ; i < nodes.size() ; i++){
		if(nodes[i].type == ATTRIBUTE_TYPE_){
			for(size_t j = 0 ; j < connectionNodes.size() ; j++){
				for(size_t k = 0 ; k < allComponents.size() ; k++){
					if(allComponents[k].id == connectionNodes[j].target){
						if(connectionNodes[j].source == nodes[i].id && allComponents[k].type == ENTITY_){
							Entity* entity = dynamic_cast <Entity*> (components[k]);
							vector<Components*> primaryKeys = entity->getAllPrimaryKey();
							for(size_t l = 0 ; l < primaryKeys.size() ; l++){
								if(primaryKeys[l]->getId() == nodes[i].id)
									nodeViewItems[i]->setPrimaryKeyViewItem();  //設定PrimaryKey Attribute元件加底線
							}
						}		
					}
					else if (allComponents[k].id == connectionNodes[j].source){
						if(connectionNodes[j].target == nodes[i].id && allComponents[k].type == ENTITY_){
							Entity* entity = dynamic_cast <Entity*> (components[k]);
							vector<Components*> primaryKeys = entity->getAllPrimaryKey();
							for(size_t l = 0 ; l < primaryKeys.size() ; l++){
								if(primaryKeys[l]->getId() == nodes[i].id)
									nodeViewItems[i]->setPrimaryKeyViewItem();  //設定PrimaryKey Attribute元件加底線
							}
						}
					}
				}					
			}
		}
	}	
}
