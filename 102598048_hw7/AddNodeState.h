#pragma once
#include "State.h"
class GUIPresentation;

class AddNodeState : public State{
	public:
		AddNodeState(GUIPresentation *present);
		~AddNodeState();
		bool canViewItemSelectable();
		bool canViewItemMovwble();
		bool canEdgeSelectable();
		bool canEdgeMoveble();
};

