#include "State.h"

State::State(GUIPresentation *present){
	presentation = present;
}

State::~State(){
}

bool State::canViewItemSelectable(){
	return false;
}

bool State::canViewItemMovwble(){
	return true;
}

bool State::canEdgeSelectable(){
	return false;
}

bool State::canEdgeMoveble(){
	return true;
}
