/****************************************************************************
** Meta object code from reading C++ file 'GraphicalUI.h'
**
** Created: Fri Jan 3 21:33:36 2014
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../GraphicalUI.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'GraphicalUI.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GraphicalUI[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      13,   12,   12,   12, 0x08,
      28,   12,   12,   12, 0x08,
      43,   12,   12,   12, 0x08,
      63,   12,   12,   12, 0x08,
      87,   12,   12,   12, 0x08,
      98,   12,   12,   12, 0x08,
     109,   12,   12,   12, 0x08,
     122,   12,   12,   12, 0x08,
     132,   12,   12,   12, 0x08,
     143,   12,   12,   12, 0x08,
     155,   12,   12,   12, 0x08,
     179,   12,   12,   12, 0x08,
     202,   12,   12,   12, 0x08,
     222,   12,   12,   12, 0x08,
     244,   12,   12,   12, 0x08,
     264,   12,   12,   12, 0x08,
     300,   12,   12,   12, 0x08,
     318,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_GraphicalUI[] = {
    "GraphicalUI\0\0slotOpenFile()\0slotSaveFile()\0"
    "slotSaveFileAsXml()\0slotSelectPointerMode()\0"
    "slotUndo()\0slotRedo()\0slotDelete()\0"
    "slotCut()\0slotCopy()\0slotPaste()\0"
    "slotAddConnectionMode()\0slotAddAttributeNode()\0"
    "slotAddEntityNode()\0slotAddRelationNode()\0"
    "slotAddPrimaryKey()\0"
    "slotSceneViewItemSelectionChanged()\0"
    "slotTableViewer()\0slotAboutTool()\0"
};

void GraphicalUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        GraphicalUI *_t = static_cast<GraphicalUI *>(_o);
        switch (_id) {
        case 0: _t->slotOpenFile(); break;
        case 1: _t->slotSaveFile(); break;
        case 2: _t->slotSaveFileAsXml(); break;
        case 3: _t->slotSelectPointerMode(); break;
        case 4: _t->slotUndo(); break;
        case 5: _t->slotRedo(); break;
        case 6: _t->slotDelete(); break;
        case 7: _t->slotCut(); break;
        case 8: _t->slotCopy(); break;
        case 9: _t->slotPaste(); break;
        case 10: _t->slotAddConnectionMode(); break;
        case 11: _t->slotAddAttributeNode(); break;
        case 12: _t->slotAddEntityNode(); break;
        case 13: _t->slotAddRelationNode(); break;
        case 14: _t->slotAddPrimaryKey(); break;
        case 15: _t->slotSceneViewItemSelectionChanged(); break;
        case 16: _t->slotTableViewer(); break;
        case 17: _t->slotAboutTool(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData GraphicalUI::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject GraphicalUI::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_GraphicalUI,
      qt_meta_data_GraphicalUI, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GraphicalUI::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GraphicalUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GraphicalUI::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GraphicalUI))
        return static_cast<void*>(const_cast< GraphicalUI*>(this));
    if (!strcmp(_clname, "Observer"))
        return static_cast< Observer*>(const_cast< GraphicalUI*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int GraphicalUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
