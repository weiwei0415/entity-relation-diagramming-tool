#include "AddNodeState.h"

AddNodeState::AddNodeState(GUIPresentation *present) : State(present){
}

AddNodeState::~AddNodeState(){
}

bool AddNodeState::canViewItemSelectable(){
	return false;
}

bool AddNodeState::canViewItemMovwble(){
	return false;
}

bool AddNodeState::canEdgeSelectable(){
	return false;
}

bool AddNodeState::canEdgeMoveble(){
	return false;
}
