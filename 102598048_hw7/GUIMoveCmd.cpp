#include "GUIMoveCmd.h"

GUIMoveCmd::GUIMoveCmd(ERModel *model, size_t id, QPointF pos){
	this->model = model;
	this->nodeId = id;
	this->start = this->model->getPositionMap()[id];
	this->terminal = pos;
}

GUIMoveCmd::~GUIMoveCmd(){
}

void GUIMoveCmd::execute(){
	this->model->setPostionMap(nodeId, terminal);
}

void GUIMoveCmd::unexecute(){
	this->model->setPostionMap(nodeId, start);
}
