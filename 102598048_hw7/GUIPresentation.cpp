#include "GUIPresentation.h"

GUIPresentation::GUIPresentation(ERModel *model){
	this->model = model;
	this->myMode = pointState;
	this->addNodeTpye = Components::connectionType;
	this->nowState = StateFactory::createState(myMode, this);
}

GUIPresentation::~GUIPresentation(){
	delete nowState;
}

bool GUIPresentation::openErdFileAction(string fileName){
	return this->model->loadFile(fileName);
}

bool GUIPresentation::openPosFileAction(string fileName){
	return this->model->loadFileAddPosition(fileName);
}

void GUIPresentation::saveErdFileAction(string fileName){
	this->model->saveErdFile(fileName);
}

void GUIPresentation::savePosFileAction(string fileName){
	this->model->savePosFile(fileName);
}

void GUIPresentation::saveXmlFileAction(string fileName){
	this->model->saveXmlFile(fileName);
}

vector<Nodes> GUIPresentation::getNodesData(){
	return this->model->getNodesData();
}

vector<ConnectionNodes> GUIPresentation::getConnectionsData(){
	return this->model->getConnectionsData();
}

vector<AllComponents> GUIPresentation::getAllComponents(){
	return this->model->getAllComponents();
}

vector<Components*> GUIPresentation::getComponents(){
	return this->model->getNodes();
}

void GUIPresentation::setMode(int mode){ 	
	this->myMode = mode; 
	delete nowState;
	nowState = StateFactory::createState(myMode, this);
	notify();
}

int GUIPresentation::getMode(){ 
	return this->myMode;
}

void GUIPresentation::setAddNodeType(Components::NodeType type){
	this->addNodeTpye = type;
}

Components::NodeType GUIPresentation::getAddNodeType(){
	return this->addNodeTpye;
}

bool GUIPresentation::isActionPointerChecked(){
	return this->myMode == pointState;
}

bool GUIPresentation::isActionConnectionChecked(){
	return this->myMode == connectState;
}

bool GUIPresentation::isActionNewAttributeChecked(){
	return this->myMode == addNodeState && this->addNodeTpye == Components::attributeType;
}

bool GUIPresentation::isActionNewEntityChecked(){
	return this->myMode == addNodeState && this->addNodeTpye == Components::entityType;
}

bool GUIPresentation::isActionNewRelationChecked(){
	return this->myMode == addNodeState && this->addNodeTpye == Components::relationType;
}

bool GUIPresentation::isActionSetPrimaryKeyChecked(){
	return this->myMode == setPrimaryKeyState;
}

bool GUIPresentation::canEnterText(){
	return this->model->canEnterText(this->addNodeTpye);
}

void GUIPresentation::addNodeAction(Components::NodeType nodeType, string text){
	char type;
	switch(nodeType){
		case Components::attributeType:
			type = ATTRIBUTE_TYPE_;
			break;
		case Components::entityType:
			type = ENTITY_TYPE_;
			break;
		case Components::relationType:
			type = RELATION_TYPE_;
			break;
		case Components::connectionType:
			type = CONNECTION_TYPE_;
			break;
	}
	this->setCommands(ONE_);
	commandManager.execute(new AddComponentCmd(model, type, text));
	notify();
}

void GUIPresentation::addConnectionAction(size_t fromNode, size_t toNode, string inputType){
	this->setCommands(ONE_);
	commandManager.execute(new ConnectComponentsCmd(model, fromNode, toNode, inputType));
	notify();
}

void GUIPresentation::addConnectionForLoad(size_t id, size_t fromNode, size_t toNode, string inputType){
	this->model->addConnection(id, model->getNodes()[fromNode], model->getNodes()[toNode], inputType);
}

void GUIPresentation::buildNodeAction(string text){
	this->addNodeAction(this->addNodeTpye, text);
}

bool GUIPresentation::canViewItemSelectable(){
	return nowState->canViewItemSelectable();
}

bool GUIPresentation::canViewItemMoveable(){
	return nowState->canViewItemMovwble();
}

bool GUIPresentation::canEdgeSelectable(){
	return nowState->canEdgeSelectable();
}

bool GUIPresentation::canEdgeMoveable(){
	return nowState->canEdgeMoveble();
}

bool GUIPresentation::checkRedoStackEmpty(){
	return this->commandManager.isRedoEmpty() == CANNOT_REDO_;
}

bool GUIPresentation::checkUndoStackEmpty(){
	return this->commandManager.isUndoEmpty() == CANNOT_UNDO_;
}

void GUIPresentation::deleteComponentAction(size_t id){
	this->commandManager.execute(new DeleteComponentCmd(this->model, id));
	notify();
}

void GUIPresentation::editTheTextAction(size_t id, string text){
	this->setCommands(ONE_);
	this->commandManager.execute(new EditTextCmd(this->model, id, text));
	notify();
}

void GUIPresentation::setPrimaryKeyAction(size_t selectedId, int input){
	this->setCommands(ONE_);
	this->commandManager.execute(new SetPrimaryKeyCmd(this->model, selectedId, input));
	notify();
}

void GUIPresentation::redoAction(){
	currentCount = redoCommands.top();
	undoCommands.push(redoCommands.top());
	redoCommands.pop();
	for(int i = 0 ; i < currentCount ; i++)
		this->commandManager.redo();
	notify();
}

void GUIPresentation::undoAction(){
	currentCount = undoCommands.top();
	redoCommands.push(undoCommands.top());
	undoCommands.pop();
	for(int i = 0 ; i < currentCount ; i++)
		this->commandManager.undo();
	notify();
}

void GUIPresentation::setPostionMap(size_t id, QPointF pos){
	this->model->setPostionMap(id, pos);
}

map<size_t, QPointF> GUIPresentation::getPositionMap(){
	return this->model->getPositionMap();
}

void GUIPresentation::setCommands(int counts){
	undoCommands.push(counts);
}

void GUIPresentation::moveAction(vector<size_t> id, vector<QPointF> pos){
	for(size_t i = 0 ; i < id.size() ; i++)
		this->commandManager.execute(new GUIMoveCmd(this->model, id[i], pos[i]));
	this->setCommands(id.size());
	notify();
}

void GUIPresentation::cutComponentAction(vector<int> itemsId){
	this->setCommands(ONE_);
	this->commandManager.execute(new CutComponentCmd(this->model, itemsId));
	notify();
}

void GUIPresentation::pasteComponentAction(){
	this->setCommands(ONE_);
	this->commandManager.execute(new PasteComponentCmd(this->model));
	notify();
}

vector<Components*> GUIPresentation::getClone(){
	return this->model->getClone();
}

void GUIPresentation::setClone(int id){
	this->model->setClone(id);
}

void GUIPresentation::cleanClone(){
	this->model->cleanClone();
}

bool GUIPresentation::checkCloneEmpty(){
	return this->model->getClone().empty();
}
