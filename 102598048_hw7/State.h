#pragma once
class GUIPresentation;

class State{
	public:
		State(GUIPresentation *present);
		virtual ~State();
		virtual bool canViewItemSelectable();
		virtual bool canViewItemMovwble();
		virtual bool canEdgeSelectable();
		virtual bool canEdgeMoveble();
	protected:
		GUIPresentation *presentation;
};

