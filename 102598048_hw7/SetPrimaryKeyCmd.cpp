#include "SetPrimaryKeyCmd.h"

SetPrimaryKeyCmd::SetPrimaryKeyCmd(ERModel *model, size_t selectedId, int input){
	this->model = model;
	this->entityId = selectedId;
	this->primaryKey = input;
}

SetPrimaryKeyCmd::~SetPrimaryKeyCmd(void){
}

void SetPrimaryKeyCmd::execute(){
	model->addSinglePrimaryKey(entityId, primaryKey);
}

void SetPrimaryKeyCmd::unexecute(){
	model->deleteSinglePrimaryKey(entityId, primaryKey);
}
