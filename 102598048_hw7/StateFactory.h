#pragma once
#include "State.h"
#include "AddNodeState.h"
#include "ConnectState.h"
#include "PointState.h"
#include "SetPrimaryKeyState.h"
#include "GUIPresentation.h"
class GUIPresentation;
class State;

class StateFactory{
	public:
		static State *createState(int mode, GUIPresentation *present);
};

