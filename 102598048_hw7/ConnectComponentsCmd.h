#pragma once
#include "Command.h"

class ConnectComponentsCmd : public Command{
	public:
		ConnectComponentsCmd(ERModel *model, size_t from, size_t to, string inputType);
		~ConnectComponentsCmd(void);
		void execute();
		void unexecute();
	private:
		size_t connectionId, from, to;
		string input;
};

