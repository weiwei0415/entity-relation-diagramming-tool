#include "AddComponentCmd.h"

AddComponentCmd::AddComponentCmd(ERModel *model, char nodeType, string text){
	this->model = model;
	//this->nodeId = this->model->getLastID() + ONE_;
	this->nodeId = this->model->autoCreateNodeId();
	this->nodeType = nodeType;
	this->nodeText = text;
}

AddComponentCmd::~AddComponentCmd(){
}

void AddComponentCmd::execute(){
	this->model->addNode(nodeType, nodeText, nodeId);
}

void AddComponentCmd::unexecute(){
	this->model->deleteComponent(nodeId);
}

void AddComponentCmd::setId(size_t id){
	this->nodeId = id;
}
