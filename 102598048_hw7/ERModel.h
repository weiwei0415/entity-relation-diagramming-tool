#pragma once
#include <QPoint>
#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <map>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <iomanip>
#include "DefineSet.h"
#include "Components.h"
#include "ComponentFactory.h"
#include "CNodeSortingCompareFunction.h"
#include "Entity.h"
#include "SaveComponentVisitor.h"
#include "SaveXmlComponentVisitor.h"
using namespace std;

typedef struct{
	char type;
	string text;
	size_t id;
} Nodes;

typedef struct{
	string text;
	size_t id;
	size_t target;
	size_t source;
} ConnectionNodes;

typedef struct{
	string type;
	string text;
	size_t id;
} AllComponents;

class ERModel{
	public:
		ERModel();
		~ERModel();
		bool loadFile(string fileName);
		void loadFileAddNodes(string connectionType, string action, string token);
		void loadFileAddConnections(string connectionType, string action, string token);
		void loadFileAddPrimaryKeys(string connectionType, string action, string token);
		void saveFile(string fileName);
		void saveFileAddNodes();
		void saveFileAddConnections();
		void saveFileAddPrimaryKeys();
		void addNode(char type, string text, size_t nodeId);
		void addConnection(size_t _ConnectionID, Components* fromNode, Components* toNode, string inputType);
		void addPrimaryKey(int selectedID, vector<int> input);
		void addSinglePrimaryKey(int selectedID, int input);
		void deleteSinglePrimaryKey(int selectedID, int input);
		void deleteComponent(int deleteID);
		void deleteConnection(int deleteID);
		void deleteNode(int deleteID);
		int checkCommand(string str);  //檢查Command
		char checkInput(string str);  //檢查新增類型
		bool checkID(size_t ID);  //檢查ID是否存在
		bool checkType(int fromID, int toID);
		bool checkCardinality(int fromID, int toID);
		bool existPathBetween(int firstID, int secondID);
		int getNodesSize();
		int getLastID();
		Entity* getPrimaryKey(int selectedID);	
		bool canEnterText(Components::NodeType type);
		size_t autoCreateNodeId();
		void editText(size_t id, string text);
		void setPostionMap(size_t id, QPointF pos);
		bool loadFileAddPosition(string fileName);
		void saveErdFile(string fileName);
		void savePosFile(string fileName);
		void saveXmlFile(string fileName);
		vector<Components*> getAllConnections();
		vector<Components*> getNodes();
		vector<Nodes> getNodesData();  
		vector<ConnectionNodes> getConnectionsData();
		vector<AllComponents> getAllComponents();
		map<size_t, QPointF> getPositionMap();
		int nodeID;
		size_t index;
		string inputType;
		string connectionType;
		string action;
		string token;	
		vector <Components*> clone;
		vector<Components*> getClone();
		void setClone(int id);
		void cleanClone();
	private:
		vector<Components*> tempComponents;
		vector<Components*> components;
		vector<string> stringToken;
		vector<int> nodeToken;
		fstream file;
		ofstream ofStream; 
		map<size_t, QPointF> pointOfId;	
	friend class CommandManagerTest;
	friend class AddComponentCmdTest;
	friend class ConnectComponentsCmdTest;
	friend class DeleteComponentCmdTest;
	friend class PresentationTest;
	friend class ERModelTest;
	friend class IntegrationTest;
};

