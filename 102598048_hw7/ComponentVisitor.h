#pragma once
#include "Components.h"
#include "Node.h"
#include "Attributes.h"
#include "Entity.h"
#include "Relationship.h"
#include "Connection.h"

class ComponentVisitor{
	public:
		ComponentVisitor();
		virtual ~ComponentVisitor();
		virtual void visit(Attributes *attributeNode) = 0;
		virtual void visit(Entity *entityNode) = 0;
		virtual void visit(Relationship *relationNode) = 0;
		virtual void visit(Connection *connections) = 0;
};