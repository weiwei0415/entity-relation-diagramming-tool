#pragma once
#include "Command.h"

class EditTextCmd : public Command{
	public:
		EditTextCmd(ERModel *model, size_t id, string text);
		~EditTextCmd(void);
		void execute();
		void unexecute();
	private:
		string newText;
};

