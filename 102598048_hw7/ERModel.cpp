#include "ERModel.h"

ERModel::ERModel(){
	this->nodeID = ZERO_;
}

ERModel::~ERModel(){
	for(size_t i = 0 ; i < components.size() ; i++)
		delete components[i];
	for(size_t i = 0 ; i < tempComponents.size() ; i++)
		delete tempComponents[i];
}

bool ERModel::loadFile(string fileName){
	this->nodeID = ZERO_;
	int cases = ZERO_;
	file.open(fileName, ios::in);
	for(size_t i = 0 ; i < components.size() ; i++)
		delete components[i];
	components.clear();
	nodeID = ZERO_;
	if (!file.is_open()) 
		return false;  //"File not found error!!\n";
	while (!file.eof()){
		getline(file, action);
		if (action.empty()){   //讀取到空白情況
			cases++;
			continue;
		}
		if(cases == LOADFILE_ADD_A_NODE_)   //新增節點情況
			loadFileAddNodes(connectionType, action, token);
		else if(cases == LOADFILE_CONNECT_TWO_NODES_)  //連接節點
			loadFileAddConnections(connectionType, action, token);
		else if(cases == LOADFILE_SET_A_PRIMARY_KEY_)  //新增PK情況
			loadFileAddPrimaryKeys(connectionType, action, token);
		stringToken.clear();
		nodeToken.clear();
	}
	file.close();
	return true;
}

void ERModel::loadFileAddNodes(string text, string action, string token){
	istringstream iStringStream(action);
	while(getline(iStringStream, token, COMMA_)){		
		if(token == action){  //只有C的情況
			stringToken.push_back(token);	
			break;
		}
		if(token[ZERO_] == SPACE_)
			token = token.substr(ONE_, token.size() - ONE_); //抓取空白後面的node名稱
		stringToken.push_back(token);
	}
	if(stringToken.size() != ONE_)
		addNode(stringToken[ZERO_][ZERO_], stringToken[ONE_], nodeID);
	else
		addNode(stringToken[ZERO_][ZERO_], DEFAULT_, nodeID);
	stringToken.clear();
	nodeToken.clear();
}

void ERModel::loadFileAddConnections(string connectionType, string action, string token){
	istringstream iStringStream(action);
	while(getline(iStringStream, action, SPACE_)){
		if(action != EMPTY_)
			stringToken.push_back(action);
	}
	iStringStream.clear();
	iStringStream.str(stringToken[ONE_]);  //綁定字串
	while(getline(iStringStream, action, COMMA_))   //以逗號切節點
		nodeToken.push_back(atoi(action.c_str()));
	if(components[atoi(stringToken[ZERO_].c_str())]->getText() == SINGLE_)
		inputType = SINGLE_;
	else if(components[atoi(stringToken[ZERO_].c_str())]->getText() == MULTI_)
		inputType = MULTI_;
	else
		inputType = DEFAULT_;
	addConnection(atoi(stringToken[ZERO_].c_str()), components[nodeToken[ZERO_]], components[nodeToken[ONE_]], inputType);	
	stringToken.clear();
	nodeToken.clear();
}

void ERModel::loadFileAddPrimaryKeys(string connectionType, string action, string token){
	istringstream iStringStream(action);
	while(getline(iStringStream, action, SPACE_)){
		if(action != EMPTY_)
			stringToken.push_back(action);
	}
	iStringStream.clear();
	iStringStream.str(stringToken[ONE_]);  //綁定字串
	while(getline(iStringStream, action, COMMA_))   //以逗號切節點
		nodeToken.push_back(atoi(action.c_str()));
	addPrimaryKey(atoi(stringToken[ZERO_].c_str()), nodeToken);
}

void ERModel::saveFile(string fileName){
	ofStream.open(fileName, ios::out); 
	saveFileAddNodes();
	saveFileAddConnections();
	saveFileAddPrimaryKeys();
	ofStream.close();
}

void ERModel::saveFileAddNodes(){
	for each(Components* comp in components){   //component
		ofStream << comp->getType()[ZERO_];
		if(comp->getType()[ZERO_] == CONNECTION_TYPE_ && comp->getText() == DEFAULT_)
			ofStream << endl;
		else
			ofStream << COMMA_ << SPACE_ << comp->getText() << endl;		
	}
	ofStream << endl;
}

void ERModel::saveFileAddConnections(){
	for each(Components* comp in getAllConnections()){   //connection
		ofStream << comp->getId() << SPACE_;
		for each(Components* node in comp->getConnection())
			tempComponents.push_back(node);
		ofStream << tempComponents[ZERO_]->getId() << COMMA_ << tempComponents[ONE_]->getId() << endl;
		tempComponents.clear();		
	}
	ofStream << endl;
}

void ERModel::saveFileAddPrimaryKeys(){
	for each(Components* comp in components){   //primary key
		if(comp->getType()[ZERO_] == ENTITY_TYPE_){
			Entity* entity = dynamic_cast<Entity*> (comp); //先轉型為Entity型態
			for each(Entity* node in entity->getAllPrimaryKey())
				tempComponents.push_back(node);
			if(tempComponents.size() != 0){
				ofStream << comp->getId() << SPACE_;
				for(size_t i = 0 ; i < tempComponents.size() ; i++){
					ofStream << tempComponents[i]->getId();
					if(i < tempComponents.size()-ONE_)
						ofStream << COMMA_;
				}
				ofStream << endl;
			}
			tempComponents.clear();	
		}
	}
}

void ERModel::addNode(char type, string text, size_t nodeId){
	ComponentFactory *factory = new ComponentFactory();
	components.push_back(factory->createComponent(nodeId, type, text));
	sort(components.begin(), components.end(), CSortNodeCompareFunction());
	nodeID++;
	delete factory;
}

void ERModel::addConnection(size_t connectionID, Components* fromNode, Components* toNode, string inputType){
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == connectionID){
			components[i]->connectTo(fromNode);
			components[i]->connectTo(toNode);
			fromNode->connectTo(components[i]);
			toNode->connectTo(components[i]);
		}
	}	
}

void ERModel::deleteComponent(int deleteID){
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == deleteID)
			index = i;
	}
	if(!components.empty()){
		if(components[index]->getType()[ZERO_] == CONNECTION_TYPE_)
			deleteConnection(deleteID);
		else
			deleteNode(deleteID);
	}
}

void ERModel::deleteConnection(int deleteID){
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == deleteID)
			index = i;
	}
	for each(Components* comp in getAllConnections()){
		if(components[index]->getId() == comp->getId()){
			for each(Components* node in comp->getConnection())
				tempComponents.push_back(node);				
			tempComponents.clear();
			for(size_t i = 0 ; i < components.size() ; i++){
				if(components[i]->getId() == comp->getId()){
					components.erase(components.begin() + i);
					sort(components.begin(), components.end(), CSortNodeCompareFunction());
					nodeID--;
				}
			}
			break;
		}
	}
}

void ERModel::deleteNode(int deleteID){
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == deleteID)
			index = i;
	}
	for each(Components* comp in getAllConnections()){
		for each(Components* node in comp->getConnection())
			tempComponents.push_back(node);
		if(tempComponents[ZERO_]->getId() == components[index]->getId()
			|| tempComponents[ONE_]->getId() == components[index]->getId()){
			for(size_t i = 0 ; i < components.size() ; i++){
				if(components[i]->getId() == comp->getId()){
					components.erase(components.begin() + i);
					sort(components.begin(), components.end(), CSortNodeCompareFunction());
					nodeID--;
				}
			}
		}	
		tempComponents.clear();	
	}	
	for(size_t i=0 ; i < components.size() ; i++){
		if(components[i]->getId() == components[index]->getId()){
			components.erase(components.begin() + i);
			sort(components.begin(), components.end(), CSortNodeCompareFunction());
			nodeID--;
		}
	}	
}

void ERModel::addPrimaryKey(int selectedID, vector<int> input){
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == selectedID){
			Entity* entity = dynamic_cast <Entity*> (components[i]);
			entity->resetPrimaryKey(); 
			for(size_t j = 0 ; j < input.size() ; j++){
				for(size_t k = 0 ; k < components.size() ; k++){
					if(components[k]->getId() == input[j])
						entity->setPrimaryKey(components[k]);
				}
			}
		}
	} 	
}

void ERModel::addSinglePrimaryKey(int selectedID, int input){
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == selectedID){
 			Entity* entity = dynamic_cast <Entity*> (components[i]);
			for(size_t j = 0 ; j < components.size() ; j++){
				if(components[j]->getId() == input)
					entity->setPrimaryKey(components[j]);    
			}
		}
	}
}

void ERModel::deleteSinglePrimaryKey(int selectedID, int input){
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == selectedID){
			Entity* entity = dynamic_cast <Entity*> (components[i]);
			entity->deletePrimaryKey();
		}
	}
}

int ERModel::checkCommand(string str){
	if(atoi(str.c_str()) == ZERO_ && str != ZERO_OF_STRING_TYPE_)  //判斷輸入非int型別的狀況
		return ERROR_COMMAND_;
	else
		return atoi(str.c_str());
}

char ERModel::checkInput(string str){
	if(str.size() != ONE_)
		return ERROR_TYPE_;
	else
		return str[ZERO_];
}

bool ERModel::checkID(size_t ID){  //確認ID是否存在
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == ID)
			return false;
	}
	return true;
}

bool ERModel::checkType(int fromID, int toID){ //確認型別是否相同
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == fromID)
			fromID = i;
		else if(components[i]->getId() == toID)
			toID = i;
	}
	if(components[fromID]->getType() == components[toID]->getType()) //型別相同
		return true;
	else if(components[fromID]->getType().compare(RELATION_) == ZERO_ 
		&& components[toID]->getType().compare(ATTRIBUTE_) == ZERO_) //fromID is Relation and toID is Attribute
		return true;
	else if(components[fromID]->getType().compare(ATTRIBUTE_) == ZERO_ 
		&& components[toID]->getType().compare(RELATION_) == ZERO_) //fromID is Relation and toID is Attribute
		return true;
	else if(components[fromID]->getType().compare(CONNECTION_) == ZERO_ 
		|| components[toID]->getType().compare(CONNECTION_) == ZERO_) //fromID is Connection or toID is Connection
		return true;
	else
		return false;
}

bool ERModel::checkCardinality(int fromID, int toID){  //確認是否為Entity與Relation連接
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == fromID)
			fromID = i;
		else if(components[i]->getId() == toID)
			toID = i;
	}
	if(components[fromID]->getType().compare(ENTITY_) == ZERO_ 
		&& components[toID]->getType().compare(RELATION_) == ZERO_) //fromID is Entity and toID is Relation
		return true;
	else if(components[fromID]->getType().compare(RELATION_) == ZERO_
		&& components[toID]->getType().compare(ENTITY_) == ZERO_) //fromID is Relation and toID is Entity
		return true;
	else
		return false;
}

bool ERModel::existPathBetween(int firstID, int secondID){
	for each(Components* comp in getAllConnections()){
		vector<int> temp;
		for each(Components* node in comp->getConnection())
			temp.push_back(node->getId());
		if(temp[NODE_ONE_] == firstID && temp[NODE_TWO_] == secondID)
			return true;
		else if(temp[NODE_ONE_] == secondID && temp[NODE_TWO_] == firstID)
			return true;
		temp.clear();
	}
	return false;
}

vector <Components*> ERModel::getAllConnections(){
	vector <Components*> temp;
	for each (Components* comp in components){
		if(comp->getType() == CONNECTION_)
			temp.push_back(comp);
	}
	return temp;
}

vector <Components*> ERModel::getNodes(){
	return components;
}

int ERModel::getNodesSize(){
	return components.size();
}

int ERModel::getLastID(){
	return nodeID - ONE_;
}

Entity* ERModel::getPrimaryKey(int selectedID){
	Entity* entity = dynamic_cast <Entity*> (components[selectedID]);
	return entity;
}

vector<Nodes> ERModel::getNodesData(){
	vector<Nodes> nodes;
	for (size_t i = 0 ; i < this->components.size() ; i++){
		Nodes temp = {this->components[i]->getType()[ZERO_], this->components[i]->getText(), this->components[i]->getId()};
		if (this->components[i]->getType() != CONNECTION_) 
			nodes.push_back(temp);
	}
	return nodes;
}

vector<ConnectionNodes> ERModel::getConnectionsData(){
	vector<ConnectionNodes> connections;
	for (size_t i = 0 ; i < this->components.size() ; i++){
		if (this->components[i]->getType() == CONNECTION_){
			size_t targetId = this->components[i]->getConnection()[ZERO_]->getId();
			size_t sourceId = this->components[i]->getConnection()[ONE_]->getId();
			ConnectionNodes temp = {this->components[i]->getText(), this->components[i]->getId(), targetId, sourceId};
			connections.push_back(temp);
		}
	}
	return connections;
}

vector<AllComponents> ERModel::getAllComponents(){
	vector<AllComponents> allComponents;
	for (size_t i = 0 ; i < this->components.size() ; i++){
		AllComponents temp = {this->components[i]->getType(), this->components[i]->getText(), this->components[i]->getId()};
		allComponents.push_back(temp);
	}
	return allComponents;
}

bool ERModel::canEnterText(Components::NodeType type){
	return type == Components::attributeType || type == Components::entityType || type == Components::relationType ? true : false;
}

size_t ERModel::autoCreateNodeId(){
	return this->components.empty() ? ZERO_ : this->components.back()->getId() + ONE_;
}

void ERModel::editText(size_t id, string text){
	this->getNodes()[id]->setText(text);
}

void ERModel::setPostionMap(size_t id, QPointF pos){
	this->pointOfId[id] = pos;
}

map<size_t, QPointF> ERModel::getPositionMap(){
	return pointOfId;
}

bool ERModel::loadFileAddPosition(string fileName){
	file.open(fileName, ios::in);
	if (!file.is_open()) 
		return false;  //"File not found error!!\n";
	while (!file.eof()){
		vector<Nodes> nodes = this->getNodesData();
		for(size_t i = 0 ; i < nodes.size() ; i++){
			getline(file, action);
			istringstream iStringStream(action);
			while(getline(iStringStream, token, SPACE_)){
				if(token != EMPTY_)
					stringToken.push_back(token);
			}
			iStringStream.clear();
			this->setPostionMap(nodes[i].id, QPointF(atoi(stringToken[ZERO_].c_str()), atoi(stringToken[ONE_].c_str())));
			stringToken.clear();
		}
	}
	file.close();
	return true;
}

void ERModel::saveErdFile(string fileName){
	ofStream.open(fileName, ios::out); 
	SaveComponentVisitor visitor;
	for (size_t i = 0 ; i < this->components.size() ; i++)
		this->components[i]->accept(visitor);
	ofStream << visitor.getFileString().c_str();
	ofStream.close();
}

void ERModel::savePosFile(string fileName){
	ofStream.open(fileName, ios::out); 
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getType() != CONNECTION_){
			if(i > ZERO_)
				ofStream << endl;
			ofStream << pointOfId[i].x() << SPACE_ << pointOfId[i].y();
		}
	}
	ofStream.close();
}

void ERModel::saveXmlFile(string fileName){
	ofStream.open(fileName, ios::out);
	SaveXmlComponentVisitor visitor(this);
	ofStream << XML_TITLE_ << endl;
	ofStream << ERD_HEAD_ << endl;
	for (size_t i = 0 ; i < this->components.size() ; i++)
		this->components[i]->accept(visitor);
	ofStream << visitor.getFileString().c_str();
	ofStream << ERD_TAIL_ << endl;
	ofStream.close();
}

void ERModel::setClone(int id){
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getId() == id)
			clone.push_back(components[i]->clone());
	}
}

vector<Components*> ERModel::getClone(){
	return clone;
}

void ERModel::cleanClone(){
	clone.clear();
}
