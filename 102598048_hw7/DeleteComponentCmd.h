#pragma once
#include "Command.h"

class DeleteComponentCmd : public Command{
	public:
		DeleteComponentCmd(ERModel *model, int deleteID);
		~DeleteComponentCmd(void);
		void execute();
		void unexecute();
	private:
		Components* component;
		vector<Components*> allComponents;
		vector <Components*> connectors;
		vector <Components*> tempComponents;
		vector <Components*> connectionVector;
		vector <Components*> primarykeys;
		vector <int> input;
};

