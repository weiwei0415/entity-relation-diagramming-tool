#include "ComponentFactory.h"

ComponentFactory::ComponentFactory(void){
}

ComponentFactory::~ComponentFactory(void){
}

Components* ComponentFactory::createComponent(int id, char type, string text){
	switch(type){
		case ATTRIBUTE_TYPE_:
			return new Attributes(id, ATTRIBUTE_, text);
		case ENTITY_TYPE_:
			return new Entity(id, ENTITY_, text);
		case RELATION_TYPE_:
			return new Relationship(id, RELATION_, text);
		case CONNECTION_TYPE_:
			return new Connection(id, CONNECTION_, text);
		default:
			return ZERO_;
	}
}
