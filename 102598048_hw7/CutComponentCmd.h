#pragma once
#include "Command.h"
#include "CommandManager.h"
#include "DeleteComponentCmd.h"

class CutComponentCmd : public Command{
	public:
		CutComponentCmd(ERModel *model, vector<int> itemsId);
		~CutComponentCmd();
		void execute();
		void unexecute();
	private:
		CommandManager commandManager;
		vector <int> cutId;
		vector <Components*> allComponents;
		stack <Command*> commands;
};