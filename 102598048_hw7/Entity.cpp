#include "Entity.h"
#include "ComponentVisitor.h"

Entity::Entity(void){
}

Entity::~Entity(void){
}

Entity::Entity(int id, string name, string text){
	this->setId(id);
	this->setType(name);
	this->setText(text);
}

vector<Components*> Entity::getConnection(){
	return connections;
}

void Entity::connectTo(Components* Components){
	connections.push_back(Components);
}

vector<Components*> Entity::getAllPrimaryKey(){
	return primaryKey;
}

void Entity::setPrimaryKey(Components* key){
	primaryKey.push_back(key);
}

void Entity::deletePrimaryKey(){
	primaryKey.pop_back();
}

void Entity::resetPrimaryKey(){
	primaryKey.clear();
}

void Entity::accept(ComponentVisitor &visitor){
	visitor.visit(this);
}

Components* Entity::clone(){
	return new Entity(getId(), getType(), getText());
}
