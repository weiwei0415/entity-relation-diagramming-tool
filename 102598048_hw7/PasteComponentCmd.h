#pragma once
#include "Command.h"
#include "CommandManager.h"
#include "AddComponentCmd.h"

class PasteComponentCmd : public Command{
	public:
		PasteComponentCmd(ERModel *model);
		~PasteComponentCmd();
		void execute();
		void unexecute();
	private:
		CommandManager commandManager;
		vector<Components*> clone;
		stack <Command*> commands;
		vector<int> itemId;
};

