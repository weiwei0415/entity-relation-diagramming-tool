#include "StateFactory.h"


State* StateFactory::createState(int mode, GUIPresentation *present){
	switch(mode){	
		case GUIPresentation::addNodeState:
			return new AddNodeState(present);
			break;
		case GUIPresentation::connectState:
			return new ConnectState(present);
			break;
		case GUIPresentation::pointState:
			return new PointState(present);
			break;
		case GUIPresentation::setPrimaryKeyState:
			return new SetPrimaryKeyState(present);
			break;
	}
	return NULL;
}
