#include "Relationship.h"
#include "ComponentVisitor.h"

Relationship::Relationship(void){
}

Relationship::~Relationship(void){
}

Relationship::Relationship(int id, string name, string text){
	this->setId(id);
	this->setType(name);
	this->setText(text);
}

vector<Components*> Relationship::getConnection(){
	return connections;
}

void Relationship::connectTo(Components* Components){
	connections.push_back(Components);
}


void Relationship::accept(ComponentVisitor &visitor){
	visitor.visit(this);
}

Components* Relationship::clone(){
	return new Relationship(getId(), getType(), getText());
}
