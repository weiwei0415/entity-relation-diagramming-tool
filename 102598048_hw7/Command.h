#pragma once
#include "ERModel.h"
#include <string>
using namespace std;

class Command{
	public:
		Command();
		virtual ~Command();
		virtual void execute() = 0;
		virtual void unexecute() = 0;
	protected:
		ERModel *model;
		size_t nodeId;
		char nodeType;
		string nodeText;
};
