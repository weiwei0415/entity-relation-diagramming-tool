#pragma once
#include "Command.h"

class SetPrimaryKeyCmd : public Command{
	public:
		SetPrimaryKeyCmd(ERModel *model, size_t selectedId, int input);
		~SetPrimaryKeyCmd(void);
		void execute();
		void unexecute();
	private:
		size_t entityId;
		int primaryKey;
};

