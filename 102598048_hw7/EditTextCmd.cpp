#include "EditTextCmd.h"

EditTextCmd::EditTextCmd(ERModel *model, size_t id, string text){
	this->model = model;
	this->nodeId = id;
	this->nodeText = this->model->getNodes()[id]->getText();
	this->newText = text;
}


EditTextCmd::~EditTextCmd(void){
}

void EditTextCmd::execute(){
	this->model->editText(nodeId, newText);
}

void EditTextCmd::unexecute(){
	this->model->editText(nodeId, nodeText);
}