#pragma once
#include "Command.h"
#include "CommandManager.h"
#include "DeleteComponentCmd.h"

class CopyComponentCmd : public Command{
	public:
		CopyComponentCmd(ERModel *model, vector<int> itemsId);
		~CopyComponentCmd();
		void execute();
		void unexecute();
	private:
		CommandManager commandManager;
		vector <int> cutId;
		vector <Components*> allComponents;
		stack <Command*> commands;
};