#pragma once
#include "ComponentViewItem.h"
#include "DefineSet.h"
#include <QGraphicsItem>
#include <QBrush>
#include <QPainter>
#include <QFont>

class EntityViewItem : public ComponentViewItem{
	public:
		EntityViewItem(QString text, size_t id);
		virtual ~EntityViewItem();
		QPainterPath shape() const;
		void updateViewItem(QString text);
		void setPrimaryKeyViewItem();
		void resetPrimaryKeyViewItem();
	protected:
		void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
	private:
		QString text;
		QPointF textPosition;
};

