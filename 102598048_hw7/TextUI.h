#pragma once
#include <iostream>
#include <string>
#include "DefineSet.h"
#include "ERModel.h"
#include "Presentation.h"
using namespace std;

class TextUI{
	public:
		TextUI(Presentation *presentation);
		~TextUI(void);
		void displayMenu();
		void processCommand();
		void processType(char nodeType);  //Type選單
		void loadERDiagramFile();
		void saveERDiagramFile();
		void addANode();
		void connectTwoNodes();
		void setAPrimaryKey();
		void deleteAComponent();
		void checkConnectState(int connectFrom, int connectTo);
		int checkExistNode(int inputType);
		void checkEntity(int index);
		void enterIdsOfAttributes();
		void showDiagrams(char input);
		void judgeCaseToPrint(char input);
		void printDiagram(int index);
		void showConnections();
		void showTable();
		void printPK();
		void printAttribute();
		void printFK(int index);
		void checkRelationOfEntity(int nodeOne, int nodeTwo, int entityID, int connectID);		
		void checkAnotherEntity(int entityID, int relationID, char connectDirection);
		void printForeignKey(int entityID);
		Presentation* presentation;
		Entity* entity;
		string tempString;
		int tempInt;
		int inputCommand;
		char inputType;
		string inputName;
		int selectedID;  //選擇的entity ID
		int deleteID;    //要刪除的ID
		string token;	
		int connectFrom;
		int connectTo;
		bool isEnd;  //判斷執行的流程區塊是否結束
		bool isPrimaryKey; //判斷是否為PrimaryKey
	private:
		vector <int> tempNode; 
		vector <int> idsOfAttributes;
		vector <Components*> tempAttribute;
};

