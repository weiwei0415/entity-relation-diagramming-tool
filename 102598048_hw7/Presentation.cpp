#include "Presentation.h"
#include "ERModel.h"

Presentation::Presentation(ERModel* model){
	this->model = model;
	attributeCount = ZERO_;
	entityCount = ZERO_;
}

Presentation::~Presentation(void){
}

bool Presentation::loadFileAction(string fileName){
	return model->loadFile(fileName);
}

void Presentation::saveFileAction(string fileName){
	return model->saveFile(fileName);
}

void Presentation::addNodeAction(char nodeType, string text){
	commandManager.execute(new AddComponentCmd(model, nodeType, text));
	addNodeCount(model->getNodes()[model->getNodesSize()-ONE_]->getType()[ZERO_]);
	nodeCount++;  //Node數量+1	
}

void Presentation::deleteNodeAction(size_t delID){
	deleteNodeCount(model->getNodes()[delID]->getType()[ZERO_]);
	commandManager.execute(new DeleteComponentCmd(model, delID));
	nodeCount--;  //Node數量-1
}

void Presentation::connectNodeAction(int fromID, int toID, string inputType){
	for(int i = 0 ; i < model->getNodesSize() ; i++){
		if(model->getNodes()[i]->getId() == fromID)
			fromID = i;
		else if(model->getNodes()[i]->getId() == toID)
			toID = i;
	}
	commandManager.execute(new ConnectComponentsCmd(model, fromID, toID, inputType));
	nodeCount++;  //Node數量+1	
}

void Presentation::addNodeCount(char inputType){
	switch(inputType){
		case ATTRIBUTE_TYPE_:
			attributeCount++;
			break;
		case ENTITY_TYPE_:
			entityCount++;
			break;
	}
}

void Presentation::deleteNodeCount(char inputType){
	switch(inputType){
		case ATTRIBUTE_TYPE_:
			attributeCount--;
			break;
		case ENTITY_TYPE_:
			entityCount--;
			break;
	}
}

void Presentation::addAttributeOfEntity(int index){
	attributeOfEntity.clear();
	for each(Components* comp in model->getAllConnections()){
		for each(Components* node in comp->getConnection())
			tempNode.push_back(node->getId());
		checkAttributeOfEntity(tempNode[ZERO_], tempNode[ONE_], index);
		tempNode.clear();
	}
	sort(attributeOfEntity.begin(), attributeOfEntity.end());
}

void Presentation::addPrimaryKey(int selectedID, vector<int> input){
	model->addPrimaryKey(selectedID, input);
}

string Presentation::redo(){
	string message = commandManager.isRedoEmpty();
	commandManager.redo();
	return message;
}

string Presentation::undo(){
	string message = commandManager.isUndoEmpty();
	commandManager.undo();
	return message;
}

bool Presentation::isNodesEmpty(){
	return model->getNodes().empty();
}

bool Presentation::isEntityEmpty(){
	return entityCount == ZERO_;
}

bool Presentation::isAttributeOfEntityEmpty(){
	return attributeOfEntity.size() == ZERO_;
}

bool Presentation::isBelongToEntity(vector<int> input){
	for(size_t i = 0 ; i < input.size() ; i++){
		for(size_t j = 0 ; j < attributeOfEntity.size() ; j++){
			if(input[i] == attributeOfEntity[j])  //找到屬性的情況
				break;
			if(j == attributeOfEntity.size()-ONE_ ) //皆未找到的情況			
				return false;
		}
	}	
	return true;
}

int Presentation::checkCommand(string str){
	return model->checkCommand(str);
}

char Presentation::checkInput(string str){
	return model->checkInput(str);
}

bool Presentation::checkID(size_t ID){  //確認ID是否存在
	return model->checkID(ID);
}

bool Presentation::checkType(int fromID, int toID){ //確認型別是否相同
	return model->checkType(fromID, toID);
}

bool Presentation::checkCardinality(int fromID, int toID){  //確認是否為Entity與Relation連接
	return model->checkCardinality(fromID, toID);
}

void Presentation::checkAttributeOfEntity(int nodeOne, int nodeTwo, int index){
	if(nodeOne == index && model->getNodes()[nodeTwo]->getType()[ZERO_] == ATTRIBUTE_TYPE_)
		attributeOfEntity.push_back(nodeTwo);
	else if(nodeTwo == index && model->getNodes()[nodeOne]->getType()[ZERO_] == ATTRIBUTE_TYPE_)
		attributeOfEntity.push_back(nodeOne);
}

string Presentation::checkCardinalityType(char inputType){
	switch(inputType){
		case SINGLE_TYPE_:
			return SINGLE_;
		case MULTI_TYPE_:
			return MULTI_;
		default:
			return DEFAULT_;
	}
}

bool Presentation::existPathBetween(int firstID, int secondID){
	return model->existPathBetween(firstID, secondID);
}

vector <Components*> Presentation::getAllConnections(){
	return model->getAllConnections();
}

int Presentation::getAttributeOfEntity(int index){
	return attributeOfEntity[index];
}

int Presentation::getAttributeOfEntitySize(){
	return attributeOfEntity.size();
}

int Presentation::getErrorAttribute(vector<int> input){
	for(size_t i = 0 ; i < input.size() ; i++){
		for(size_t j = 0 ; j < attributeOfEntity.size() ; j++){
			if(input[i] == attributeOfEntity[j]){  //找到屬性的情況
				i = attributeOfEntity.size();
				break;
			}
			if(j == attributeOfEntity.size()-ONE_ ) //皆未找到的情況			
				return input[i];
		}
	}
	return ZERO_;
}

vector <Components*> Presentation::getNodes(){
	return model->getNodes();
}

int Presentation::getLastID(){
	return model->getLastID();
}

int Presentation::getNodesSize(){
	return model->getNodesSize();
}

Entity* Presentation::getPrimaryKey(int selectedID){
	return model->getPrimaryKey(selectedID);
}

