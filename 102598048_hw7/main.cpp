#include <QtCore/QCoreApplication>
#include <QApplication>
#include <QLabel>
#include <iostream>
#include <string>
#include "TextUI.h"
#include "ERModel.h"
#include "Presentation.h"
#include "GraphicalUI.h"

int main(int argc, char *argv[]){
	QApplication app(argc, argv);
	ERModel model;
	cout << "Please choose type you want: "<<endl
		<<"[1]TextUI [2]GraphicalUI"<<endl
		<<"> ";
	string inputType;
	getline(cin, inputType);
	while(true){
		if(inputType == TYPE_OF_TEXT_UI_){
			Presentation _Presentation(&model);
			TextUI UI(&_Presentation);
			UI.displayMenu();
			return app.exec();
		}
		else if(inputType == TYPE_OF_GRAPHICAL_UI_){
			GUIPresentation GUIpresentation(&model);
			GraphicalUI GUI(&GUIpresentation);
			GUI.setGeometry(DEFAULT_WINDOWS_X, DEFAULT_WINDOWS_Y, WINDOWS_WIDTH, WINDOWS_HEIGHT);
			GUI.show();
			return app.exec();
		}
		else{
			cout << "your input is error, please type again." << endl
				<<"> ";
			getline(cin, inputType);
		}
	}	
}


