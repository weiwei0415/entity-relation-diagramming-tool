#include "RelationViewItem.h"

RelationViewItem::RelationViewItem(QString text, size_t id){
	this->id = id;
	this->text = text;
	qFont = QFont(DRAW_TEXT_FONT_, DRAW_TEXT_SIZE_);
	qFont.setBold(true);
	QFontMetrics fontMetrics(qFont);
	QPolygonF decisionNode;
	int width = fontMetrics.width(text);
	int height = fontMetrics.height() * DOUBLE_;
	decisionNode << QPointF(width, 0) << QPointF(0, height)
		        << QPointF(-width, 0) << QPointF(0, -height)
				<< QPointF(width, 0);			
	qPath.addPolygon(decisionNode);
	textPosition = QPointF(qPath.boundingRect().center().x() - (width * HALF_), qPath.boundingRect().center().y());
}

RelationViewItem::~RelationViewItem(){
}

void RelationViewItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
	painter->setRenderHint(QPainter::Antialiasing);
	painter->setFont(qFont);
	painter->setPen(QPen(Qt::black, DRAW_ACTIVITY_PEN_WIDTH_, Qt::SolidLine));
	painter->drawPath(qPath);
	painter->drawText(textPosition, this->text);
	if (this->isSelected()){
		painter->setPen(QPen(Qt::darkCyan, DRAW_BOUNDING_PEN_WIDTH_, Qt::DashDotDotLine));
		painter->drawRect(this->boundingRect());
	}
}

QPainterPath RelationViewItem::shape() const{
    return qPath;
}

void RelationViewItem::updateViewItem(QString text){
	this->text = text;
	qPath = QPainterPath();
	QFontMetrics fontMetrics(qFont);
	QPolygonF decisionNode;
	int width = fontMetrics.width(text);
	int height = fontMetrics.height() * DOUBLE_;
	decisionNode << QPointF(width, 0) << QPointF(0, height)
		        << QPointF(-width, 0) << QPointF(0, -height)
				<< QPointF(width, 0);			
	qPath.addPolygon(decisionNode);
	textPosition = QPointF(qPath.boundingRect().center().x() - (width * HALF_), qPath.boundingRect().center().y());
}

void RelationViewItem::setPrimaryKeyViewItem(){
}

void RelationViewItem::resetPrimaryKeyViewItem(){
}