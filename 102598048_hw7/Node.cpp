#include "Node.h"

Node::Node(){
}

Node::~Node(){
}

Node::Node(int id, string name, string text){
	this->setId(id);
	this->setType(name);
	this->setText(text);
}

vector<Components*> Node::getConnection(){
	return connections;
}

void Node::connectTo(Components* Components){
	connections.push_back(Components);
}

