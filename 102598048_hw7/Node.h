#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "Components.h"
using namespace std;

class ComponentVisitor;
class Node : public Components{
	public:
		Node();
		~Node();
		Node(int id, string name, string text);
		void connectTo(Components* components);
		vector<Components*> getConnection();
		virtual void accept(ComponentVisitor &visitor) = 0;
		virtual Components* clone() = 0;
	private:
		vector<Components*> connections;
};

