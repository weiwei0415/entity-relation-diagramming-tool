#include "Components.h"

class CSortNodeCompareFunction{
	public:
		bool operator() (Components *nodeOne, Components *nodeTwo){ 
			return nodeOne->getId() < nodeTwo->getId(); 
		}
};