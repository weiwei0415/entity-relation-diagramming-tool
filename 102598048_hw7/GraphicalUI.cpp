#include "GraphicalUI.h"

GraphicalUI::GraphicalUI(GUIPresentation *present){
	presentation = present;
	presentation->attach(this);
	initialize();
	initialMenu();
	initialAction();
	createToolBar();
	this->updateChecked();
}

GraphicalUI::~GraphicalUI(){
	delete scene, view;
	presentation->detach(this);
}

void GraphicalUI::updateChecked(){
	pointer->setChecked(presentation->isActionPointerChecked());
	connection->setChecked(presentation->isActionConnectionChecked());
	newAttribute->setChecked(presentation->isActionNewAttributeChecked());
	newEntity->setChecked(presentation->isActionNewEntityChecked());
	newRelation->setChecked(presentation->isActionNewRelationChecked());
	primaryKey->setChecked(presentation->isActionSetPrimaryKeyChecked());
	undo->setEnabled(!this->presentation->checkUndoStackEmpty());
	redo->setEnabled(!this->presentation->checkRedoStackEmpty());
	deleteComponent->setEnabled(false);
	cut->setEnabled(false);
	copy->setEnabled(false);
	paste->setEnabled(!this->presentation->checkCloneEmpty());
	
	textEdit->clear();	
	QTextCursor cursor(textEdit->textCursor());
	cursor.movePosition(QTextCursor::Start);
	QTextFrame *topFrame = cursor.currentFrame();
	QTextFrameFormat topFrameFormat = topFrame->frameFormat();
	topFrameFormat.setPadding(16);
	topFrame->setFrameFormat(topFrameFormat);

	QTextTableFormat tableFormat;
	tableFormat.setBorder(1);
	tableFormat.setCellPadding(16);
	tableFormat.setAlignment(Qt::AlignLeft);

	vector<Nodes> nodes = this->presentation->getNodesData();
	vector<ConnectionNodes> connections = this->presentation->getConnectionsData();
	vector<Components*> components = this->presentation->getComponents();
	vector<size_t> attributes;
	for(size_t i = 0 ; i < components.size() ; i++){
		if(components[i]->getType() == ENTITY_){	
			vector<Components*> primaryKey = dynamic_cast <Entity*> (components[i])->getAllPrimaryKey();
			cursor.insertText(components[i]->getText().c_str());
			attributes.clear();
			for(size_t j = 0 ; j < connections.size() ; j++){
				if(connections[j].source == components[i]->getId() || connections[j].target == components[i]->getId()){
					for(size_t x = 0 ; x < nodes.size() ; x++){
						if(connections[j].source == nodes[x].id || connections[j].target == nodes[x].id){
							if(nodes[x].type == ATTRIBUTE_TYPE_)
								attributes.push_back(nodes[x].id);
						}
					}
				}
			}
			if(!attributes.empty()){
				cursor.insertTable(ONE_, attributes.size(), tableFormat);   //insertTable (int rows, int columns, const QTextTableFormat & format)
				for(size_t k = 0 ; k < attributes.size() ; k++){
					for(size_t l = 0 ; l < nodes.size() ; l++){
						if(attributes[k] == nodes[l].id){
							for(size_t i = 0 ; i < primaryKey.size() ; i++){
								if(attributes[k] == primaryKey[i]->getId())
									cursor.insertImage(PK_ICON_PATH_);
							}
							cursor.insertText(nodes[l].text.c_str());
							cursor.movePosition(QTextCursor::NextBlock);
						}
					}
				}
				cursor.setPosition(topFrame->lastPosition());
			}
			cursor.insertText(BR_);
		}
	}
	this->update();
}

void GraphicalUI::initialize(){
	widget = new QWidget;
	scene = new ComponentScene(presentation, widget);
	QBoxLayout *layout = new QHBoxLayout;
	view = new QGraphicsView(scene);
	layout->addWidget(view);
	//TableDockWidget
	this->componentsTableDockWidget = new QDockWidget(COMPONENTS_TABLE_NAME_, this);
	this->componentsTableDockWidget->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	this->componentsTableGroupBox = new QGroupBox(this->componentsTableDockWidget);
	this->componentsTableLayout = new QVBoxLayout(this->componentsTableGroupBox);
	this->componentsTableWidget = new ComponentTableWidget(this->presentation, this->widget);
	this->componentsTableLayout->addWidget(this->componentsTableWidget);
	this->componentsTableDockWidget->setWidget(this->componentsTableGroupBox);
	this->addDockWidget(Qt::RightDockWidgetArea, this->componentsTableDockWidget);
	this->componentsTableDockWidget->setFixedWidth(TABLE_WIDTH_);
	//TableView
	this->componentsTableDockWidget = new QDockWidget(TABLE_VIEWER_, this);
	this->textEdit = new QTextEdit(this->componentsTableDockWidget);
	this->componentsTableDockWidget->setWidget(textEdit);
	this->addDockWidget(Qt::BottomDockWidgetArea, this->componentsTableDockWidget);
	this->textEdit->setReadOnly(true);
	this->componentsTableDockWidget->setVisible(false);
	//Scene
	scene->setpWidget(widget);
	widget->setLayout(layout);
	msgBox = new QMessageBox;
	scene->setSceneRect(QRectF(ZERO_, ZERO_, SCENE_WIDTH_, SCENE_HEIGHT_));	
	this->setCentralWidget(widget);
	this->setWindowTitle(tr(WINDOW_TITLE_));
	this->resize(SCENE_WIDTH_, SCENE_HEIGHT_);	
}

void GraphicalUI::initialMenu(){
	//最上面那排
	menuFile = this->menuBar()->addMenu(tr(FILE_));   
	menuEdit = this->menuBar()->addMenu(tr(EDIT_));
	menuAdd = this->menuBar()->addMenu(tr(ADD_));
	menuHelp = this->menuBar()->addMenu(tr(HELP_));
	//Initial menuFile
	openFile = menuFile->addAction(QIcon(OPEN_ICON_PATH_), tr(OPEN_));   
	openFile->setShortcut(Qt::CTRL + Qt::Key_O);
	saveFile = menuFile->addAction(QIcon(SAVE_ICON_PATH_), tr(SAVE_));   
	saveFile->setShortcut(Qt::CTRL + Qt::Key_S);
	saveFileAsXml = menuFile->addAction(QIcon(), tr(SAVE_AS_XML_));   
	menuFile->addSeparator();
	exit = menuFile->addAction(QIcon(EXIT_ICON_PATH_), tr(EXIT_));
	exit->setShortcut(Qt::CTRL + Qt::Key_E);
	//Initial menuEdit
	undo = menuEdit->addAction(QIcon(UNDO_ICON_PATH_), tr(UNDO_));
	undo->setShortcut(Qt::CTRL + Qt::Key_Z);
	redo = menuEdit->addAction(QIcon(REDO_ICON_PATH_), tr(REDO_));
	redo->setShortcut(Qt::CTRL + Qt::Key_Y);
	menuEdit->addSeparator();
	deleteComponent = menuEdit->addAction(QIcon(DELETE_ICON_PATH_), tr(DELETE_));
	deleteComponent->setShortcut(Qt::Key_Delete);
	cut = menuEdit->addAction(QIcon(CUT_ICON_PATH_), tr(CUT_));
	cut->setShortcut(Qt::CTRL + Qt::Key_X);
	copy = menuEdit->addAction(QIcon(COPY_ICON_PATH_), tr(COPY_));
	copy->setShortcut(Qt::CTRL + Qt::Key_C);
	paste = menuEdit->addAction(QIcon(PASTE_ICON_PATH_), tr(PASTE_));
	paste->setShortcut(Qt::CTRL + Qt::Key_V);
	//Initial menuAdd
	pointer = menuAdd->addAction(QIcon(POINTER_ICON_PATH_), tr(POINTER_));   
	menuAdd->addSeparator();
	newAttribute = menuAdd->addAction(QIcon(ATTRIBUTE_ICON_PATH_), tr(ATTRIBUTE_));
	newEntity = menuAdd->addAction(QIcon(ENTITY_ICON_PATH_), tr(ENTITY_));
	newRelation = menuAdd->addAction(QIcon(RELATION_ICON_PATH_), tr(RELATION_));
	menuAdd->addSeparator();
	connection = menuAdd->addAction(QIcon(CONNECTION_ICON_PATH_), tr(CONNECTION_));
	menuAdd->addSeparator();
	primaryKey = menuAdd->addAction(QIcon(PRIMARY_ICON_PATH_), tr(PRIMARY_KEY_));
	tableViewer = menuAdd->addAction(QIcon(TABLE_VIEWER_ICON_PATH_), tr(TABLE_VIEWER_));
	//Initial menuHelp
	aboutTool = menuHelp->addAction(QIcon(), tr(ABOUT_TOOL_));
	this->msgBox->setWindowTitle(ABOUT_TOOL_TITLE_);
	this->msgBox->setStandardButtons(QMessageBox::Yes);
}

void GraphicalUI::initialAction(){
	connect(openFile, SIGNAL(triggered()), this, SLOT(slotOpenFile()));
	connect(saveFile, SIGNAL(triggered()), this, SLOT(slotSaveFile()));
	connect(saveFileAsXml, SIGNAL(triggered()), this, SLOT(slotSaveFileAsXml()));
	connect(exit, SIGNAL(triggered()), this, SLOT(close()));	
	connect(undo, SIGNAL(triggered()), this, SLOT(slotUndo()));	
	connect(redo, SIGNAL(triggered()), this, SLOT(slotRedo()));	
	connect(deleteComponent, SIGNAL(triggered()), this, SLOT(slotDelete()));	
	connect(cut, SIGNAL(triggered()), this, SLOT(slotCut()));
	connect(copy, SIGNAL(triggered()), this, SLOT(slotCopy()));
	connect(paste, SIGNAL(triggered()), this, SLOT(slotPaste()));
	connect(pointer, SIGNAL(triggered()), this, SLOT(slotSelectPointerMode()));
	connect(connection, SIGNAL(triggered()), this, SLOT(slotAddConnectionMode()));
	connect(primaryKey, SIGNAL(triggered()), this, SLOT(slotAddPrimaryKey()));
	connect(newAttribute, SIGNAL(triggered()), this, SLOT(slotAddAttributeNode()));
	connect(newEntity, SIGNAL(triggered()), this, SLOT(slotAddEntityNode()));
	connect(newRelation, SIGNAL(triggered()), this, SLOT(slotAddRelationNode()));
	connect(this->scene, SIGNAL(selectionChanged()), this, SLOT(slotSceneViewItemSelectionChanged()));
	connect(tableViewer, SIGNAL(triggered()), this, SLOT(slotTableViewer()));
	connect(aboutTool, SIGNAL(triggered()), this, SLOT(slotAboutTool()));
}

void GraphicalUI::createToolBar(){
	toolBarFile = this->addToolBar(TOOL_BAR_FILE_);
	toolBarFile->addAction(openFile);
	toolBarFile->addAction(saveFile);
	toolBarFile->addAction(exit);
	toolBarEdit = this->addToolBar(TOOL_BAR_FILE_);
	toolBarEdit->addAction(undo);
	toolBarEdit->addAction(redo);
	toolBarEdit->addSeparator();
	toolBarEdit->addAction(deleteComponent);
	toolBarEdit->addAction(cut);
	toolBarEdit->addAction(copy);
	toolBarEdit->addAction(paste);
	toolBarAdd = this->addToolBar(TOOL_BAR_ADD_);
	toolBarAdd->addAction(pointer);
	toolBarAdd->addSeparator();
	toolBarAdd->addAction(newAttribute);
	toolBarAdd->addAction(newEntity);
	toolBarAdd->addAction(newRelation);
	toolBarAdd->addSeparator();
	toolBarAdd->addAction(connection);
	toolBarAdd->addSeparator();
	toolBarAdd->addAction(primaryKey);
	toolBarAdd->addSeparator();
	toolBarAdd->addAction(tableViewer);
	pointer->setCheckable(true);
	connection->setCheckable(true);
	primaryKey->setCheckable(true);
	newAttribute->setCheckable(true);
	newEntity->setCheckable(true);
	newRelation->setCheckable(true);
}

void GraphicalUI::slotOpenFile(){
	QString fileNameErd = QFileDialog::getOpenFileName(this, tr(OPEN_FILE_TITLE_), DEFAULT_PATH_, tr(DEFAULT_TYPE_));
	QString fileNamePos = fileNameErd;
	fileNamePos.replace(QString(FILE_ERD_), QString(FILE_POS_));
	if(fileNameErd.length() >= ONE_){
		this->presentation->openErdFileAction(fileNameErd.toLocal8Bit().constData());
		this->presentation->openPosFileAction(fileNamePos.toLocal8Bit().constData());
		this->scene->createViewItemAfterOpenFile();
	}
	this->presentation->setMode(GUIPresentation::pointState);
}

void GraphicalUI::slotSaveFile(){
	QString fileNameErd = QFileDialog::getSaveFileName(this, tr(SAVE_FILE_TITLE_), DEFAULT_PATH_, tr(DEFAULT_TYPE_));
	QString fileNamePos = fileNameErd;
	fileNamePos.replace(QString(FILE_ERD_), QString(FILE_POS_));
	if(fileNameErd.length() >= ONE_){
		this->presentation->saveErdFileAction(fileNameErd.toLocal8Bit().constData());
		this->presentation->savePosFileAction(fileNamePos.toLocal8Bit().constData());
	}
}

void GraphicalUI::slotSaveFileAsXml(){
	QString fileNameXml = QFileDialog::getSaveFileName(this, tr(SAVE_XML_FILE_TITLE_), DEFAULT_PATH_, tr(XML_TYPE_));
	if(fileNameXml.length() >= ONE_)
		this->presentation->saveXmlFileAction(fileNameXml.toLocal8Bit().constData());
}

void GraphicalUI::slotSelectPointerMode(){
	this->presentation->setMode(GUIPresentation::pointState);
	this->scene->removeDemoItem();
}

void GraphicalUI::slotUndo(){
	this->presentation->undoAction();
}

void GraphicalUI::slotRedo(){
	this->presentation->redoAction();
}

void GraphicalUI::slotDelete(){
	QList<QGraphicsItem*> deleteItems = this->scene->selectedItems();
	vector<int> itemsId;
	for(int i = 0 ; i < deleteItems.size() ; i++){
		ComponentViewItem *del = static_cast<ComponentViewItem *> (deleteItems[i]);
		itemsId.push_back(del->getComponentId());
	}
	for(int i = 0 ; i < itemsId.size() ; i++)
		this->presentation->deleteComponentAction(itemsId[i]);
	this->presentation->setCommands(itemsId.size()); 
}

void GraphicalUI::slotCut(){
	QList<QGraphicsItem*> cutItems = this->scene->selectedItems();
	vector<int> itemsId;
	for(int i = 0 ; i < cutItems.size() ; i++){
		ComponentViewItem *item = static_cast<ComponentViewItem *> (cutItems[i]);
		itemsId.push_back(item->getComponentId());
	}
	this->presentation->cutComponentAction(itemsId);
}

void GraphicalUI::slotCopy(){
	QList<QGraphicsItem*> copyItems = this->scene->selectedItems();
	this->presentation->cleanClone();
	for(int i = 0 ; i < copyItems.size() ; i++){
		ComponentViewItem *item = static_cast<ComponentViewItem *> (copyItems[i]);
		this->presentation->setClone(item->getComponentId());
	}
	this->presentation->setMode(GUIPresentation::pointState);
}

void GraphicalUI::slotPaste(){
	this->presentation->pasteComponentAction();
}

void GraphicalUI::slotAddConnectionMode(){
	this->presentation->setMode(GUIPresentation::connectState);
	this->scene->removeDemoItem();
}

void GraphicalUI::slotAddAttributeNode(){
	this->presentation->setAddNodeType(Components::attributeType);
	this->presentation->setMode(GUIPresentation::addNodeState);
}

void GraphicalUI::slotAddEntityNode(){
	this->presentation->setAddNodeType(Components::entityType);
	this->presentation->setMode(GUIPresentation::addNodeState);
}

void GraphicalUI::slotAddRelationNode(){
	this->presentation->setAddNodeType(Components::relationType);
	this->presentation->setMode(GUIPresentation::addNodeState);
}

void GraphicalUI::slotAddPrimaryKey(){
	this->presentation->setMode(GUIPresentation::setPrimaryKeyState);
}

void GraphicalUI::slotSceneViewItemSelectionChanged(){
	this->deleteComponent->setEnabled(false);
	this->cut->setEnabled(false);
	this->copy->setEnabled(false);
	if (this->scene->selectedItems().size() >= CAN_DO_DELETE_COMPONENT_ && this->presentation->getMode() == GUIPresentation::pointState){
		this->deleteComponent->setEnabled(true);
		this->cut->setEnabled(true);
		this->copy->setEnabled(true);
	}
}

void GraphicalUI::slotTableViewer(){
	if(this->componentsTableDockWidget->isVisible())
		this->componentsTableDockWidget->setVisible(false);
	else
		this->componentsTableDockWidget->setVisible(true);
}

void GraphicalUI::slotAboutTool(){
	this->msgBox->setText(ABOUT_TOOL_TEXT_);
	this->msgBox->exec();
}