#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "Node.h"
using namespace std;

class Relationship : public Node{
	public:
		Relationship();
		~Relationship();
		Relationship(int id, string name, string text);
		void connectTo(Components* components);
		vector<Components*> getConnection();
		void accept(ComponentVisitor &visitor);
		Components* clone();
	private:
		vector<Components*> connections;
};

