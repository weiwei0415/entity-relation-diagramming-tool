#pragma once
#include <iostream>
#include <vector>
#include <string>
#include "DefineSet.h"
#include "Components.h"
using namespace std;

class Connection : public Components{
	public:
		Connection(void);
		~Connection(void);
		Connection(int id, string name, string text);
		void connectTo(Components* components);
		void disconnectTo(Components* components);
		vector<Components*> getConnection();
		void accept(ComponentVisitor &visitor);
		Components* clone();
	private:
		vector<Components*> connections;
};

