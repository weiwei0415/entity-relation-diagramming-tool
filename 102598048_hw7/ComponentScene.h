#include <QWidget>
#include <QtDebug>
#include <QtGui>
#include <QMouseEvent>
#include "GUIPresentation.h"
#include "ComponentViewItem.h"
#include "AttributeViewItem.h"
#include "EntityViewItem.h"
#include "RelationViewItem.h"
#include "ConnectionViewItem.h"
#include "ViewItemFactory.h"
#include "Observer.h"
#include "Entity.h"
#include <vector>
#include <map>

class ComponentScene : public QGraphicsScene, public Observer{
	public:
		ComponentScene(GUIPresentation *presentations, QWidget *widget);
		~ComponentScene(void);
		void setpWidget(QWidget *widget) {parentWidget = widget;};
		void createViewItemAfterOpenFile();
		void moveViewItemJudge();
		void saveViewItemPosition();
		void autoCreateViewItem();
		void autoUpdateViewItem();
		void setPrimaryKeyViewItem(vector<Nodes> nodes, vector<ConnectionNodes> connectionNodes, vector<Components*> components, vector<AllComponents> allComponents);
		void cleanAllViewItems();
		void createNodeItemForLoadFile();
		void createConnectionItemForLoadFile();
		ConnectionViewItem* buildConnectionViewItem(size_t id, size_t source, size_t target, string text);
		void setEdgeSituation(size_t source, size_t target, size_t &count, ConnectionNodes connection);
		ComponentViewItem* findViewItemById(size_t id);
		void updateChecked();
		void checkConnection(size_t fromNode, size_t toNode, string text);
		void removeDemoItem();	
		QVector <ComponentViewItem*> getNodeViewItems();
	protected:
		bool eventFilter(QObject *object, QEvent *events);//�ƥ󰻴�(�ƹ�)
		void mouseMoveEvent(QGraphicsSceneMouseEvent* me);
		void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
		void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent);
	private:
		void addNodeStateCreateViewItem(QPointF eventPos);
		void showEnterTextDialogIfNeeded(QString &text, bool &flag);
		void changeViewItemState();
		void changeEdgeItemState();
		QVector <ComponentViewItem*> nodeViewItems;
		QVector <ConnectionViewItem*> connectionViewItems;
		GUIPresentation *presentation;
		QWidget *parentWidget;
		int count;
		vector<size_t> connectionNode;	
		bool isDemoNodeExists;
		ComponentViewItem *demoViewItem;
		vector<size_t> itemsId;
		vector<QPointF> itemsPos;
		map<size_t, QPointF> startPos;
		bool isChange;
};

