#pragma once
#include "Command.h"
#include <stack>
using namespace std;

class CommandManager{
	private:
		stack<Command*> undoCmds;
		stack<Command*> redoCmds;
	public:
		CommandManager();
		~CommandManager();
		void execute(Command* command);
		void redo();
		void undo();
		string isRedoEmpty();
		string isUndoEmpty();
	friend class CommandManagerTest;
};

