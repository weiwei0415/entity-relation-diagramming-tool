#include "EntityViewItem.h"

EntityViewItem::EntityViewItem(QString text, size_t id){
	this->id = id;
	this->text = text;
	qFont = QFont(DRAW_TEXT_FONT_, DRAW_TEXT_SIZE_);
	qFont.setBold(true);
	QFontMetrics fontMetrics(qFont);
	int width = fontMetrics.width(text);
	int height = fontMetrics.height() * DOUBLE_;
	qPath.addRect(-(width + ENTITY_WIDTH_) * HALF_, -(height + ENTITY_HEIGHT_) * HALF_, width + ENTITY_WIDTH_, height + ENTITY_HEIGHT_);
	textPosition = QPointF(qPath.boundingRect().center().x() - (width * HALF_), qPath.boundingRect().center().y());
}

EntityViewItem::~EntityViewItem(){
}

void EntityViewItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){
	painter->setRenderHint(QPainter::Antialiasing);
	painter->setFont(qFont);
	painter->setPen(QPen(Qt::black, DRAW_ACTIVITY_PEN_WIDTH_, Qt::SolidLine));
	painter->drawPath(qPath);
	painter->drawText(textPosition, this->text);
	if (this->isSelected()){
		painter->setPen(QPen(Qt::darkCyan, DRAW_BOUNDING_PEN_WIDTH_, Qt::DashDotDotLine));
		painter->drawRect(this->boundingRect());
	}
}

QPainterPath EntityViewItem::shape() const{
    return qPath;
}

void EntityViewItem::updateViewItem(QString text){
	this->text = text;
	qPath = QPainterPath();
	QFontMetrics fontMetrics(qFont);
	int width = fontMetrics.width(text);
	int height = fontMetrics.height() * DOUBLE_;
	qPath.addRect(-(width + ENTITY_WIDTH_) * HALF_, -(height + ENTITY_HEIGHT_) * HALF_, width + ENTITY_WIDTH_, height + ENTITY_HEIGHT_);
	textPosition = QPointF(qPath.boundingRect().center().x() - (width * HALF_), qPath.boundingRect().center().y());
}

void EntityViewItem::setPrimaryKeyViewItem(){
}

void EntityViewItem::resetPrimaryKeyViewItem(){
}