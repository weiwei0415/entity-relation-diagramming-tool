#include "SaveComponentVisitor.h"

SaveComponentVisitor::SaveComponentVisitor(){
	this->componentStream = string(DEFAULT_);
	this->primaryKeyStream = string(DEFAULT_);
}

SaveComponentVisitor::~SaveComponentVisitor(){
}

void SaveComponentVisitor::visit(Attributes *attributeNode){
	this->componentStream += attributeNode->getType()[ZERO_] + string(COMMA_STR_) + string(SPACE_STR_) + attributeNode->getText() + string(BR_);
}

void SaveComponentVisitor::visit(Entity *entityNode){
	stringstream entityId;
	stringstream keyId;
	vector<Components*> primaryKey = entityNode->getAllPrimaryKey();
	this->componentStream += entityNode->getType()[ZERO_] + string(COMMA_STR_) + string(SPACE_STR_) + entityNode->getText() + string(BR_);
	entityId << entityNode->getId();
	for(size_t i = 0 ; i < primaryKey.size() ; i++){
		keyId << primaryKey[i]->getId();
		if(i < primaryKey.size() - ONE_)
			keyId << string(COMMA_STR_);
	}
	if(!primaryKey.empty()){
		this->primaryKeyStream += entityId.str() + string(SPACE_STR_);
		this->primaryKeyStream += keyId.str() + string(BR_);
	}
}

void SaveComponentVisitor::visit(Relationship *relationNode){
	this->componentStream += relationNode->getType()[ZERO_] + string(COMMA_STR_) + string(SPACE_STR_) + relationNode->getText() + string(BR_);
}

void SaveComponentVisitor::visit(Connection *connections){
	stringstream connectionId;
	stringstream nodesId;
	vector<Components*> connectionNode = connections->getConnection();	
	this->componentStream += connections->getType()[ZERO_];
	if(connections->getText() != DEFAULT_)
		this->componentStream += string(COMMA_STR_) + string(SPACE_STR_) + connections->getText();
	this->componentStream += string(BR_);
	connectionId << connections->getId();
	nodesId << connectionNode[ZERO_]->getId() << string(COMMA_STR_) << connectionNode[ONE_]->getId();	
	this->connectionStream += connectionId.str() + string(SPACE_STR_);
	this->connectionStream += nodesId.str() + string(BR_);
}

string SaveComponentVisitor::getFileString(){
	return this->componentStream + string(BR_) + this->connectionStream + string(BR_) + this->primaryKeyStream;
}