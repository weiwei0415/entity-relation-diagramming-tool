#pragma once
#include "State.h"
class GUIPresentation;

class ConnectState : public State{
	public:
		ConnectState(GUIPresentation *present);
		~ConnectState();
		bool canViewItemSelectable();
		bool canViewItemMovwble();
		bool canEdgeSelectable();
		bool canEdgeMoveble();
};

