#include "AddComponentCmdTest.h"

void AddComponentCmdTest::SetUp(){
	_ERModel = new ERModel();
}

void AddComponentCmdTest::TearDown(){
	delete _ERModel;
}

void AddComponentCmdTest::testExecute(){
	//新增Entity
	_AddCmd = new AddComponentCmd(_ERModel, ENTITY_TYPE_, "hello");
	_AddCmd->execute();
	ASSERT_EQ(1, _ERModel->components.size());
	//新增Attribute
	_AddCmd2 = new AddComponentCmd(_ERModel, ATTRIBUTE_TYPE_, "");
	_AddCmd2->execute();
	ASSERT_EQ(2, _ERModel->components.size());
	delete _AddCmd, _AddCmd2;
}

void AddComponentCmdTest::testUnexecute(){
	//新增Entity
	_AddCmd = new AddComponentCmd(_ERModel, ENTITY_TYPE_, "hello");
	_AddCmd->execute();	
	ASSERT_EQ(1, _ERModel->components.size());
	//復原
	_AddCmd->unexecute();
	ASSERT_TRUE(_ERModel->components.empty());
	delete _AddCmd;
}

//--------------GTest
TEST_F(AddComponentCmdTest, testExecute) {
	testExecute();
}

TEST_F(AddComponentCmdTest, testUnexecute) {
	testUnexecute();
}