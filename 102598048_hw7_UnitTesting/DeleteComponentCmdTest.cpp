#include "DeleteComponentCmdTest.h"

void DeleteComponentCmdTest::SetUp(){
	_ERModel = new ERModel();
}

void DeleteComponentCmdTest::TearDown(){
	delete _ERModel;
}

void DeleteComponentCmdTest::testExecute(){
	//新增Entity
	_AddCmd = new AddComponentCmd(_ERModel, ENTITY_TYPE_, "hello");
	_AddCmd->execute();
	ASSERT_EQ(1, _ERModel->components.size());
	//刪除Entity
	_DeleteCmd = new DeleteComponentCmd(_ERModel, _ERModel->components.back()->getId());
	_DeleteCmd->execute();
	ASSERT_TRUE(_ERModel->components.empty());
	delete _AddCmd, _AddCmd2, _DeleteCmd;
}

void DeleteComponentCmdTest::testUnexecute(){
	//新增Entity
	_AddCmd = new AddComponentCmd(_ERModel, ENTITY_TYPE_, "E1");
	_AddCmd->execute();
	ASSERT_EQ(1, _ERModel->components.size());
	_AddCmd = new AddComponentCmd(_ERModel, ATTRIBUTE_TYPE_, "A1");
	_AddCmd->execute();
	ASSERT_EQ(2, _ERModel->components.size());
	_AddCmd = new AddComponentCmd(_ERModel, ENTITY_TYPE_, "E2");
	_AddCmd->execute();
	ASSERT_EQ(3, _ERModel->components.size());
	_AddCmd = new AddComponentCmd(_ERModel, ATTRIBUTE_TYPE_, "A2");
	_AddCmd->execute();
	ASSERT_EQ(4, _ERModel->components.size());
	_ConnectCmd = new ConnectComponentsCmd(_ERModel, _ERModel->components.at(0)->getId(), _ERModel->components.at(1)->getId(), "");
	_ConnectCmd->execute();
	ASSERT_EQ(5, _ERModel->components.size());
	_ConnectCmd = new ConnectComponentsCmd(_ERModel, _ERModel->components.at(2)->getId(), _ERModel->components.at(3)->getId(), "");
	_ConnectCmd->execute();
	ASSERT_EQ(6, _ERModel->components.size());
	//刪除Connection
	_DeleteCmd = new DeleteComponentCmd(_ERModel, _ERModel->components.back()->getId());
	_DeleteCmd->execute();
	ASSERT_EQ(5, _ERModel->components.size());
	//復原
	_DeleteCmd->unexecute();
	ASSERT_EQ(6, _ERModel->components.size());
	//刪除Entity
	_DeleteCmd = new DeleteComponentCmd(_ERModel, _ERModel->components[0]->getId());
	_DeleteCmd->execute();
	ASSERT_EQ(4, _ERModel->components.size());
	//復原
	_DeleteCmd->unexecute();
	ASSERT_EQ(6, _ERModel->components.size());
	delete _AddCmd, _DeleteCmd;
}

//--------------GTest
TEST_F(DeleteComponentCmdTest, testExecute) {
	testExecute();
}

TEST_F(DeleteComponentCmdTest, testUnexecute) {
	testUnexecute();
}