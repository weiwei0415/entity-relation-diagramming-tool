#include "ConnectComponentsCmdTest.h"

void ConnectComponentsCmdTest::SetUp() {
	_ERModel = new ERModel();
}

void ConnectComponentsCmdTest::TearDown() {
	delete _ERModel;
}

void ConnectComponentsCmdTest::testExecute() {
	//新增Entity
	_AddCmd = new AddComponentCmd(_ERModel, ENTITY_TYPE_, "hello");
	_AddCmd->execute();
	ASSERT_EQ(1, _ERModel->components.size());
	//新增Attribute
	_AddCmd2 = new AddComponentCmd(_ERModel, ATTRIBUTE_TYPE_, "");
	_AddCmd2->execute();
	ASSERT_EQ(2, _ERModel->components.size());
	_ConnectCmd = new ConnectComponentsCmd(_ERModel, _ERModel->components.at(0)->getId(), _ERModel->components.at(1)->getId(), "");
	_ConnectCmd->execute();
	ASSERT_EQ("Connection", _ERModel->components.back()->getType());
	delete _AddCmd, _AddCmd2, _ConnectCmd;
}

void ConnectComponentsCmdTest::testUnexecute(){
	//新增Entity
	_AddCmd = new AddComponentCmd(_ERModel, ENTITY_TYPE_, "hello");
	_AddCmd->execute();
	ASSERT_EQ(1, _ERModel->components.size());
	//新增Attribute
	_AddCmd2 = new AddComponentCmd(_ERModel, ATTRIBUTE_TYPE_, "");
	_AddCmd2->execute();
	ASSERT_EQ(2, _ERModel->components.size());
	_ConnectCmd = new ConnectComponentsCmd(_ERModel, _ERModel->components.at(0)->getId(), _ERModel->components.at(1)->getId(), "");
	_ConnectCmd->execute();
	ASSERT_EQ(3, _ERModel->components.size());
	ASSERT_EQ("Connection", _ERModel->components.back()->getType());
	_ConnectCmd->unexecute();
	ASSERT_EQ(2, _ERModel->components.size());
	delete _AddCmd, _AddCmd2, _ConnectCmd;
}

//--------------GTest
TEST_F(ConnectComponentsCmdTest,testExecute){
	testExecute();
}

TEST_F(ConnectComponentsCmdTest,testUnexecute){
	testUnexecute();
}