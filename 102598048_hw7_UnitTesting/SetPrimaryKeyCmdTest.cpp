#include "SetPrimaryKeyCmdTest.h"

void SetPrimaryKeyCmdTest::SetUp(){
	model = new ERModel();
	model->addNode('E', "E1", 0);
	model->addNode('A', "A1", 1);
	model->addNode('C', "", 2);
	model->addConnection(2, model->getNodes()[0],  model->getNodes()[1], "");
}

void SetPrimaryKeyCmdTest::TearDown(){
	delete model, setPrimaryKeyCmd;
}

void SetPrimaryKeyCmdTest::testExecute(){
	setPrimaryKeyCmd = new SetPrimaryKeyCmd(model, 0, 1);
	setPrimaryKeyCmd->execute();
	Entity* entity = dynamic_cast <Entity*> (model->getNodes()[0]);
	ASSERT_EQ(1, entity->getAllPrimaryKey().at(0)->getId());
}

void SetPrimaryKeyCmdTest::testUnexecute(){
	setPrimaryKeyCmd = new SetPrimaryKeyCmd(model, 0, 1);
	setPrimaryKeyCmd->execute();
	Entity* entity = dynamic_cast <Entity*> (model->getNodes()[0]);
	ASSERT_EQ(1, entity->getAllPrimaryKey().at(0)->getId());
	setPrimaryKeyCmd->unexecute();
	ASSERT_TRUE(entity->getAllPrimaryKey().empty());
}

//--------------GTest
TEST_F(SetPrimaryKeyCmdTest, testExecute){
	testExecute();
}

TEST_F(SetPrimaryKeyCmdTest, testUnexecute){
	testUnexecute();
}