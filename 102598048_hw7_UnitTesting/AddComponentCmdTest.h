#include <gtest/gtest.h>
#include "..\102598048_hw7\AddComponentCmd.h"

class AddComponentCmdTest : public testing::Test{
	protected:
		virtual void SetUp();
		virtual void TearDown();
		void testExecute();
		void testUnexecute();

	private:
		ERModel* _ERModel;
		AddComponentCmd *_AddCmd, *_AddCmd2; 
};

