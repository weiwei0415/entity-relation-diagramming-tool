#include <gtest/gtest.h>
#include "..\102598048_hw7\Presentation.h"

class PresentationTest : public testing::Test{
	protected:
		virtual void SetUp();
		virtual void TearDown();
		void testingData();
		void testLoadFileAction();
		void testSaveFileAction();
		void testAddNodeAction();
		void testDeleteNodeAction();
		void testConnectNodeAction();
		void testAddNodeCount();
		void testDeleteNodeCount();
		void testAddAttributeOfEntity();
		void testAddPrimaryKey();
		void testIsNodesEmpty();
		void testIsEntityEmpty();
		void testIsAttributeOfEntityEmpty();
		void testIsBelongToEntity();
		void testCheckCommand();
		void testCheckInput();
		void testCheckID();
		void testCheckType();
		void testCheckCardinality();
		void testCheckAttributeOfEntity();
		void testCheckCardinalityType();
		void testExistPathBetween();
		void testGetErrorAttribute();
	private:
		ERModel _ERModel;
		Presentation *_Presentation;
		string _Path;
};

