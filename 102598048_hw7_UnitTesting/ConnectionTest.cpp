#include "ConnectionTest.h"

void ConnectionTest::SetUp(){
	_Components = new Connection();
	//Attribute
	_Node1 = new Attributes();
	_Node1->setId(0);
	_Node1->setType(ATTRIBUTE_);
	_Node1->setText("A1");
	//Entity
	_Node2 = new Entity();
	_Node2->setId(1);
	_Node2->setType(ENTITY_);
	_Node2->setText("E1");
	//Relation
	_Node3 = new Relationship();
	_Node3->setId(2);
	_Node3->setType(RELATION_);
	_Node3->setText("R1");
}

void ConnectionTest::TearDown(){
	delete _Components;
}

void ConnectionTest::testConnectTo(){
	ASSERT_TRUE(_Components->getConnection().empty());
	//Attribute
	_Components->connectTo(_Node1);
	ASSERT_EQ(1, _Components->getConnection().size());
	ASSERT_EQ(0, _Components->getConnection().back()->getId());
	ASSERT_EQ(ATTRIBUTE_, _Components->getConnection().back()->getType());
	ASSERT_EQ("A1", _Components->getConnection().back()->getText());
	//Entity
	_Components->connectTo(_Node2);
	ASSERT_EQ(2, _Components->getConnection().size());
	ASSERT_EQ(1, _Components->getConnection().back()->getId());
	ASSERT_EQ(ENTITY_, _Components->getConnection().back()->getType());
	ASSERT_EQ("E1", _Components->getConnection().back()->getText());
	//Relation
	_Components->connectTo(_Node3);
	ASSERT_EQ(3, _Components->getConnection().size());
	ASSERT_EQ(2, _Components->getConnection().back()->getId());
	ASSERT_EQ(RELATION_, _Components->getConnection().back()->getType());
	ASSERT_EQ("R1", _Components->getConnection().back()->getText());
}

void ConnectionTest::testDisconnectTo(){
	ASSERT_TRUE(_Components->getConnection().empty());
	_Components->connectTo(_Node1);
	_Components->connectTo(_Node2);
	_Components->connectTo(_Node3);
	ASSERT_EQ(3, _Components->getConnection().size());
	_Components->disconnectTo(_Node1);
	ASSERT_EQ(2, _Components->getConnection().size());
	_Components->disconnectTo(_Node2);
	ASSERT_EQ(1, _Components->getConnection().size());
	_Components->disconnectTo(_Node3);
	ASSERT_EQ(0, _Components->getConnection().size());
}

//-----GTest-----
TEST_F(ConnectionTest, testConnectTo) {
	testConnectTo();
}

TEST_F(ConnectionTest, testDisconnectTo) {
	testDisconnectTo();
}