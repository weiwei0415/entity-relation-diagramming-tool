#include "ComponentFactoryTest.h"

void ComponentFactoryTest::SetUp(){
}

void ComponentFactoryTest::TearDown(){
	delete _CreatedAttribute, _CreatedEntity, _CreatedRelation, _CreatedFinal, _CreatedConnection, _CreatedError;
}

TEST_F(ComponentFactoryTest,testComponentFactory){
	_CreatedAttribute = ComponentFactory::createComponent(0, 'A', "A1");
	ASSERT_EQ("Attribute", _CreatedAttribute->getType());
	_CreatedEntity = ComponentFactory::createComponent(1, 'E', "E1");
	ASSERT_EQ("Entity", _CreatedEntity->getType());
	_CreatedRelation = ComponentFactory::createComponent(2, 'R', "R1");
	ASSERT_EQ("Relation", _CreatedRelation->getType());
	_CreatedConnection = ComponentFactory::createComponent(3, 'C', "C1");
	ASSERT_EQ("Connection", _CreatedConnection->getType());
	_CreatedError = ComponentFactory::createComponent(4, 'G', "G1");
	ASSERT_EQ(0, _CreatedError);
}
