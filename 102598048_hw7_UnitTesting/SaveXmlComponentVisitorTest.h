#include <gtest/gtest.h>
#include "..\102598048_hw7\SaveXmlComponentVisitor.h"
#include "..\102598048_hw7\Attributes.h"
#include "..\102598048_hw7\Entity.h"
#include "..\102598048_hw7\Relationship.h"
#include "..\102598048_hw7\Connection.h"
#include "..\102598048_hw7\ERModel.h"

class SaveXmlComponentVisitorTest : public testing::Test{
	protected:
		virtual void SetUp();
		virtual void TearDown();
		void testVisitAttributes();
		void testVisitEntity();
		void testVisitRelationship();
		void testVisitConnection();
	private:
		SaveXmlComponentVisitor *saveXmlComponentVisitor;
		Attributes *attributeNode;
		Entity *entityNode;
		Relationship *relationNode;
		Connection *connections;
		ERModel *model;
};

