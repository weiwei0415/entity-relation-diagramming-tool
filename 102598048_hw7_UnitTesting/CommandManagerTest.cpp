#include "CommandManagerTest.h"

void CommandManagerTest::SetUp(){
	_ERModel = new ERModel();
}

void CommandManagerTest::TearDown(){
	while (!_CmdManager.redoCmds.empty()){
		Command *command = _CmdManager.redoCmds.top();
		_CmdManager.redoCmds.pop();
		delete command;
	}
	while (!_CmdManager.undoCmds.empty()){
		Command *command = _CmdManager.undoCmds.top();
		_CmdManager.undoCmds.pop();
		delete command;
	}
	delete _ERModel;
}

void CommandManagerTest::testExecute(){
	_CmdManager.execute(new AddComponentCmd(_ERModel, ENTITY_TYPE_, "E1"));
	ASSERT_EQ('E', _ERModel->components.at(0)->getType()[ZERO_]);
	ASSERT_EQ(0, _ERModel->components.at(0)->getId());
	ASSERT_EQ("E1", _ERModel->components.at(0)->getText());
	ASSERT_EQ(1, _CmdManager.undoCmds.size());
	ASSERT_TRUE(_CmdManager.redoCmds.empty());
	_CmdManager.execute(new AddComponentCmd(_ERModel, ATTRIBUTE_TYPE_, "A1"));
	ASSERT_EQ('A', _ERModel->components.at(1)->getType()[ZERO_]);
	ASSERT_EQ(1, _ERModel->components.at(1)->getId());
	ASSERT_EQ("A1", _ERModel->components.at(1)->getText());
	ASSERT_EQ(2, _CmdManager.undoCmds.size());
	ASSERT_TRUE(_CmdManager.redoCmds.empty());
	_CmdManager.undo();
	_CmdManager.execute(new AddComponentCmd(_ERModel, ATTRIBUTE_TYPE_, "A2"));
	ASSERT_EQ('A', _ERModel->components.at(1)->getType()[ZERO_]);
	ASSERT_EQ(1, _ERModel->components.at(1)->getId());
	ASSERT_EQ("A2", _ERModel->components.at(1)->getText());
	ASSERT_EQ(2,  _CmdManager.undoCmds.size());
	ASSERT_TRUE(_CmdManager.redoCmds.empty());
}

void CommandManagerTest::testRedo(){
	ASSERT_TRUE(_CmdManager.redoCmds.empty());
	ASSERT_TRUE(_CmdManager.undoCmds.empty());
	_CmdManager.redo();
	ASSERT_TRUE(_CmdManager.redoCmds.empty());
	ASSERT_TRUE(_CmdManager.undoCmds.empty());
	_CmdManager.redoCmds.push( new AddComponentCmd(_ERModel, ENTITY_TYPE_, ""));
	ASSERT_EQ(1, _CmdManager.redoCmds.size());
	ASSERT_TRUE(_CmdManager.undoCmds.empty());
	_CmdManager.redo();
	ASSERT_EQ(0, _CmdManager.redoCmds.size());
	ASSERT_EQ(1, _CmdManager.undoCmds.size());
}

void CommandManagerTest::testUndo(){
	ASSERT_TRUE(_CmdManager.redoCmds.empty());
	ASSERT_TRUE(_CmdManager.undoCmds.empty());
	_CmdManager.undo();
	ASSERT_TRUE(_CmdManager.redoCmds.empty());
	ASSERT_TRUE(_CmdManager.undoCmds.empty());
	_CmdManager.undoCmds.push(new AddComponentCmd(_ERModel, ENTITY_TYPE_, ""));
	ASSERT_EQ(1, _CmdManager.undoCmds.size());
	ASSERT_TRUE(_CmdManager.redoCmds.empty());
	_CmdManager.undo();
	ASSERT_EQ(0, _CmdManager.undoCmds.size());
	ASSERT_EQ(1, _CmdManager.redoCmds.size());
}

void CommandManagerTest::testIsRedoEmpty(){
	ASSERT_TRUE(_CmdManager.redoCmds.empty());
	ASSERT_EQ("Cannot redo.", _CmdManager.isRedoEmpty());
	_CmdManager.redoCmds.push(new AddComponentCmd(_ERModel, ENTITY_TYPE_, "GG"));
	ASSERT_EQ(1, _CmdManager.redoCmds.size());
	ASSERT_EQ("Redo succeed!", _CmdManager.isRedoEmpty());
}

void CommandManagerTest::testIsUndoEmpty(){
	ASSERT_TRUE(_CmdManager.undoCmds.empty());
	ASSERT_EQ("Cannot undo.", _CmdManager.isUndoEmpty());
	_CmdManager.undoCmds.push(new AddComponentCmd(_ERModel, ENTITY_TYPE_, "GG"));
	ASSERT_EQ(1, _CmdManager.undoCmds.size());
	ASSERT_EQ("Undo succeed!", _CmdManager.isUndoEmpty());
}

//-----GTest-----
TEST_F(CommandManagerTest, testExecute) {
	testExecute();
}

TEST_F(CommandManagerTest, testRedo) {
	testRedo();
}

TEST_F(CommandManagerTest, testUndo) {
	testUndo();
}

TEST_F(CommandManagerTest, testIsRedoEmpty) {
	testIsRedoEmpty();
}

TEST_F(CommandManagerTest, testIsUndoEmpty) {
	testIsUndoEmpty();
}

