#include <gtest/gtest.h>
#include "..\102598048_hw7\AddComponentCmd.h"
#include "..\102598048_hw7\ConnectComponentsCmd.h"
#include "..\102598048_hw7\DeleteComponentCmd.h"

class DeleteComponentCmdTest : public testing::Test{
	protected:
		virtual void SetUp();
		virtual void TearDown();
		void testExecute();
		void testUnexecute();
	private:
		ERModel* _ERModel;
		AddComponentCmd *_AddCmd, *_AddCmd2;
		ConnectComponentsCmd *_ConnectCmd;
		DeleteComponentCmd *_DeleteCmd; 
};
