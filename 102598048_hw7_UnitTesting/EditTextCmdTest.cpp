#include "EditTextCmdTest.h"

void EditTextCmdTest::SetUp(){
	model = new ERModel();
	model->addNode('A', "A1", 0);
}

void EditTextCmdTest::TearDown(){
	delete model, editTextCmd;
}

void EditTextCmdTest::testExecute(){
	ASSERT_EQ("A1", model->getNodes()[0]->getText());
	editTextCmd = new EditTextCmd(model, 0, "A2");
	editTextCmd->execute();
	ASSERT_EQ("A2", model->getNodes()[0]->getText());
}

void EditTextCmdTest::testUnexecute(){
	ASSERT_EQ("A1", model->getNodes()[0]->getText());
	editTextCmd = new EditTextCmd(model, 0, "A2");
	editTextCmd->execute();
	ASSERT_EQ("A2", model->getNodes()[0]->getText());
	editTextCmd->unexecute();
	ASSERT_EQ("A1", model->getNodes()[0]->getText());
}

//--------------GTest
TEST_F(EditTextCmdTest, testExecute){
	testExecute();
}

TEST_F(EditTextCmdTest, testUnexecute){
	testUnexecute();
}