#include <gtest/gtest.h>
#include "..\102598048_hw7\CommandManager.h"
#include "..\102598048_hw7\AddComponentCmd.h"

class CommandManagerTest : public testing::Test{
	protected:
		virtual void SetUp();
		virtual void TearDown();
		void testExecute();
		void testRedo();
		void testUndo();
		void testIsRedoEmpty();
		void testIsUndoEmpty();
	private:
		ERModel* _ERModel;
		CommandManager _CmdManager;
		Command *_NewCmd1, *_NewCmd2;
};

