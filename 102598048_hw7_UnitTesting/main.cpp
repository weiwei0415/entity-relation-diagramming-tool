#include <gtest/gtest.h>
#include "../102598048_hw7/DefineSet.h"
#include "../102598048_hw7/AddComponentCmd.cpp"
#include "../102598048_hw7/Command.cpp"
#include "../102598048_hw7/ConnectComponentsCmd.cpp"
#include "../102598048_hw7/CutComponentCmd.cpp"
#include "../102598048_hw7/PasteComponentCmd.cpp"
#include "../102598048_hw7/DeleteComponentCmd.cpp"
#include "../102598048_hw7/SetPrimaryKeyCmd.cpp"
#include "../102598048_hw7/GUIMoveCmd.cpp"
#include "../102598048_hw7/EditTextCmd.cpp"
#include "../102598048_hw7/CommandManager.cpp"
#include "../102598048_hw7/Attributes.cpp"
#include "../102598048_hw7/Components.cpp"
#include "../102598048_hw7/Connection.cpp"
#include "../102598048_hw7/Entity.cpp"
#include "../102598048_hw7/Node.cpp"
#include "../102598048_hw7/Relationship.cpp"
#include "../102598048_hw7/ERModel.cpp"
#include "../102598048_hw7/ComponentFactory.cpp"
#include "../102598048_hw7/Presentation.cpp"
#include "../102598048_hw7/GUIPresentation.cpp"
#include "../102598048_hw7/TextUI.cpp"
#include "../102598048_hw7/Subject.cpp"
#include "../102598048_hw7/StateFactory.cpp"
#include "../102598048_hw7/State.cpp"
#include "../102598048_hw7/AddNodeState.cpp"
#include "../102598048_hw7/ConnectState.cpp"
#include "../102598048_hw7/PointState.cpp"
#include "../102598048_hw7/SetPrimaryKeyState.cpp"
#include "../102598048_hw7/ComponentVisitor.cpp"
#include "../102598048_hw7/SaveComponentVisitor.cpp"
#include "../102598048_hw7/SaveXmlComponentVisitor.cpp"

int main(int argc, char *argv[]){
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("pause");
}
