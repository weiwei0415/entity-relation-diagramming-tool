#include "GUIMoveCmdTest.h"

void GUIMoveCmdTest::SetUp(){
	model = new ERModel();
	model->setPostionMap(0, QPointF(100, 200));
}

void GUIMoveCmdTest::TearDown(){
	delete model, moveCmd;
}

void GUIMoveCmdTest::testExecute(){
	moveCmd = new GUIMoveCmd(model, 0, QPointF(200, 100));
	moveCmd->execute();
	ASSERT_EQ(QPointF(200, 100), model->getPositionMap()[0]);
}

void GUIMoveCmdTest::testUnexecute(){
	moveCmd = new GUIMoveCmd(model, 0, QPointF(200, 100));
	moveCmd->execute();
	ASSERT_EQ(QPointF(200, 100), model->getPositionMap()[0]);
	moveCmd->unexecute();
	ASSERT_EQ(QPointF(100, 200), model->getPositionMap()[0]);
}

//--------------GTest
TEST_F(GUIMoveCmdTest, testExecute){
	testExecute();
}

TEST_F(GUIMoveCmdTest, testUnexecute){
	testUnexecute();
}
