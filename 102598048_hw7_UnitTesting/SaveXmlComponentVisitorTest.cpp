#include "SaveXmlComponentVisitorTest.h"


void SaveXmlComponentVisitorTest::SetUp(){
	model = new ERModel();
	saveXmlComponentVisitor = new SaveXmlComponentVisitor(model);
	attributeNode = new Attributes(1, "Attribute", "A1");
	entityNode = new Entity(2, "Entity", "E1");	
	relationNode = new Relationship(3, "Relation", "R1");
	connections = new Connection(4, "Connection", "C1");
	connections->connectTo(attributeNode);
	connections->connectTo(entityNode);
	entityNode->setPrimaryKey(attributeNode);
}

void SaveXmlComponentVisitorTest::TearDown(){
	delete saveXmlComponentVisitor, attributeNode, entityNode, relationNode, connections;
}

void SaveXmlComponentVisitorTest::testVisitAttributes(){
	saveXmlComponentVisitor->visit(attributeNode);
	ASSERT_EQ("\t<Attribute>\n\t\t<Id>1</Id>\n\t\t<Text>A1</Text>\n\t\t<positionX>0</positionX>\n\t\t<positionY>0</positionY>\n\t</Attribute>\n", saveXmlComponentVisitor->getFileString());
}

void SaveXmlComponentVisitorTest::testVisitEntity(){
	saveXmlComponentVisitor->visit(entityNode);
	ASSERT_EQ("\t<Entity>\n\t\t<Id>2</Id>\n\t\t<Text>E1</Text>\n\t\t<PrimaryKey>1</PrimaryKey>\n\t\t<positionX>0</positionX>\n\t\t<positionY>0</positionY>\n\t</Entity>\n", saveXmlComponentVisitor->getFileString());
}

void SaveXmlComponentVisitorTest::testVisitRelationship(){
	saveXmlComponentVisitor->visit(relationNode);
	ASSERT_EQ("\t<Relation>\n\t\t<Id>3</Id>\n\t\t<Text>R1</Text>\n\t\t<positionX>0</positionX>\n\t\t<positionY>0</positionY>\n\t</Relation>\n", saveXmlComponentVisitor->getFileString());
}

void SaveXmlComponentVisitorTest::testVisitConnection(){
	saveXmlComponentVisitor->visit(connections);
	ASSERT_EQ("\t<Connection>\n\t\t<Id>4</Id>\n\t\t<Text>C1</Text>\n\t\t<Source>1</Source>\n\t\t<Target>2</Target>\n\t</Connection>\n", saveXmlComponentVisitor->getFileString());
}

//--------------GTest
TEST_F(SaveXmlComponentVisitorTest, testVisitAttributes){
	testVisitAttributes();
}

TEST_F(SaveXmlComponentVisitorTest, testVisitEntity){
	testVisitEntity();
}

TEST_F(SaveXmlComponentVisitorTest, testVisitRelationship){
	testVisitRelationship();
}

TEST_F(SaveXmlComponentVisitorTest, testVisitConnection){
	testVisitConnection();
}