#include "SaveComponentVisitorTest.h"

void SaveComponentVisitorTest::SetUp(){
	saveComponentVisitor = new SaveComponentVisitor();
	attributeNode = new Attributes(1, "Attribute", "A1");
	entityNode = new Entity(2, "Entity", "E1");	
	relationNode = new Relationship(3, "Relation", "R1");
	connections = new Connection(4, "Connection", "C1");
	connections->connectTo(attributeNode);
	connections->connectTo(entityNode);
	entityNode->setPrimaryKey(attributeNode);
}

void SaveComponentVisitorTest::TearDown(){
	delete saveComponentVisitor, attributeNode, entityNode, relationNode, connections;
}

void SaveComponentVisitorTest::testVisitAttributes(){
	saveComponentVisitor->visit(attributeNode);
	ASSERT_EQ("A, A1\n\n\n", saveComponentVisitor->getFileString());
}

void SaveComponentVisitorTest::testVisitEntity(){
	saveComponentVisitor->visit(entityNode);
	ASSERT_EQ("E, E1\n\n\n2 1\n", saveComponentVisitor->getFileString());
}

void SaveComponentVisitorTest::testVisitRelationship(){
	saveComponentVisitor->visit(relationNode);
	ASSERT_EQ("R, R1\n\n\n", saveComponentVisitor->getFileString());
}

void SaveComponentVisitorTest::testVisitConnection(){
	saveComponentVisitor->visit(connections);
	ASSERT_EQ("C, C1\n\n4 1,2\n\n", saveComponentVisitor->getFileString());
}

//--------------GTest
TEST_F(SaveComponentVisitorTest, testVisitAttributes){
	testVisitAttributes();
}

TEST_F(SaveComponentVisitorTest, testVisitEntity){
	testVisitEntity();
}

TEST_F(SaveComponentVisitorTest, testVisitRelationship){
	testVisitRelationship();
}

TEST_F(SaveComponentVisitorTest, testVisitConnection){
	testVisitConnection();
}