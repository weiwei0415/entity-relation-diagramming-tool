#include <gtest/gtest.h>
#include "..\102598048_hw7\ERModel.h"

class ERModelTest : public testing::Test{
	protected:
		virtual void SetUp();
		virtual void TearDown();
		void testingData();
		void testLoadFile();
		void testLoadFileAddNodes();
		void testLoadFileAddConnections();
		void testLoadFileAddPrimaryKeys();
		void testSaveFile();
		void testSaveFileAddNodes();
		void testSaveFileAddConnections();
		void testSaveFileAddPrimaryKeys();
		void testAddNode();
		void testAddConnection();
		void testAddConnectionNode();
		void testAddPrimaryKey();
		void testDeleteComponent();
		void testDeleteConnection();
		void testDeleteNode();
		void testCheckCommand();
		void testCheckInput();
		void testCheckID();
		void testCheckType();
		void testCheckCardinality();
		void testExistPathBetween();
		void testLoadFileAddPosition();
		void testSaveErdFile();
		void testSavePosFile();
		void testSaveXmlFile();
	private:
		ERModel *_ERModel;
		string _Path;
};

