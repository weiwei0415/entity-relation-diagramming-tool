#include <gtest/gtest.h>
#include "..\102598048_hw7\SaveComponentVisitor.h"
#include "..\102598048_hw7\Attributes.h"
#include "..\102598048_hw7\Entity.h"
#include "..\102598048_hw7\Relationship.h"
#include "..\102598048_hw7\Connection.h"
#include "..\102598048_hw7\ERModel.h"

class SaveComponentVisitorTest : public testing::Test{
	protected:
		virtual void SetUp();
		virtual void TearDown();
		void testVisitAttributes();
		void testVisitEntity();
		void testVisitRelationship();
		void testVisitConnection();
	private:
		SaveComponentVisitor *saveComponentVisitor;
		ERModel *model;
		Attributes *attributeNode;
		Entity *entityNode;
		Relationship *relationNode;
		Connection *connections;
};

