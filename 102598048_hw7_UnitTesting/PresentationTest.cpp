#include "PresentationTest.h"
#include <iostream>

void PresentationTest::SetUp(){
	_Presentation = new Presentation(&_ERModel);
	_Path = "testdata\\test_file1.add";
	_mkdir("testdata\\");
	ofstream outputFile(_Path);
	outputFile << "E, Engineer\n";
	outputFile << "A, Emp_ID\n";
	outputFile << "R, Has\n";
	outputFile << "A, Name\n";
	outputFile << "E, PC\n";
	outputFile << "A, PC_ID\n";
	outputFile << "A, Purchase_Date\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C, 1\n";
	outputFile << "C, 1\n";
	outputFile << "A, Department\n";
	outputFile << "C\n";
	outputFile << "\n";
	outputFile << "7 0,1\n";
	outputFile << "8 0,3\n";
	outputFile << "9 4,5\n";
	outputFile << "10 4,6\n";
	outputFile << "11 0,2\n";
	outputFile << "12 2,4\n";
	outputFile << "14 0,13\n";
	outputFile << "\n";
	outputFile << "0 1,3\n";
	outputFile << "4 5\n";
	outputFile.close();
}

void PresentationTest::TearDown(){
	remove("testdata\\test_file1.add");
	_rmdir("testdata\\");
	delete _Presentation;
}

void PresentationTest::testingData(){
	ASSERT_TRUE(_ERModel.components.empty());
	_Presentation->addNodeAction('E', "E1");
	ASSERT_EQ(1, _ERModel.components.size());
	_Presentation->addNodeAction('A', "A1");
	ASSERT_EQ(2, _ERModel.components.size());
	_Presentation->connectNodeAction(_ERModel.components.at(0)->getId(), _ERModel.components.at(1)->getId(), "");
	ASSERT_EQ(3, _ERModel.components.size());
	_Presentation->addNodeAction('A', "A2");
	ASSERT_EQ(4, _ERModel.components.size());
	_Presentation->addNodeAction('R', "R1");
	ASSERT_EQ(5, _ERModel.components.size());
}

void PresentationTest::testLoadFileAction(){
	ASSERT_FALSE(_Presentation->loadFileAction("./testdata/test_file_not_exist.add"));	
	ASSERT_TRUE(_Presentation->loadFileAction("./testdata/test_file1.add"));
}

void PresentationTest::testSaveFileAction(){
	_Presentation->saveFileAction("./testdata/test_file2.add");
}

void PresentationTest::testAddNodeAction(){
	ASSERT_TRUE(_ERModel.components.empty());
	_Presentation->addNodeAction('E', "E1");
	ASSERT_EQ(1, _ERModel.components.size());
}

void PresentationTest::testDeleteNodeAction(){
	ASSERT_TRUE(_ERModel.components.empty());
	_Presentation->addNodeAction('E', "E1");
	ASSERT_EQ(1, _ERModel.components.size());
	_Presentation->deleteNodeAction(_ERModel.components.back()->getId());
	ASSERT_TRUE(_ERModel.components.empty());
}

void PresentationTest::testConnectNodeAction(){
	ASSERT_TRUE(_ERModel.components.empty());
	_Presentation->addNodeAction('E', "E1");
	ASSERT_EQ(1, _ERModel.components.size());
	_Presentation->addNodeAction('A', "A1");
	ASSERT_EQ(2, _ERModel.components.size());
	_Presentation->connectNodeAction(_ERModel.components.at(0)->getId(), _ERModel.components.at(1)->getId(), "");
	ASSERT_EQ(3, _ERModel.components.size());
	ASSERT_EQ("Connection", _ERModel.components.back()->getType());
}

void PresentationTest::testAddNodeCount(){
	ASSERT_EQ(0, _Presentation->attributeCount);
	ASSERT_EQ(0, _Presentation->entityCount);
	_Presentation->addNodeCount('A');
	ASSERT_EQ(1, _Presentation->attributeCount);
	_Presentation->addNodeCount('E');
	ASSERT_EQ(1, _Presentation->entityCount);
}

void PresentationTest::testDeleteNodeCount(){
	_Presentation->attributeCount = 10;
	_Presentation->entityCount = 8;
	_Presentation->deleteNodeCount('A');
	ASSERT_EQ(9, _Presentation->attributeCount);
	_Presentation->deleteNodeCount('E');
	ASSERT_EQ(7, _Presentation->entityCount);
}


void PresentationTest::testAddAttributeOfEntity(){
	ASSERT_TRUE(_ERModel.components.empty());
	_Presentation->addNodeAction('E', "E1");
	ASSERT_EQ(1, _ERModel.components.size());
	_Presentation->addNodeAction('A', "A1");
	ASSERT_EQ(2, _ERModel.components.size());
	_Presentation->addNodeAction('A', "A2");
	ASSERT_EQ(3, _ERModel.components.size());
	_Presentation->addNodeAction('E', "E2");
	ASSERT_EQ(4, _ERModel.components.size());
	_Presentation->addNodeAction('A', "A3");
	ASSERT_EQ(5, _ERModel.components.size());
	_Presentation->connectNodeAction(_ERModel.components.at(0)->getId(), _ERModel.components.at(1)->getId(), "");
	ASSERT_EQ(6, _ERModel.components.size());
	_Presentation->connectNodeAction(_ERModel.components.at(0)->getId(), _ERModel.components.at(2)->getId(), "");
	ASSERT_EQ(7, _ERModel.components.size());
	_Presentation->connectNodeAction(_ERModel.components.at(4)->getId(), _ERModel.components.at(5)->getId(), "");
	ASSERT_EQ(8, _ERModel.components.size());
	_Presentation->addAttributeOfEntity(_ERModel.components.at(0)->getId());
	ASSERT_EQ(2, _Presentation->attributeOfEntity.size());  //確認"E1"裡有兩個attribute
	ASSERT_EQ(1, _Presentation->attributeOfEntity.at(0));   //確認"A1"的ID是1
	ASSERT_EQ(2, _Presentation->attributeOfEntity.at(1));   //確認"A2"的ID是2
}

void PresentationTest::testAddPrimaryKey(){
	ASSERT_TRUE(_ERModel.components.empty());
	_Presentation->addNodeAction('E', "E1");
	ASSERT_EQ(1, _ERModel.components.size());
	_Presentation->addNodeAction('A', "A1");
	ASSERT_EQ(2, _ERModel.components.size());
	_Presentation->addNodeAction('A', "A2");
	ASSERT_EQ(3, _ERModel.components.size());
	_Presentation->connectNodeAction(_ERModel.components.at(0)->getId(), _ERModel.components.at(1)->getId(), "");
	ASSERT_EQ(4, _ERModel.components.size());
	_Presentation->connectNodeAction(_ERModel.components.at(0)->getId(), _ERModel.components.at(2)->getId(), "");
	ASSERT_EQ(5, _ERModel.components.size());
	_Presentation->addAttributeOfEntity(_ERModel.components.at(0)->getId());
	_Presentation->addPrimaryKey(_ERModel.components.at(0)->getId(), _Presentation->attributeOfEntity);
	Entity* conn = _ERModel.getPrimaryKey(0);
	ASSERT_EQ(1, conn->getAllPrimaryKey().at(0)->getId());
	ASSERT_EQ(2, conn->getAllPrimaryKey().at(1)->getId());
}

void PresentationTest::testIsNodesEmpty(){
	ASSERT_TRUE(_Presentation->isNodesEmpty());
	testingData();
	ASSERT_FALSE(_Presentation->isNodesEmpty());
}
void PresentationTest::testIsEntityEmpty(){
	ASSERT_TRUE(_Presentation->isEntityEmpty());
	testingData();
	ASSERT_FALSE(_Presentation->isEntityEmpty());
}
void PresentationTest::testIsAttributeOfEntityEmpty(){
	ASSERT_TRUE(_Presentation->isAttributeOfEntityEmpty());
	testingData();
	_Presentation->addAttributeOfEntity(_ERModel.components.at(0)->getId());
	ASSERT_FALSE(_Presentation->isAttributeOfEntityEmpty());
}

void PresentationTest::testIsBelongToEntity(){
	ASSERT_TRUE(_Presentation->isBelongToEntity(_Presentation->attributeOfEntity));
	testingData();
	_Presentation->addAttributeOfEntity(_ERModel.components.at(0)->getId());
	vector<int> input;
	input.push_back(3); //給定非Entity連接的attribute ID
	input.push_back(10);
	ASSERT_FALSE(_Presentation->isBelongToEntity(input));
	ASSERT_TRUE(_Presentation->isBelongToEntity(_Presentation->attributeOfEntity));
}

void PresentationTest::testCheckCommand(){
	ASSERT_EQ(-1, _Presentation->checkCommand("A"));
	ASSERT_EQ(0, _Presentation->checkCommand("0"));
	ASSERT_EQ(1, _Presentation->checkCommand("1"));
}

void PresentationTest::testCheckInput(){
	ASSERT_EQ('X', _Presentation->checkInput("GG"));
	ASSERT_EQ('E', _Presentation->checkInput("E"));
}

void PresentationTest::testCheckID(){
	testingData();
	ASSERT_FALSE(_Presentation->checkID(0));  
	ASSERT_TRUE(_Presentation->checkID(5));
}

void PresentationTest::testCheckType(){
	testingData();
	//fromID and toID are Entity
	ASSERT_TRUE(_Presentation->checkType(_ERModel.components[0]->getId(), _ERModel.components[0]->getId())); 
	//fromID and toID are Attribute
	ASSERT_TRUE(_Presentation->checkType(_ERModel.components[1]->getId(), _ERModel.components[1]->getId())); 
	//fromID and toID are Relation
	ASSERT_TRUE(_Presentation->checkType(_ERModel.components[4]->getId(), _ERModel.components[4]->getId())); 
	//fromID and toID are Connection
	ASSERT_TRUE(_Presentation->checkType(_ERModel.components[2]->getId(), _ERModel.components[2]->getId())); 
	//fromID is Attribute,  toID is Relation.
	ASSERT_TRUE(_Presentation->checkType(_ERModel.components[3]->getId(), _ERModel.components[4]->getId())); 
	//fromID is Relation,  toID is Attribute.
	ASSERT_TRUE(_Presentation->checkType(_ERModel.components[4]->getId(), _ERModel.components[3]->getId())); 
	//fromID is Connection,  toID isn't Connection.
	ASSERT_TRUE(_Presentation->checkType(_ERModel.components[2]->getId(), _ERModel.components[3]->getId())); 
	//fromID isn't Connection,  toID is Connection.
	ASSERT_TRUE(_Presentation->checkType(_ERModel.components[2]->getId(), _ERModel.components[3]->getId())); 
	//fromID is Entity,  toID is Attribute.
	ASSERT_FALSE(_Presentation->checkType(_ERModel.components[0]->getId(), _ERModel.components[3]->getId()));
	//fromID is Entity,  toID is Relation.
	ASSERT_FALSE(_Presentation->checkType(_ERModel.components[0]->getId(), _ERModel.components[4]->getId()));
}

void PresentationTest::testCheckCardinality(){
	testingData();
	//連接Entity和Relation
	_Presentation->connectNodeAction(_ERModel.components[0]->getId(), _ERModel.components[4]->getId(), "1");
	ASSERT_EQ(6, _ERModel.components.size());
	//fromID is Entity and toID is Relation
	ASSERT_TRUE(_Presentation->checkCardinality(_ERModel.components[0]->getId(), _ERModel.components[4]->getId()));
	//fromID is Relation and toID is Entity
	ASSERT_TRUE(_Presentation->checkCardinality(_ERModel.components[4]->getId(), _ERModel.components[0]->getId()));
	//else
	ASSERT_FALSE(_Presentation->checkCardinality(_ERModel.components[0]->getId(), _ERModel.components[3]->getId()));
}

void PresentationTest::testCheckAttributeOfEntity(){
	testingData();
	_Presentation->checkAttributeOfEntity(0, 1, 0);
	ASSERT_EQ(1, _Presentation->attributeOfEntity.back());
	_Presentation->checkAttributeOfEntity(1, 0, 0);
	ASSERT_EQ(1, _Presentation->attributeOfEntity.back());
}

void PresentationTest::testCheckCardinalityType(){
	ASSERT_EQ("1", _Presentation->checkCardinalityType('0'));
	ASSERT_EQ("N", _Presentation->checkCardinalityType('1'));
	ASSERT_EQ("", _Presentation->checkCardinalityType('2'));
}

void PresentationTest::testExistPathBetween(){
	testingData();
	//兩components有連接情況
	ASSERT_TRUE(_Presentation->existPathBetween(_ERModel.components[0]->getId(), _ERModel.components[1]->getId()));
	ASSERT_TRUE(_Presentation->existPathBetween(_ERModel.components[1]->getId(), _ERModel.components[0]->getId()));
	//兩components未連接情況
	ASSERT_FALSE(_Presentation->existPathBetween(_ERModel.components[0]->getId(), _ERModel.components[3]->getId()));
}

void PresentationTest::testGetErrorAttribute(){
	testingData();
	_Presentation->addAttributeOfEntity(_ERModel.components.at(0)->getId());
	vector<int> input;
	input.push_back(1); 
	input.push_back(3); 
	ASSERT_EQ(0,_Presentation->getErrorAttribute(input));
	input.push_back(10); 
	ASSERT_EQ(10,_Presentation->getErrorAttribute(input));
	input.clear();
}

//--------------GTest
TEST_F(PresentationTest, testLoadFileAction){
	testLoadFileAction();
}

TEST_F(PresentationTest, testSaveFileAction){
	testSaveFileAction();
}

TEST_F(PresentationTest, testAddNodeAction) {
	testAddNodeAction();
}

TEST_F(PresentationTest, testDeleteNodeAction) {
	testDeleteNodeAction();
}

TEST_F(PresentationTest, testConnectNodeAction) {
	testConnectNodeAction();
}

TEST_F(PresentationTest, testAddNodeCount) {
	testAddNodeCount();
}

TEST_F(PresentationTest, testDeleteNodeCount) {
	testDeleteNodeCount();
}

TEST_F(PresentationTest, testAddAttributeOfEntity) {
	testAddAttributeOfEntity();
}

TEST_F(PresentationTest, testAddPrimaryKey) {
	testAddPrimaryKey();
}

TEST_F(PresentationTest, testIsNodesEmpty) {
	testIsNodesEmpty();
}

TEST_F(PresentationTest, testIsEntityEmpty) {
	testIsEntityEmpty();
}

TEST_F(PresentationTest, testIsAttributeOfEntityEmpty) {
	testIsAttributeOfEntityEmpty();
}

TEST_F(PresentationTest, testIsBelongToEntity) {
	testIsBelongToEntity();
}

TEST_F(PresentationTest, testCheckCommand) {
	testCheckCommand();
}

TEST_F(PresentationTest, testCheckInput) {
	testCheckInput();
}

TEST_F(PresentationTest, testCheckID) {
	testCheckID();
}

TEST_F(PresentationTest, testCheckType) {
	testCheckType();
}

TEST_F(PresentationTest, testCheckCardinality) {
	testCheckCardinality();
}

TEST_F(PresentationTest, testCheckAttributeOfEntity) {
	testCheckAttributeOfEntity();
}

TEST_F(PresentationTest, testCheckCardinalityType) {
	testCheckCardinalityType();
}

TEST_F(PresentationTest, testExistPathBetween) {
	testExistPathBetween();
}

TEST_F(PresentationTest, testGetErrorAttribute) {
	testGetErrorAttribute();
}

