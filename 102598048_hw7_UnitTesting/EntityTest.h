#include <gtest/gtest.h>
#include "..\102598048_hw7\Connection.h"
#include "..\102598048_hw7\Attributes.h"
#include "..\102598048_hw7\Entity.h"
#include "..\102598048_hw7\Relationship.h"
#include "..\102598048_hw7\Components.h"

class EntityTest : public testing::Test{
	public:
		virtual void SetUp();
		virtual void TearDown();
		void testConnectTo();
		void testResetPrimaryKey();
	protected:
		Entity *_Entity;
		Components *_Node1, *_Node2;
};

