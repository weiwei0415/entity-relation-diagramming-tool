#include <gtest/gtest.h>
#include "..\102598048_hw7\GUIPresentation.h"

class GUIPresentationTest : public testing::Test{
	protected:
		virtual void SetUp();
		virtual void TearDown();
		void testOpenErdFileAction();
		void testAddNodeAction();
		void testAddConnectionAction();
		void testDeleteComponentAction();
		void testEditTheTextAction();
		void testSetPrimaryKeyAction();
		void testCheckRedoStackEmpty();
		void testCheckUndoStackEmpty();
	private:
		ERModel model;
		GUIPresentation *presentation;
		string path;
};

