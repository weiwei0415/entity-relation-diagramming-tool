#include "GUIPresentationTest.h"

void GUIPresentationTest::SetUp(){
	presentation = new GUIPresentation(&model);
	path = "testdata\\test_file1.add";
	_mkdir("testdata\\");
	ofstream outputFile(path);
	outputFile << "E, Engineer\n";
	outputFile << "A, Emp_ID\n";
	outputFile << "R, Has\n";
	outputFile << "A, Name\n";
	outputFile << "E, PC\n";
	outputFile << "A, PC_ID\n";
	outputFile << "A, Purchase_Date\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C, 1\n";
	outputFile << "C, 1\n";
	outputFile << "A, Department\n";
	outputFile << "C\n";
	outputFile << "\n";
	outputFile << "7 0,1\n";
	outputFile << "8 0,3\n";
	outputFile << "9 4,5\n";
	outputFile << "10 4,6\n";
	outputFile << "11 0,2\n";
	outputFile << "12 2,4\n";
	outputFile << "14 0,13\n";
	outputFile << "\n";
	outputFile << "0 1,3\n";
	outputFile << "4 5\n";
	outputFile.close();
}

void GUIPresentationTest::TearDown(){
	remove("testdata\\test_file1.add");
	_rmdir("testdata\\");
	delete presentation;
}

void GUIPresentationTest::testOpenErdFileAction(){
	ASSERT_FALSE(presentation->openErdFileAction("./testdata/test_file_not_exist.add"));	
	ASSERT_TRUE(presentation->openErdFileAction("./testdata/test_file1.add"));
}

void GUIPresentationTest::testCheckRedoStackEmpty(){
	ASSERT_TRUE(presentation->checkRedoStackEmpty());
	presentation->addNodeAction(Components::attributeType,"attribute");
	ASSERT_TRUE(presentation->checkRedoStackEmpty());
	presentation->undoAction();
	ASSERT_FALSE(presentation->checkRedoStackEmpty());
	presentation->redoAction();
	ASSERT_TRUE(presentation->checkRedoStackEmpty());
}

void GUIPresentationTest::testCheckUndoStackEmpty(){
	ASSERT_TRUE(presentation->checkUndoStackEmpty());
	presentation->addNodeAction(Components::attributeType,"A1");
	ASSERT_FALSE(presentation->checkUndoStackEmpty());
	presentation->undoAction();
	ASSERT_TRUE(presentation->checkUndoStackEmpty());
	presentation->redoAction();
	ASSERT_FALSE(presentation->checkUndoStackEmpty());
}

void GUIPresentationTest::testAddNodeAction(){
	presentation->addNodeAction(Components::attributeType, "A1");
	ASSERT_EQ(0, presentation->getComponents().back()->getId());
	ASSERT_EQ("Attribute", presentation->getComponents().back()->getType());
	ASSERT_EQ("A1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::entityType, "E1");
	ASSERT_EQ(1, presentation->getComponents().back()->getId());
	ASSERT_EQ("Entity", presentation->getComponents().back()->getType());
	ASSERT_EQ("E1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::relationType, "R1");
	ASSERT_EQ(2, presentation->getComponents().back()->getId());
	ASSERT_EQ("Relation", presentation->getComponents().back()->getType());
	ASSERT_EQ("R1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::connectionType, "C1");
	ASSERT_EQ(3, presentation->getComponents().back()->getId());
	ASSERT_EQ("Connection", presentation->getComponents().back()->getType());
	ASSERT_EQ("C1", presentation->getComponents().back()->getText());
}

void GUIPresentationTest::testAddConnectionAction(){
	presentation->addNodeAction(Components::attributeType, "A1");
	ASSERT_EQ(0, presentation->getComponents().back()->getId());
	ASSERT_EQ("Attribute", presentation->getComponents().back()->getType());
	ASSERT_EQ("A1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::entityType, "E1");
	ASSERT_EQ(1, presentation->getComponents().back()->getId());
	ASSERT_EQ("Entity", presentation->getComponents().back()->getType());
	ASSERT_EQ("E1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::relationType, "R1");
	ASSERT_EQ(2, presentation->getComponents().back()->getId());
	ASSERT_EQ("Relation", presentation->getComponents().back()->getType());
	ASSERT_EQ("R1", presentation->getComponents().back()->getText());
	presentation->addConnectionAction(0, 1, "");
	ASSERT_EQ(3, presentation->getComponents().back()->getId());
	ASSERT_EQ("Connection", presentation->getComponents().back()->getType());
	ASSERT_EQ("", presentation->getComponents().back()->getText());
	presentation->addConnectionAction(1, 2, "1");
	ASSERT_EQ(4, presentation->getComponents().back()->getId());
	ASSERT_EQ("Connection", presentation->getComponents().back()->getType());
	ASSERT_EQ("1", presentation->getComponents().back()->getText());
}

void GUIPresentationTest::testDeleteComponentAction(){
	presentation->addNodeAction(Components::attributeType, "A1");
	ASSERT_EQ(0, presentation->getComponents().back()->getId());
	ASSERT_EQ("Attribute", presentation->getComponents().back()->getType());
	ASSERT_EQ("A1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::entityType, "E1");
	ASSERT_EQ(1, presentation->getComponents().back()->getId());
	ASSERT_EQ("Entity", presentation->getComponents().back()->getType());
	ASSERT_EQ("E1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::relationType, "R1");
	ASSERT_EQ(2, presentation->getComponents().back()->getId());
	ASSERT_EQ("Relation", presentation->getComponents().back()->getType());
	ASSERT_EQ("R1", presentation->getComponents().back()->getText());
	presentation->addConnectionAction(0, 1, "");
	ASSERT_EQ(3, presentation->getComponents().back()->getId());
	ASSERT_EQ("Connection", presentation->getComponents().back()->getType());
	ASSERT_EQ("", presentation->getComponents().back()->getText());
	presentation->addConnectionAction(1, 2, "1");
	ASSERT_EQ(4, presentation->getComponents().back()->getId());
	ASSERT_EQ("Connection", presentation->getComponents().back()->getType());
	ASSERT_EQ("1", presentation->getComponents().back()->getText());
	ASSERT_EQ(5, presentation->getComponents().size());
	presentation->deleteComponentAction(4);
	ASSERT_EQ(4, presentation->getComponents().size());
	presentation->deleteComponentAction(1);
	ASSERT_EQ(2, presentation->getComponents().size());
	presentation->deleteComponentAction(0);
	ASSERT_EQ(1, presentation->getComponents().size());
	presentation->deleteComponentAction(2);
	ASSERT_EQ(0, presentation->getComponents().size());
}

void GUIPresentationTest::testEditTheTextAction(){
	presentation->addNodeAction(Components::attributeType, "A1");
	ASSERT_EQ(0, presentation->getComponents().back()->getId());
	ASSERT_EQ("Attribute", presentation->getComponents().back()->getType());
	ASSERT_EQ("A1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::entityType, "E1");
	ASSERT_EQ(1, presentation->getComponents().back()->getId());
	ASSERT_EQ("Entity", presentation->getComponents().back()->getType());
	ASSERT_EQ("E1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::relationType, "R1");
	ASSERT_EQ(2, presentation->getComponents().back()->getId());
	ASSERT_EQ("Relation", presentation->getComponents().back()->getType());
	ASSERT_EQ("R1", presentation->getComponents().back()->getText());
	presentation->editTheTextAction(0, "Attribute1");
	ASSERT_EQ("Attribute1", presentation->getComponents()[0]->getText());
	presentation->editTheTextAction(1, "Entity1");
	ASSERT_EQ("Entity1", presentation->getComponents()[1]->getText());
	presentation->editTheTextAction(2, "Relation1");
	ASSERT_EQ("Relation1", presentation->getComponents()[2]->getText());
}

void GUIPresentationTest::testSetPrimaryKeyAction(){
	presentation->addNodeAction(Components::attributeType, "A1");
	ASSERT_EQ(0, presentation->getComponents().back()->getId());
	ASSERT_EQ("Attribute", presentation->getComponents().back()->getType());
	ASSERT_EQ("A1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::entityType, "E1");
	ASSERT_EQ(1, presentation->getComponents().back()->getId());
	ASSERT_EQ("Entity", presentation->getComponents().back()->getType());
	ASSERT_EQ("E1", presentation->getComponents().back()->getText());
	presentation->addNodeAction(Components::relationType, "R1");
	ASSERT_EQ(2, presentation->getComponents().back()->getId());
	ASSERT_EQ("Relation", presentation->getComponents().back()->getType());
	ASSERT_EQ("R1", presentation->getComponents().back()->getText());
	presentation->addConnectionAction(0, 1, "");
	ASSERT_EQ(3, presentation->getComponents().back()->getId());
	ASSERT_EQ("Connection", presentation->getComponents().back()->getType());
	ASSERT_EQ("", presentation->getComponents().back()->getText());
	presentation->addConnectionAction(1, 2, "1");
	ASSERT_EQ(4, presentation->getComponents().back()->getId());
	ASSERT_EQ("Connection", presentation->getComponents().back()->getType());
	ASSERT_EQ("1", presentation->getComponents().back()->getText());

	Entity* entity = dynamic_cast <Entity*> (presentation->getComponents()[1]);
	ASSERT_TRUE(entity->getAllPrimaryKey().empty());
	presentation->setPrimaryKeyAction(1, 0);
	ASSERT_EQ(1, entity->getAllPrimaryKey().size());
	ASSERT_EQ(0, entity->getAllPrimaryKey()[0]->getId());
}

TEST_F(GUIPresentationTest, testOpenAddFileAction){
	testOpenErdFileAction();
}

TEST_F(GUIPresentationTest, testCheckRedoStackEmpty){
	testCheckRedoStackEmpty();
}

TEST_F(GUIPresentationTest, testCheckUndoStackEmpty){
	testCheckUndoStackEmpty();
}

TEST_F(GUIPresentationTest, testAddNodeAction){
	testAddNodeAction();
}

TEST_F(GUIPresentationTest, testAddConnectionAction){
	testAddConnectionAction();
}

TEST_F(GUIPresentationTest, testDeleteComponentAction){
	testDeleteComponentAction();
}

TEST_F(GUIPresentationTest, testEditTheTextAction){
	testEditTheTextAction();
}

TEST_F(GUIPresentationTest, testSetPrimaryKeyAction){
	testSetPrimaryKeyAction();
}



