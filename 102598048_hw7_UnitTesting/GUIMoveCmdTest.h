#include <gtest/gtest.h>
#include "..\102598048_hw7\GUIMoveCmd.h"

class GUIMoveCmdTest : public testing::Test{
	protected:
		virtual void SetUp();
		virtual void TearDown();
		void testExecute();
		void testUnexecute();
	private:
		ERModel* model;
		GUIMoveCmd *moveCmd;
};
