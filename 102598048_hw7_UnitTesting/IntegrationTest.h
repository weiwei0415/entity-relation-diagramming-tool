#include <gtest/gtest.h>
#include "..\102598048_hw7\Presentation.h"

class IntegrationTest : public testing::Test{
	protected:
		virtual void SetUp();
		virtual void TearDown();
		void testLoadFileNotExist();
		void testIsPrimaryExist();
		void testUndoDeleteComponent();
		void testRedoConnectComponent();
		void testCommonUsage();
	private:
		ERModel _ERModel;
		Presentation *_Presentation;
		string _Path;
};

