#include "NodeTest.h"

void NodeTest::SetUp(){
	_Components = new Attributes();
	//Entity
	_Node = new Entity();
	_Node->setId(0);
	_Node->setType(ENTITY_);
	_Node->setText("E1");
}

void NodeTest::TearDown(){
	delete _Components;
}

void NodeTest::testConnectTo(){
	ASSERT_TRUE(_Components->getConnection().empty());
	//Entity
	_Components->connectTo(_Node);
	ASSERT_EQ(1, _Components->getConnection().size());
	ASSERT_EQ(0, _Components->getConnection().back()->getId());
	ASSERT_EQ(ENTITY_, _Components->getConnection().back()->getType());
	ASSERT_EQ("E1", _Components->getConnection().back()->getText());
}

//-----GTest-----
TEST_F(NodeTest, testConnectTo){
	testConnectTo();
}