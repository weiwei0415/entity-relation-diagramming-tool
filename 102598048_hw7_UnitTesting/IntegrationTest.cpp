#include "IntegrationTest.h"

void IntegrationTest::SetUp(){
	_Presentation = new Presentation(&_ERModel);
	_Path = "testdata\\test_file1.add";
	_mkdir("testdata\\");
	ofstream outputFile(_Path);
	outputFile << "E, Engineer\n";
	outputFile << "A, Emp_ID\n";
	outputFile << "R, Has\n";
	outputFile << "A, Name\n";
	outputFile << "E, PC\n";
	outputFile << "A, PC_ID\n";
	outputFile << "A, Purchase_Date\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C, 1\n";
	outputFile << "C, 1\n";
	outputFile << "A, Department\n";
	outputFile << "C\n";
	outputFile << "\n";
	outputFile << "7 0,1\n";
	outputFile << "8 0,3\n";
	outputFile << "9 4,5\n";
	outputFile << "10 4,6\n";
	outputFile << "11 0,2\n";
	outputFile << "12 2,4\n";
	outputFile << "14 0,13\n";
	outputFile << "\n";
	outputFile << "0 1,3\n";
	outputFile << "4 5\n";
	outputFile.close();
}

void IntegrationTest::TearDown(){
	remove("testdata\\test_file1.add");
	_rmdir("testdata\\");
	delete _Presentation;
}

void IntegrationTest::testLoadFileNotExist(){
	//Assert file not found error
	ASSERT_FALSE(_Presentation->loadFileAction("./testdata/file_not_exist.add"));
}

void IntegrationTest::testIsPrimaryExist(){
	//Assert the diagram is loaded correctly
	ASSERT_TRUE(_Presentation->loadFileAction("./testdata/test_file1.add"));
	_Presentation->addAttributeOfEntity(_ERModel.components.at(0)->getId());
	vector<int> input;
	input.push_back(1); //Emp_ID 
	input.push_back(3); //Name 
	_Presentation->addPrimaryKey(_ERModel.components.at(0)->getId(),input);
	//Assert Engineer��s primary key is ��Name�� and ��Emp_ID��
	Entity* conn = _ERModel.getPrimaryKey(0);
	ASSERT_EQ(1, conn->getAllPrimaryKey().at(0)->getId());
	ASSERT_EQ(3, conn->getAllPrimaryKey().at(1)->getId());
	//Assert PC��s primary key is ��PC_ID�� 
	conn = _ERModel.getPrimaryKey(4);
	ASSERT_EQ(5, conn->getAllPrimaryKey().at(0)->getId());
	//Assert PC��s primary key is not ��Purchase_Date��
	for(size_t i = 0;i < conn->getAllPrimaryKey().size(); i++)
		ASSERT_NE(6, conn->getAllPrimaryKey().at(i)->getId());
}

void IntegrationTest::testUndoDeleteComponent(){
	//Assert the diagram is loaded correctly
	ASSERT_TRUE(_Presentation->loadFileAction("./testdata/test_file1.add"));
	//Add an entity with text ��Test�� 
	ASSERT_EQ(15, _ERModel.components.size());
	_Presentation->addNodeAction('E', "Test");
	//Assert the entity is added correctly
	ASSERT_EQ(15, _ERModel.components.back()->getId());
	ASSERT_EQ("Entity", _ERModel.components.back()->getType());
	ASSERT_EQ("Test", _ERModel.components.back()->getText());
	ASSERT_EQ(16, _ERModel.components.size());
	//Delete the entity added above 
	_Presentation->deleteNodeAction(_ERModel.components.back()->getId());
	//Assert the entity has been deleted
	ASSERT_EQ(15, _ERModel.components.size());
	//Undo 
	_Presentation->undo();
	//Assert there is an entity with text ��Test��
	ASSERT_EQ(15, _ERModel.components.back()->getId());
	ASSERT_EQ("Entity", _ERModel.components.back()->getType());
	ASSERT_EQ("Test", _ERModel.components.back()->getText());
	ASSERT_EQ(16, _ERModel.components.size());
}

void IntegrationTest::testRedoConnectComponent(){
	//Assert the diagram is loaded correctly
	ASSERT_TRUE(_Presentation->loadFileAction("./testdata/test_file1.add"));
	//Add an entity with text ��Test��
	ASSERT_EQ(15, _ERModel.components.size());
	_Presentation->addNodeAction('E', "Test");
	//Assert the entity is added correctly
	ASSERT_EQ(15, _ERModel.components.back()->getId());
	ASSERT_EQ("Entity", _ERModel.components.back()->getType());
	ASSERT_EQ("Test", _ERModel.components.back()->getText());
	ASSERT_EQ(16, _ERModel.components.size());
	//Add an attribute with text ��Test Attr��
	_Presentation->addNodeAction('A', "Test Attr");
	//Assert the attribute is added correctly 
	ASSERT_EQ(16, _ERModel.components.back()->getId());
	ASSERT_EQ("Attribute", _ERModel.components.back()->getType());
	ASSERT_EQ("Test Attr", _ERModel.components.back()->getText());
	ASSERT_EQ(17, _ERModel.components.size());
	//Connect node 15 and 16 
	_Presentation->connectNodeAction(_ERModel.components.at(15)->getId(), _ERModel.components.at(16)->getId(), "");
	//Assert there is a connection between node 15 and 16 
	ASSERT_EQ(15, _ERModel.components[17]->getConnection()[0]->getId());
	ASSERT_EQ(16, _ERModel.components[17]->getConnection()[1]->getId());
	ASSERT_EQ(17, _ERModel.components[15]->getConnection()[0]->getId());  
	ASSERT_EQ(17, _ERModel.components[16]->getConnection()[0]->getId());
	ASSERT_EQ(18, _ERModel.components.size());
	//Undo 
	_Presentation->undo();
	//Assert there is no connection between node 15 and 16
	for(size_t i = 0 ; i < _ERModel.components.size() ; i++){
		if(!(_ERModel.components[i]->getConnection().empty()) && _ERModel.components[i]->getType() == "Connection"){
			ASSERT_NE(15, _ERModel.components[i]->getConnection()[0]->getId());
			ASSERT_NE(16, _ERModel.components[i]->getConnection()[1]->getId());	
		}
		ASSERT_NE(i, _ERModel.components[15]->getConnection()[0]->getId());  
		ASSERT_NE(i, _ERModel.components[16]->getConnection()[0]->getId());
	}	
	ASSERT_EQ(17, _ERModel.components.size());
	//Redo
	_Presentation->redo();
	//Assert node 15 and 16 are connected 
	ASSERT_EQ(15, _ERModel.components[17]->getConnection()[0]->getId());
	ASSERT_EQ(16, _ERModel.components[17]->getConnection()[1]->getId());
	ASSERT_EQ(17, _ERModel.components[15]->getConnection()[0]->getId());  
	ASSERT_EQ(17, _ERModel.components[16]->getConnection()[0]->getId());
	ASSERT_EQ(18, _ERModel.components.size());
}

void IntegrationTest::testCommonUsage(){
	//Assert the diagram is loaded correctly
	ASSERT_TRUE(_Presentation->loadFileAction("./testdata/test_file1.add"));
	//Assert the diagram is loaded correctly 
	ASSERT_EQ(15, _ERModel.components.size());
	_Presentation->addNodeAction('E', "Work Diary");
	//Assert the Entity is added correctly
	ASSERT_EQ(15,_ERModel.components.back()->getId());
	ASSERT_EQ("Entity", _ERModel.components.back()->getType());
	ASSERT_EQ("Work Diary", _ERModel.components.back()->getText());
	ASSERT_EQ(16, _ERModel.components.size());
	//Add a Relationship with text "Write"
	_Presentation->addNodeAction('R', "Write");
	//Assert the Relationship is added correctly
	ASSERT_EQ(16, _ERModel.components.back()->getId());
	ASSERT_EQ("Relation", _ERModel.components.back()->getType());
	ASSERT_EQ("Write", _ERModel.components.back()->getText());
	ASSERT_EQ(17, _ERModel.components.size());
	//Connect node 0 and 16, and set its cardinality as "1"
	_Presentation->connectNodeAction(_ERModel.components.at(0)->getId(), _ERModel.components.at(16)->getId(), "1");
	//Assert node 0 and 16 are connected
	ASSERT_EQ(0, _ERModel.components[17]->getConnection()[0]->getId());
	ASSERT_EQ(16, _ERModel.components[17]->getConnection()[1]->getId());
	ASSERT_EQ(17, _ERModel.components[0]->getConnection()[4]->getId());  
	ASSERT_EQ(17, _ERModel.components[16]->getConnection()[0]->getId());
	ASSERT_EQ(18, _ERModel.components.size());
	//Connect node 15 and 16
	_Presentation->connectNodeAction(_ERModel.components.at(15)->getId(), _ERModel.components.at(16)->getId(), "1");
	//Assert node 15 and 16 are connected
	ASSERT_EQ(15, _ERModel.components[18]->getConnection()[0]->getId());
	ASSERT_EQ(16, _ERModel.components[18]->getConnection()[1]->getId());
	ASSERT_EQ(18, _ERModel.components[15]->getConnection()[0]->getId());  
	ASSERT_EQ(18, _ERModel.components[16]->getConnection()[1]->getId());
	ASSERT_EQ(19, _ERModel.components.size());
	//Add an Attribute with text "Content" 
	_Presentation->addNodeAction('A', "Content");
	//Assert the Attribute is added correctly
	ASSERT_EQ(19, _ERModel.components.back()->getId());
	ASSERT_EQ("Attribute", _ERModel.components.back()->getType());
	ASSERT_EQ("Content", _ERModel.components.back()->getText());
	ASSERT_EQ(20, _ERModel.components.size());
	//Add an Attribute with text "WD_ID" 
	_Presentation->addNodeAction('A', "WD_ID");
	//Assert the Attribute is added correctly 
	ASSERT_EQ(20, _ERModel.components.back()->getId());
	ASSERT_EQ("Attribute", _ERModel.components.back()->getType());
	ASSERT_EQ("WD_ID", _ERModel.components.back()->getText());
	ASSERT_EQ(21, _ERModel.components.size());
	//Add an Attribute with text "WD_date"
	_Presentation->addNodeAction('A',"WD_date");
	//Assert the Attribute is added correctly
	ASSERT_EQ(21, _ERModel.components.back()->getId());
	ASSERT_EQ("Attribute", _ERModel.components.back()->getType());
	ASSERT_EQ("WD_date", _ERModel.components.back()->getText());
	ASSERT_EQ(22, _ERModel.components.size());
	//Connect node 15 and 19
	_Presentation->connectNodeAction(_ERModel.components.at(15)->getId(), _ERModel.components.at(19)->getId(), "1");
	//. Assert node 15 and 19 are connected 
	ASSERT_EQ(15, _ERModel.components[22]->getConnection()[0]->getId());
	ASSERT_EQ(19, _ERModel.components[22]->getConnection()[1]->getId());
	ASSERT_EQ(22, _ERModel.components[15]->getConnection()[1]->getId());  
	ASSERT_EQ(22, _ERModel.components[19]->getConnection()[0]->getId());
	ASSERT_EQ(23, _ERModel.components.size());
	//Connect node 15 and 20
	_Presentation->connectNodeAction(_ERModel.components.at(15)->getId(), _ERModel.components.at(20)->getId(), "1");
	//Assert node 15 and 20 are connected
	ASSERT_EQ(15, _ERModel.components[23]->getConnection()[0]->getId());
	ASSERT_EQ(20, _ERModel.components[23]->getConnection()[1]->getId());
	ASSERT_EQ(23, _ERModel.components[15]->getConnection()[2]->getId());  
	ASSERT_EQ(23, _ERModel.components[20]->getConnection()[0]->getId());
	ASSERT_EQ(24, _ERModel.components.size());
	//Connect node 15 and 21 
	_Presentation->connectNodeAction(_ERModel.components.at(15)->getId(), _ERModel.components.at(21)->getId(), "1");
	// Assert node 15 and 21 are connected 
	ASSERT_EQ(15, _ERModel.components[24]->getConnection()[0]->getId());
	ASSERT_EQ(21, _ERModel.components[24]->getConnection()[1]->getId());
	ASSERT_EQ(24, _ERModel.components[15]->getConnection()[3]->getId());  
	ASSERT_EQ(24, _ERModel.components[21]->getConnection()[0]->getId());
	ASSERT_EQ(25, _ERModel.components.size());
	//Set "Work Diary" primary key "WD_ID" 
	_Presentation->addAttributeOfEntity(_ERModel.components.at(15)->getId());
	vector<int> input;
	input.push_back(20); //WD_ID
	_Presentation->addPrimaryKey(_ERModel.components.at(15)->getId(), input);
	//Assert "Work Diary" has the primary key "WD_ID"
	Entity* conn =  _ERModel.getPrimaryKey(15);
	ASSERT_EQ(20, conn->getAllPrimaryKey().at(0)->getId());
	//Assert the Entity "Work Diary" exists 
	ASSERT_EQ(15, _ERModel.components.at(15)->getId());
	ASSERT_EQ("Entity", _ERModel.components.at(15)->getType());
	ASSERT_EQ("Work Diary", _ERModel.components.at(15)->getText());
	//Delete the Entity he/she added above 
	_Presentation->deleteNodeAction(_ERModel.components.at(15)->getId());
	//Assert the Entity has been deleted
	for (size_t i = 0 ; i < _ERModel.components.size() ; i++)
		ASSERT_NE(15, _ERModel.components.at(i)->getId());
	/*Assert there is no connection between node 15 and 16 
	  Assert there is no connection between node 15 and 19 
	  Assert there is no connection between node 15 and 20 
	  Assert there is no connection between node 15 and 21 */
	for (size_t i = 0 ; i < _ERModel.components.size() ; i++) {
		ASSERT_NE(18, _ERModel.components.at(i)->getId());
		ASSERT_NE(22, _ERModel.components.at(i)->getId());
		ASSERT_NE(23, _ERModel.components.at(i)->getId());
		ASSERT_NE(24, _ERModel.components.at(i)->getId());
	}
	//Assert the Entity "Work Diary" does not exist 
	for (size_t i = 0 ; i < _ERModel.components.size() ; i++)
		ASSERT_NE("Work Diary", _ERModel.components.at(i)->getText());
	//Assert Engineer's primary key is "Name" and "Emp_ID" 
	conn = _ERModel.getPrimaryKey(0);
	ASSERT_EQ(1, conn->getAllPrimaryKey().at(0)->getId());
	ASSERT_EQ(3, conn->getAllPrimaryKey().at(1)->getId());
	//Undo
	_Presentation->undo();
	//Assert Work Diary's primary key is "WD_ID"
	conn = _ERModel.getPrimaryKey(15);
	ASSERT_EQ(20, conn->getAllPrimaryKey().at(0)->getId());  //GG
	//Redo
	_Presentation->redo();
	//Assert the Entity "Work Diary" does not exist
	for (size_t i = 0 ; i < _ERModel.components.size() ; i++ )
		ASSERT_NE("Work Diary", _ERModel.components.at(i)->getText());
	//Assert Engineer's primary key is "Name" and "Emp_ID" 
	conn = _ERModel.getPrimaryKey(0);
	ASSERT_EQ(1, conn->getAllPrimaryKey().at(0)->getId());
	ASSERT_EQ(3, conn->getAllPrimaryKey().at(1)->getId());
}

//--------------GTest
TEST_F(IntegrationTest, testLoadFileNotExist) {
	testLoadFileNotExist();
}

TEST_F(IntegrationTest, testIsPrimaryExist) {
	testIsPrimaryExist();
}

TEST_F(IntegrationTest, testUndoDeleteComponent) {
	testUndoDeleteComponent();
}

TEST_F(IntegrationTest, testRedoConnectComponent) {
	testRedoConnectComponent();
}

TEST_F(IntegrationTest, testCommonUsage) {
	testCommonUsage();
}

