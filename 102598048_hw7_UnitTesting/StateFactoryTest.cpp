#include "StateFactoryTest.h"

void StateFactoryTest::SetUp(){
}

void StateFactoryTest::TearDown(){
	delete addNodeState, connectState, pointState, setPrimaryKeyState, presentation;
}

TEST_F(StateFactoryTest,testStateFactory){
	addNodeState = StateFactory::createState(0, presentation);
	ASSERT_EQ(false, addNodeState->canViewItemSelectable());
	ASSERT_EQ(false, addNodeState->canViewItemMovwble());
	ASSERT_EQ(false, addNodeState->canEdgeSelectable());
	ASSERT_EQ(false, addNodeState->canEdgeMoveble());
	connectState = StateFactory::createState(1, presentation);
	ASSERT_EQ(false, connectState->canViewItemSelectable());
	ASSERT_EQ(true, connectState->canViewItemMovwble());
	ASSERT_EQ(false, connectState->canEdgeSelectable());
	ASSERT_EQ(true, connectState->canEdgeMoveble());
	pointState = StateFactory::createState(2, presentation);
	ASSERT_EQ(true, pointState->canViewItemSelectable());
	ASSERT_EQ(true, pointState->canViewItemMovwble());
	ASSERT_EQ(true, pointState->canEdgeSelectable());
	ASSERT_EQ(true, pointState->canEdgeMoveble());
	setPrimaryKeyState = StateFactory::createState(3, presentation);
	ASSERT_EQ(false, setPrimaryKeyState->canViewItemSelectable());
	ASSERT_EQ(false, setPrimaryKeyState->canViewItemMovwble());
	ASSERT_EQ(false, setPrimaryKeyState->canEdgeSelectable());
	ASSERT_EQ(false, setPrimaryKeyState->canEdgeMoveble());
}
