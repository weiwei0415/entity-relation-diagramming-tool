#include "RelationshipTest.h"

void RelationshipTest::SetUp() {
	_Components = new Attributes();
	//Connection
	_Node = new Connection();
	_Node->setId(0);
	_Node->setType(CONNECTION_);
	_Node->setText("C1");
}

void RelationshipTest::TearDown() {
	delete _Components;
}

void RelationshipTest::testConnectTo() {
	ASSERT_TRUE(_Components->getConnection().empty());
	//Entity
	_Components->connectTo(_Node);
	ASSERT_EQ(1, _Components->getConnection().size());
	ASSERT_EQ(0, _Components->getConnection().back()->getId());
	ASSERT_EQ(CONNECTION_, _Components->getConnection().back()->getType());
	ASSERT_EQ("C1", _Components->getConnection().back()->getText());
}

//-----GTest-----
TEST_F(RelationshipTest, testConnectTo) {
	testConnectTo();
}