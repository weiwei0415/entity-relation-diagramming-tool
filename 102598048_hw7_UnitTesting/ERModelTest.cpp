#include "ERModelTest.h"

void ERModelTest::SetUp(){
	_ERModel = new ERModel();
	_Path = "testdata\\test_file1.add";
	_mkdir("testdata\\");
	ofstream outputFile(_Path);
	outputFile << "E, Engineer\n";
	outputFile << "A, Emp_ID\n";
	outputFile << "R, Has\n";
	outputFile << "A, Name\n";
	outputFile << "E, PC\n";
	outputFile << "A, PC_ID\n";
	outputFile << "A, Purchase_Date\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C\n";
	outputFile << "C, 1\n";
	outputFile << "C, 1\n";
	outputFile << "A, Department\n";
	outputFile << "C\n";
	outputFile << "\n";
	outputFile << "7 0,1\n";
	outputFile << "8 0,3\n";
	outputFile << "9 4,5\n";
	outputFile << "10 4,6\n";
	outputFile << "11 0,2\n";
	outputFile << "12 2,4\n";
	outputFile << "14 0,13\n";
	outputFile << "\n";
	outputFile << "0 1,3\n";
	outputFile << "4 5\n";
	outputFile.close();
}

void ERModelTest::TearDown(){
	remove("testdata\\test_file1.add");
	_rmdir("testdata\\");
	delete _ERModel;
}

void ERModelTest::testingData(){
	ASSERT_TRUE(_ERModel->components.empty());
	_ERModel->addNode('E', "E1", _ERModel->nodeID);
	ASSERT_EQ(1, _ERModel->components.size());
	_ERModel->addNode('A', "A1", _ERModel->nodeID);
	ASSERT_EQ(2, _ERModel->components.size());
	_ERModel->addNode('C', "", _ERModel->nodeID);
	ASSERT_EQ(3, _ERModel->components.size());
	_ERModel->addConnection(_ERModel->components.back()->getId(), _ERModel->components[0], _ERModel->components[1], "");
	_ERModel->addNode('A', "A2", _ERModel->nodeID);
	ASSERT_EQ(4, _ERModel->components.size());
	_ERModel->addNode('R', "R1", _ERModel->nodeID);
	ASSERT_EQ(5, _ERModel->components.size());
}

void ERModelTest::testLoadFile(){
	ASSERT_FALSE(_ERModel->loadFile("./testdata/test_file_not_exist.add"));
	_ERModel->addNode('E', "E1", _ERModel->nodeID);
	ASSERT_EQ(1, _ERModel->components.size());
	ASSERT_TRUE(_ERModel->loadFile("./testdata/test_file1.add"));
}

void ERModelTest::testLoadFileAddNodes(){
	_ERModel->loadFileAddNodes("", "E, Engineer", "");
	ASSERT_EQ(1, _ERModel->components.size());
	ASSERT_EQ(0, _ERModel->components.back()->getId());
	ASSERT_EQ("Entity", _ERModel->components.back()->getType());
	ASSERT_EQ("Engineer", _ERModel->components.back()->getText());
}
void ERModelTest::testLoadFileAddConnections(){
	_ERModel->loadFileAddNodes("", "E, Engineer", "");
	_ERModel->loadFileAddNodes("", "A, Emp_ID", "");
	_ERModel->loadFileAddNodes("", "R, Has", "");
	_ERModel->loadFileAddNodes("", "R, Write", "");
	_ERModel->loadFileAddNodes("", "C", "");
	_ERModel->loadFileAddConnections("", "4 0,1", "");
	ASSERT_EQ(5, _ERModel->components.size());
	ASSERT_EQ(4, _ERModel->components.back()->getId());
	ASSERT_EQ("Connection", _ERModel->components.back()->getType());
	ASSERT_EQ("", _ERModel->components.back()->getText());
	_ERModel->loadFileAddNodes("", "C, 1", "");
	_ERModel->loadFileAddConnections("", "5 0,2", "");
	ASSERT_EQ(6, _ERModel->components.size());
	ASSERT_EQ(5, _ERModel->components.back()->getId());
	ASSERT_EQ("Connection", _ERModel->components.back()->getType());
	ASSERT_EQ("1", _ERModel->components.back()->getText());
	_ERModel->loadFileAddNodes("", "C, N", "");
	_ERModel->loadFileAddConnections("", "6 0,3", "");
	ASSERT_EQ(7, _ERModel->components.size());
	ASSERT_EQ(6, _ERModel->components.back()->getId());
	ASSERT_EQ("Connection", _ERModel->components.back()->getType());
	ASSERT_EQ("N", _ERModel->components.back()->getText());	
}
void ERModelTest::testLoadFileAddPrimaryKeys(){
	_ERModel->loadFileAddNodes("", "E, Engineer", "");
	_ERModel->loadFileAddNodes("", "A, Emp_ID", "");
	_ERModel->loadFileAddNodes("", "A, Emp_Name", "");
	_ERModel->loadFileAddNodes("", "C", "");
	_ERModel->loadFileAddConnections("", "2 0,1", "");
	_ERModel->loadFileAddNodes("", "C", "");
	_ERModel->loadFileAddConnections("", "3 0,2", "");
	_ERModel->loadFileAddPrimaryKeys("", "0 1,2" , "");
	Entity* conn = _ERModel->getPrimaryKey(0);
	ASSERT_EQ(1, conn->getAllPrimaryKey().at(0)->getId());
	ASSERT_EQ(2, conn->getAllPrimaryKey().at(1)->getId());
}

void ERModelTest::testSaveFile(){
	_ERModel->loadFile("./testdata/test_file1.add");
	_ERModel->saveFile("./testdata/test_file2.add");
}

void ERModelTest::testSaveFileAddNodes(){
	_ERModel->loadFile("./testdata/test_file1.add");
	_ERModel->saveFile("./testdata/test_file_for_nodes.add");
}

void ERModelTest::testSaveFileAddConnections(){
	_ERModel->loadFile("./testdata/test_file1.add");
	_ERModel->saveFile("./testdata/test_file_for_connections.add");
}

void ERModelTest::testSaveFileAddPrimaryKeys(){
	_ERModel->loadFile("./testdata/test_file1.add");
	_ERModel->saveFile("./testdata/test_file_for_primarykeys.add");
}

void ERModelTest::testAddNode(){ 
	testingData();
	_ERModel->addNode('E', "E2", _ERModel->nodeID);
	ASSERT_EQ(6, _ERModel->components.size());
}

void ERModelTest::testAddConnectionNode(){
	testingData();	
	_ERModel->addNode('C', "", _ERModel->nodeID);
	ASSERT_EQ(6, _ERModel->components.size());
	ASSERT_EQ(5, _ERModel->components.back()->getId());
	ASSERT_EQ("Connection", _ERModel->components.back()->getType());
	ASSERT_EQ("", _ERModel->components.back()->getText());
}

void ERModelTest::testAddConnection(){
	testingData();
	_ERModel->addNode('C', "", _ERModel->nodeID);
	ASSERT_EQ(6, _ERModel->components.size());
	_ERModel->addConnection(_ERModel->components.back()->getId(), _ERModel->components[0], _ERModel->components[3], "");
	ASSERT_EQ(0, _ERModel->components[5]->getConnection()[0]->getId());
	ASSERT_EQ(3, _ERModel->components[5]->getConnection()[1]->getId());
	ASSERT_EQ(5, _ERModel->components[0]->getConnection()[1]->getId());  
	ASSERT_EQ(5, _ERModel->components[3]->getConnection()[0]->getId());
}

void ERModelTest::testAddPrimaryKey(){
	testingData();
	vector<int> input;
	input.push_back(1); //新增欲設定成PK的attribute ID
	_ERModel->addPrimaryKey(_ERModel->components[0]->getId(), input);
	Entity* conn = _ERModel->getPrimaryKey(0);
	ASSERT_EQ(1, conn->getAllPrimaryKey().at(0)->getId());
}

void ERModelTest::testDeleteComponent(){
	testingData();
	_ERModel->deleteComponent(_ERModel->components[1]->getId());
	ASSERT_EQ(3, _ERModel->components.size());
	_ERModel->deleteComponent(_ERModel->components[0]->getId());
	ASSERT_EQ(2, _ERModel->components.size());
}

void ERModelTest::testDeleteConnection(){
	testingData();
	_ERModel->deleteConnection(_ERModel->components[2]->getId());
	ASSERT_EQ(4, _ERModel->components.size());
}

void ERModelTest::testDeleteNode(){
	testingData();
	_ERModel->deleteNode(_ERModel->components[1]->getId());  //刪除有連接的節點
	ASSERT_EQ(3, _ERModel->components.size());
	_ERModel->deleteNode(_ERModel->components[0]->getId());  //刪除未連接的節點
	ASSERT_EQ(2, _ERModel->components.size());
}

void ERModelTest::testCheckCommand(){
	ASSERT_EQ(-1, _ERModel->checkCommand("A"));
	ASSERT_EQ(0, _ERModel->checkCommand("0"));
	ASSERT_EQ(1, _ERModel->checkCommand("1"));
}

void ERModelTest::testCheckInput(){
	ASSERT_EQ('X', _ERModel->checkInput("GG"));
	ASSERT_EQ('E', _ERModel->checkInput("E"));
}

void ERModelTest::testCheckID(){
	testingData();
	ASSERT_FALSE(_ERModel->checkID(0));  
	ASSERT_TRUE(_ERModel->checkID(5));
}

void ERModelTest::testCheckType(){
	testingData();
	//fromID and toID are Entity
	ASSERT_TRUE(_ERModel->checkType(_ERModel->components[0]->getId(), _ERModel->components[0]->getId())); 
	//fromID and toID are Attribute
	ASSERT_TRUE(_ERModel->checkType(_ERModel->components[1]->getId(), _ERModel->components[1]->getId())); 
	//fromID and toID are Relation
	ASSERT_TRUE(_ERModel->checkType(_ERModel->components[4]->getId(), _ERModel->components[4]->getId())); 
	//fromID and toID are Connection
	ASSERT_TRUE(_ERModel->checkType(_ERModel->components[2]->getId(), _ERModel->components[2]->getId())); 
	//fromID is Attribute, toID is Relation.
	ASSERT_TRUE(_ERModel->checkType(_ERModel->components[3]->getId(), _ERModel->components[4]->getId())); 
	//fromID is Relation, toID is Attribute.
	ASSERT_TRUE(_ERModel->checkType(_ERModel->components[4]->getId(), _ERModel->components[3]->getId())); 
	//fromID is Connection, toID isn't Connection.
	ASSERT_TRUE(_ERModel->checkType(_ERModel->components[2]->getId(), _ERModel->components[3]->getId())); 
	//fromID isn't Connection, toID is Connection.
	ASSERT_TRUE(_ERModel->checkType(_ERModel->components[2]->getId(), _ERModel->components[3]->getId())); 
	//fromID is Entity, toID is Attribute.
	ASSERT_FALSE(_ERModel->checkType(_ERModel->components[0]->getId(), _ERModel->components[3]->getId()));
	//fromID is Entity, toID is Relation.
	ASSERT_FALSE(_ERModel->checkType(_ERModel->components[0]->getId(), _ERModel->components[4]->getId()));
}

void ERModelTest::testCheckCardinality(){
	testingData();
	//連接Entity和Relation
	_ERModel->addNode('C', "1", _ERModel->nodeID);
	ASSERT_EQ(6, _ERModel->components.size());
	_ERModel->addConnection(_ERModel->components.back()->getId(), _ERModel->components[0], _ERModel->components[4], "");
	//fromID is Entity and toID is Relation
	ASSERT_TRUE(_ERModel->checkCardinality(_ERModel->components[0]->getId(), _ERModel->components[4]->getId()));
	//fromID is Relation and toID is Entity
	ASSERT_TRUE(_ERModel->checkCardinality(_ERModel->components[4]->getId(), _ERModel->components[0]->getId()));
	//else
	ASSERT_FALSE(_ERModel->checkCardinality(_ERModel->components[0]->getId(), _ERModel->components[3]->getId()));
}

void ERModelTest::testExistPathBetween(){
	testingData();
	//兩components有連接情況
	ASSERT_TRUE(_ERModel->existPathBetween(_ERModel->components[0]->getId(), _ERModel->components[1]->getId()));
	ASSERT_TRUE(_ERModel->existPathBetween(_ERModel->components[1]->getId(), _ERModel->components[0]->getId()));
	//兩components未連接情況
	ASSERT_FALSE(_ERModel->existPathBetween(_ERModel->components[0]->getId(), _ERModel->components[3]->getId()));
}

void ERModelTest::testLoadFileAddPosition(){
	ASSERT_TRUE(_ERModel->loadFile("./testdata/file1.erd"));
	ASSERT_TRUE(_ERModel->loadFileAddPosition("./testdata/file1.pos"));
}

void ERModelTest::testSaveErdFile(){
	ASSERT_TRUE(_ERModel->loadFile("./testdata/file1.erd"));
	ASSERT_TRUE(_ERModel->loadFileAddPosition("./testdata/file1.pos"));
	_ERModel->saveErdFile("./testdata/file2.erd");
}

void ERModelTest::testSavePosFile(){
	ASSERT_TRUE(_ERModel->loadFile("./testdata/file1.erd"));
	ASSERT_TRUE(_ERModel->loadFileAddPosition("./testdata/file1.pos"));
	_ERModel->saveErdFile("./testdata/file2.pos");
}

void ERModelTest::testSaveXmlFile(){
	ASSERT_TRUE(_ERModel->loadFile("./testdata/file1.erd"));
	ASSERT_TRUE(_ERModel->loadFileAddPosition("./testdata/file1.pos"));
	_ERModel->saveXmlFile("./testdata/file2.xml");
}

//--------------GTest
TEST_F(ERModelTest, testLoadFile){
	testLoadFile();
}

TEST_F(ERModelTest, testLoadFileAddNodes){
	testLoadFileAddNodes();
}

TEST_F(ERModelTest, testLoadFileAddConnections){
	testLoadFileAddConnections();
}

TEST_F(ERModelTest, testLoadFileAddPrimaryKeys){
	testLoadFileAddPrimaryKeys();
}

TEST_F(ERModelTest, testSaveFile){
	testSaveFile();
}

TEST_F(ERModelTest, testSaveFileAddNodes){
	testSaveFileAddNodes();
}

TEST_F(ERModelTest, testSaveFileAddConnections){
	testSaveFileAddConnections();
}

TEST_F(ERModelTest, testSaveFileAddPrimaryKeys){
	testSaveFileAddPrimaryKeys();
}

TEST_F(ERModelTest, testAddNode){
	testAddNode();
}

TEST_F(ERModelTest, testAddConnectionNode){
	testAddConnectionNode();
}

TEST_F(ERModelTest, testAddConnection){
	testAddConnection();
}

TEST_F(ERModelTest, testAddPrimaryKey){
	testAddPrimaryKey();
}

TEST_F(ERModelTest, testDeleteComponent){
	testDeleteComponent();
}

TEST_F(ERModelTest, testDeleteConnection){
	testDeleteConnection();
}

TEST_F(ERModelTest, testDeleteNode){
	testDeleteNode();
}

TEST_F(ERModelTest, testCheckCommand){
	testCheckCommand();
}

TEST_F(ERModelTest, testCheckInput){
	testCheckInput();
}

TEST_F(ERModelTest, testCheckID){
	testCheckID();
}

TEST_F(ERModelTest, testCheckType){
	testCheckType();
}

TEST_F(ERModelTest, testCheckCardinality){
	testCheckCardinality();
}

TEST_F(ERModelTest, testExistPathBetween){
	testExistPathBetween();
}

TEST_F(ERModelTest, testLoadFileAddPosition){
	testLoadFileAddPosition();
}

TEST_F(ERModelTest, testSaveErdFile){
	testSaveErdFile();
}

TEST_F(ERModelTest, testSavePosFile){
	testSavePosFile();
}

TEST_F(ERModelTest, testSaveXmlFile){
	testSaveXmlFile();
}
