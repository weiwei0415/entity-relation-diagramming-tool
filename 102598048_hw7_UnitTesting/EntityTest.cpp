#include "EntityTest.h"

void EntityTest::SetUp() {
	_Entity = new Entity();
	//Attribute
	_Node1 = new Entity();
	_Node1->setId(0);
	_Node1->setType(ATTRIBUTE_);
	_Node1->setText("A1");
	//Relation
	_Node2 = new Relationship();
	_Node2->setId(1);
	_Node2->setType(RELATION_);
	_Node2->setText("R1");
}

void EntityTest::TearDown() {
	delete _Entity;
}

void EntityTest::testConnectTo() {
	ASSERT_TRUE(_Entity->getConnection().empty());
	//Attribute
	_Entity->connectTo(_Node1);
	ASSERT_EQ(1, _Entity->getConnection().size());
	ASSERT_EQ(0, _Entity->getConnection().back()->getId());
	ASSERT_EQ(ATTRIBUTE_, _Entity->getConnection().back()->getType());
	ASSERT_EQ("A1", _Entity->getConnection().back()->getText());
	//Relation
	_Entity->connectTo(_Node2);
	ASSERT_EQ(2, _Entity->getConnection().size());
	ASSERT_EQ(1, _Entity->getConnection().back()->getId());
	ASSERT_EQ(RELATION_, _Entity->getConnection().back()->getType());
	ASSERT_EQ("R1", _Entity->getConnection().back()->getText());
}

void EntityTest::testResetPrimaryKey() {
	ASSERT_TRUE(_Entity->getAllPrimaryKey().empty());
	_Entity->setPrimaryKey(_Node1);
	ASSERT_EQ(1, _Entity->getAllPrimaryKey().size());
	_Entity->resetPrimaryKey();
	ASSERT_TRUE(_Entity->getAllPrimaryKey().empty());
}

//-----GTest-----
TEST_F(EntityTest, testConnectTo) {
	testConnectTo();
}

TEST_F(EntityTest, testResetPrimaryKey) {
	testResetPrimaryKey();
}
